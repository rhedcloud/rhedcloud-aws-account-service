/* *****************************************************************************
 This file is part of the RHEDcloud AWS Account Service.

 Copyright 2020 RHEDcloud Foundation. All rights reserved.
 ******************************************************************************/

package edu.emory.awsaccount.service.roleDeprovisioning.step;

import java.util.List;
import java.util.Properties;

import javax.jms.JMSException;

import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.moa.EnterpriseObjectDeleteException;
import org.openeai.moa.EnterpriseObjectGenerateException;
import org.openeai.moa.EnterpriseObjectQueryException;
import org.openeai.transport.RequestService;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.PermissionSet;
import com.amazon.aws.moa.objects.resources.v1_0.PermissionSetQuerySpecification;
import com.amazon.aws.moa.objects.resources.v1_0.PermissionSetRequisition;
import com.amazon.aws.moa.objects.resources.v1_0.Property;
import com.amazon.aws.moa.objects.resources.v1_0.RoleDeprovisioningRequisition;

import edu.emory.awsaccount.service.provider.RoleDeprovisioningProvider;

/**
 * Delete the AWS IAM Identity Center Permission Set for the custom role.
 */
public class DeletePermissionSetForCustomRole extends AbstractStep implements Step {
 

	String LOGTAG = getStepTag() + "[DeletePermissionSetForCustomRole] ";
	private RequestService rs;
	
    @SuppressWarnings("unused")
	public void init(String provisioningId, Properties props, AppConfig aConfig, RoleDeprovisioningProvider rpp) throws StepException {
        super.init(provisioningId, props, aConfig, rpp);

        // This step needs to send messages to the IDM service
        // to delete and create permission sets.
        try {
        	ProducerPool p2pIdm = (ProducerPool)getAppConfig()
                .getObject("IdmServiceProducerPool");
            PointToPointProducer p2p =
                    (PointToPointProducer) p2pIdm.getProducer();
            rs = (RequestService)p2p;

        } catch (EnterpriseConfigurationObjectException ecoe) {
            String errMsg = "An error occurred retrieving an object from " +
                    "AppConfig. The exception is: " + ecoe.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new StepException(errMsg);
        } catch (JMSException e) {
            String errMsg = "An error occurred retrieving a P2P from " +
                    "ProducerPool. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new StepException(errMsg);
		}
        
        // This step needs to get PermissionSet and PermissionSetRequistion objects from the config
		try {
			PermissionSet ps = (PermissionSet) getAppConfig().getObject("PermissionSet.v1_0");
			PermissionSetRequisition psseed = (PermissionSetRequisition) getAppConfig().getObject("PermissionSetRequisition.v1_0");
			PermissionSetQuerySpecification psquery = (PermissionSetQuerySpecification) getAppConfig().getObject("PermissionSetQuerySpecification.v1_0");
		} catch (EnterpriseConfigurationObjectException e) {
			String msg="Can't load PermissionSet.v1_0 or PermissionSetRequisition.v1_0 or PermissionSetQuerySpecification.v1_0";
			logger.fatal(LOGTAG+msg);
			throw new StepException(msg);
		}

        logger.info(LOGTAG + "Initialization complete.");
    }
    
    protected List<Property> run() throws StepException {
        long startTime = System.currentTimeMillis();
        logger.info(LOGTAG + "Begin running the step.");

        addResultProperty(STEP_EXECUTION_METHOD_PROPERTY_KEY, STEP_EXECUTION_METHOD_EXECUTED);

        RoleDeprovisioningRequisition requisition = getRoleDeprovisioning().getRoleDeprovisioningRequisition();
        String permissionSetName = requisition.getRoleName();
        addResultProperty("customPermissionSetName", permissionSetName);
        
		try {
			PermissionSet ps = (PermissionSet) getAppConfig().getObject("PermissionSet.v1_0");
			PermissionSetQuerySpecification psquery = (PermissionSetQuerySpecification) getAppConfig()
					.getObject("PermissionSetQuerySpecification.v1_0");
			psquery.setName(permissionSetName);

			@SuppressWarnings({ "unchecked" })
			List<PermissionSet> results = ps.query(psquery, rs);
			if (results.size() != 1) {
		           String errMsg = results.size()+" results returned from permissionSetName but expected 1 and only 1.";
		           logger.warn(LOGTAG + errMsg);
		           addResultProperty("deletedCustomPermissionSetArn", errMsg);
			} 
			if (results.size() > 0) {
				ps = results.get(0);				
				ps.delete("purge", rs);
			    // set result properties
		        addResultProperty("deletedCustomPermissionSetArn", results.get(0).getArn());				
			}
			
	        // Update the step.
	        update(STEP_STATUS_COMPLETED, STEP_RESULT_SUCCESS);

	        // Log completion time.
	        long time = System.currentTimeMillis() - startTime;
	        logger.info(LOGTAG + "Step run completed in " + time + "ms.");
	        
	        // Return the properties.
	        return getResultProperties();
	        
		} catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "An error occurred retrieving an object from " +
                    "AppConfig. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            update(STEP_STATUS_COMPLETED, STEP_RESULT_FAILURE);
            throw new StepException(errMsg,e);
		} catch (EnterpriseFieldException e) {
            String errMsg = "An error occurred setting a field in the permission set query spec" +
                    ". The exception is: " + e.getMessage();
            logger.error(LOGTAG + errMsg);
            update(STEP_STATUS_COMPLETED, STEP_RESULT_FAILURE);
            throw new StepException(errMsg,e);
		} catch (EnterpriseObjectDeleteException e) {
            String errMsg = "An error occurred deleting the permission set for " +
                    permissionSetName+". The exception is: " + e.getMessage();
            logger.error(LOGTAG + errMsg);
            update(STEP_STATUS_COMPLETED, STEP_RESULT_FAILURE);
            throw new StepException(errMsg,e);
		} catch (EnterpriseObjectQueryException e) {
            String errMsg = "An error occurred querying for the permission set named " +
                    permissionSetName+". The exception is: " + e.getMessage();
            logger.error(LOGTAG + errMsg);
            update(STEP_STATUS_COMPLETED, STEP_RESULT_FAILURE);
            throw new StepException(errMsg,e);
		} 
    }

    protected List<Property> simulate() throws StepException {
        long startTime = System.currentTimeMillis();
        logger.info(LOGTAG + "Begin step simulation.");

        addResultProperty(STEP_EXECUTION_METHOD_PROPERTY_KEY, STEP_EXECUTION_METHOD_SIMULATED);

        // simulated result properties
        addResultProperty("customPermissionSetArn", "arn:aws:sso:::permissionSet/ssoins-72238aa0982f098e/ps-27a030a837227e97");

        // Update the step.
        update(STEP_STATUS_COMPLETED, STEP_RESULT_SUCCESS);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step simulation completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    protected List<Property> fail() throws StepException {
        long startTime = System.currentTimeMillis();
        logger.info(LOGTAG + "Begin step failure simulation.");

        addResultProperty(STEP_EXECUTION_METHOD_PROPERTY_KEY, STEP_EXECUTION_METHOD_FAILURE);

        // Update the step.
        update(STEP_STATUS_COMPLETED, STEP_RESULT_FAILURE);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step failure simulation completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    public void rollback() throws StepException {
    	long startTime = System.currentTimeMillis();
        addResultProperty(STEP_EXECUTION_METHOD_PROPERTY_KEY, STEP_EXECUTION_METHOD_EXECUTED);

        RoleDeprovisioningRequisition requisition = getRoleDeprovisioning().getRoleDeprovisioningRequisition();
        String permissionSetName = requisition.getRoleName();
        addResultProperty("customPermissionSetName", permissionSetName);
		try {
			PermissionSet ps = (PermissionSet) getAppConfig().getObject("PermissionSet.v1_0");
			PermissionSetRequisition psseed = (PermissionSetRequisition) getAppConfig().getObject("PermissionSetRequisition.v1_0");
			psseed.setName(permissionSetName);
			psseed.setSessionDuration("PT12H");
			@SuppressWarnings({ "unchecked" })
			List<PermissionSet> results = ps.generate(psseed, rs);
		       // set result properties
	        addResultProperty("customPermissionSetArn", results.get(0).getArn());

	        // Update the step.
	        update(STEP_STATUS_COMPLETED, STEP_RESULT_SUCCESS);

	        // Log completion time.
	        long time = System.currentTimeMillis() - startTime;
	        logger.info(LOGTAG + "Step run completed in " + time + "ms.");
	        
		} catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "An error occurred retrieving an object from " +
                    "AppConfig. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            update(STEP_STATUS_COMPLETED, STEP_EXECUTION_METHOD_FAILURE);
            throw new StepException(errMsg,e);
		} catch (EnterpriseFieldException e) {
            String errMsg = "An error occurred setting a field in the requisition" +
                    ". The exception is: " + e.getMessage();
            logger.error(LOGTAG + errMsg);
            update(STEP_STATUS_COMPLETED, STEP_EXECUTION_METHOD_FAILURE);
            throw new StepException(errMsg,e);
		} catch (EnterpriseObjectGenerateException e) {
           String errMsg = "An error occurred generating the permission set for " +
                    permissionSetName+". The exception is: " + e.getMessage();
            logger.error(LOGTAG + errMsg);
            update(STEP_STATUS_COMPLETED, STEP_EXECUTION_METHOD_FAILURE);
            throw new StepException(errMsg,e);
		} 
    }

}
