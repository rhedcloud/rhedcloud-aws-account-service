/* *****************************************************************************
 This file is part of the RHEDcloud AWS Account Service.

 Copyright 2020 RHEDcloud Foundation. All rights reserved.
 ******************************************************************************/

package edu.emory.awsaccount.service.roleDeprovisioning.step;

import java.util.List;
import java.util.Properties;

import javax.jms.JMSException;

import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.transport.RequestService;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.IamRole;
import com.amazon.aws.moa.objects.resources.v1_0.Property;
import com.amazon.aws.moa.objects.resources.v1_0.RoleDeprovisioningRequisition;
import com.amazonaws.services.identitymanagement.model.NoSuchEntityException;

import edu.emory.awsaccount.service.provider.RoleDeprovisioningProvider;


/**
 * Delete the AWS IAM role for the custom role.
 */
public class DeleteAwsRoleForCustomRole extends AbstractStep implements Step {
	private RequestService rs;

    public void init(String provisioningId, Properties props, AppConfig aConfig, RoleDeprovisioningProvider rpp) throws StepException {
        super.init(provisioningId, props, aConfig, rpp);

        String LOGTAG = getStepTag() + "[DeleteAwsRoleForCustomRole.init] ";

        // This step needs to send messages to the IDM service
        // to create and delete AWS IamRoles.
        try {
        	ProducerPool p2pIdm = (ProducerPool)getAppConfig()
                .getObject("IdmServiceProducerPool");
            PointToPointProducer p2p =
                    (PointToPointProducer) p2pIdm.getProducer();
            rs = (RequestService)p2p;

        } catch (EnterpriseConfigurationObjectException ecoe) {
            String errMsg = "An error occurred retrieving an object from " +
                    "AppConfig. The exception is: " + ecoe.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new StepException(errMsg);
        } catch (JMSException e) {
            String errMsg = "An error occurred retrieving a P2P from " +
                    "ProducerPool. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new StepException(errMsg);
		}
        
        // This step needs to get IamRole objects from the config
		try {
			getAppConfig().getObject("IamRole.v1_0");
		} catch (EnterpriseConfigurationObjectException e) {
			String msg="Can't load IamRole.v1_0 or IamRoleQuerySpecification.v1_0";
			logger.fatal(LOGTAG+msg);
			throw new StepException(msg);
		}        logger.info(LOGTAG + "Initialization complete.");
    }

    protected List<Property> run() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[DeleteAwsRoleForCustomRole.run] ";
        logger.info(LOGTAG + "Begin running the step.");

        addResultProperty(STEP_EXECUTION_METHOD_PROPERTY_KEY, STEP_EXECUTION_METHOD_EXECUTED);

        // the account and custom role name was specified in the requisition
        RoleDeprovisioningRequisition requisition = getRoleDeprovisioning().getRoleDeprovisioningRequisition();
        String accountId = requisition.getAccountId();
        String roleName = requisition.getRoleName();

        try {
            long elapsedStartTime = System.currentTimeMillis();
            
            IamRole iamRole = (IamRole) getAppConfig().getObject("IamRole.v1_0");
            iamRole.setAccountId(accountId);
            iamRole.setName(roleName);
            iamRole.setAssumeRolePolicyDocument("blank");
            
            iamRole.delete("Delete", rs);
            
            addResultProperty("deletedAwsIamRole", roleName);

            long elapsedTime = System.currentTimeMillis() - elapsedStartTime;
            logger.info(LOGTAG + "Deleted AWS IAM role in " + elapsedTime + " ms.");
        }
        catch (NoSuchEntityException e) {
            // not an error - the role was probably deleted in a previous deprovisioning run
            logger.info(LOGTAG + "AWS IAM role " + roleName + " does not exist.");
            addResultProperty("deletedAwsIamRole", "not applicable");
        }
        catch (Exception e) {
        	if (e.getMessage().indexOf("NoSuchEntity") >= 0) {
                // not an error - the role was probably deleted in a previous deprovisioning run
                logger.info(LOGTAG + "AWS IAM role " + roleName + " does not exist.");
                addResultProperty("deletedAwsIamRole", "not applicable");
        	}
        	else {
                String errMsg = "An error occurred deleting the AWS IAM role. The exception is: " + e.getMessage();
                logger.error(LOGTAG + errMsg);
                throw new StepException(errMsg, e);
        	}
        }

        // Update the step.
        update(STEP_STATUS_COMPLETED, STEP_RESULT_SUCCESS);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step run completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    protected List<Property> simulate() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[DeleteAwsRoleForCustomRole.simulate] ";
        logger.info(LOGTAG + "Begin step simulation.");

        addResultProperty(STEP_EXECUTION_METHOD_PROPERTY_KEY, STEP_EXECUTION_METHOD_SIMULATED);

        // simulated result properties
        addResultProperty("deletedAwsIamRole", "simulated");

        // Update the step.
        update(STEP_STATUS_COMPLETED, STEP_RESULT_SUCCESS);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step simulation completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    protected List<Property> fail() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[DeleteAwsRoleForCustomRole.fail] ";
        logger.info(LOGTAG + "Begin step failure simulation.");

        addResultProperty(STEP_EXECUTION_METHOD_PROPERTY_KEY, STEP_EXECUTION_METHOD_FAILURE);

        // Update the step.
        update(STEP_STATUS_COMPLETED, STEP_RESULT_FAILURE);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step failure simulation completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    public void rollback() throws StepException {
        long startTime = System.currentTimeMillis();
        super.rollback();
        String LOGTAG = getStepTag() + "[DeleteAwsRoleForCustomRole.rollback] ";
        logger.info(LOGTAG + "Rollback called, but this step has nothing to roll back.");

        // Update the step.
        update(STEP_STATUS_ROLLBACK, STEP_RESULT_SUCCESS);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Rollback completed in " + time + "ms.");
    }

}
