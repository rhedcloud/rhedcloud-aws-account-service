/*******************************************************************************
 $Source: $
 $Revision: $
 *******************************************************************************/

/******************************************************************************
 This file is part of the Emory AWS Account Service.

 Copyright (C) 2017 Emory University. All rights reserved.
 ******************************************************************************/
package edu.emory.awsaccount.service.provider.step;

import java.util.HashSet;
import java.util.List;
import java.util.Properties;

import javax.jms.JMSException;

import org.openeai.OpenEaiException;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.transport.RequestService;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.AccountAssignment;
import com.amazon.aws.moa.objects.resources.v1_0.AccountAssignmentQuerySpecification;
import com.amazon.aws.moa.objects.resources.v1_0.AccountAssignmentRequisition;
import com.amazon.aws.moa.objects.resources.v1_0.Property;

import edu.emory.awsaccount.service.provider.VirtualPrivateCloudProvisioningProvider;

/**
 * Runs AWS CreateAccountAssignment
 * <P>
 *
 * @author Tom Cervenka (tcerven@emory.edu)
 * @version 1.0 - 3 Aug 2023
 **/
public class AssignGroupAndAccountToPermissionSet extends AbstractStep implements Step {

	private String permissionSetName;
	private ProducerPool p2pIdm;
	private int requestTimeoutIntervalInMillis;
	private String groupName;

	@SuppressWarnings("unused")
	public void init (String provisioningId, Properties props,
			AppConfig aConfig, VirtualPrivateCloudProvisioningProvider vpcpp)
					throws StepException {

		super.init(provisioningId, props, aConfig, vpcpp);

		String LOGTAG = getStepTag() + "[AssignGroupAndAccountToPermissionSet.init] ";
	
        // This step needs to send messages to the IDM service
        // to create account roles.
        try {
            p2pIdm = (ProducerPool)getAppConfig()
                .getObject("IdmServiceProducerPool");
        }
        catch (EnterpriseConfigurationObjectException ecoe) {
            String errMsg = "An error occurred retrieving an object from " +
                    "AppConfig. The exception is: " + ecoe.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new StepException(errMsg);
        }

		// Get custom step properties.
		logger.info(LOGTAG + "Getting custom step properties...");

		permissionSetName = getProperties().getProperty("permissionSet");
		logger.info(LOGTAG + "permissionSet is: " + permissionSetName);
		if (permissionSetName==null) {
            String errMsg = "The permissionSet property is null.";
            logger.fatal(LOGTAG + errMsg);
            throw new StepException(errMsg);	
		}
		
		groupName = getProperties().getProperty("groupName");
		logger.info(LOGTAG + "groupName is: " + groupName);
		if (groupName==null) {
            String errMsg = "The groupName property is null.";
            logger.fatal(LOGTAG + errMsg);
            throw new StepException(errMsg);	
		}
		
		try {
			requestTimeoutIntervalInMillis = Integer.parseInt(getProperties().getProperty("requestTimeoutIntervalInMillis","10000"));
		} catch (NumberFormatException e) {
			String errMsg="The requestTimeoutIntervalInMillis property must be an integer.";
			logger.fatal(LOGTAG+errMsg);
			throw new StepException(errMsg,e);			
		}
		logger.info(LOGTAG + "requestTimeoutIntervalInMillis is: " + requestTimeoutIntervalInMillis);
		
		try {
			AccountAssignment aa = (AccountAssignment) getAppConfig().getObject("AccountAssignment.v1_0");
			AccountAssignmentRequisition aar = (AccountAssignmentRequisition) getAppConfig().getObject("AccountAssignmentRequisition.v1_0");
			AccountAssignmentQuerySpecification aaq = (AccountAssignmentQuerySpecification) getAppConfig().getObject("AccountAssignmentQuerySpecification.v1_0");
		} catch (EnterpriseConfigurationObjectException e) {
			String errMsg="The appconfig is missing AccountAssignment.v1_0 and/or AccountAssignmentRequisition.v1_0 and/or AccountAssignmentQuerySpecification.v1_0";
			logger.fatal(LOGTAG+errMsg);
			throw new StepException(errMsg,e);
		}

		logger.info(LOGTAG + "Initialization complete.");
	}
	
	private void processAccountAssignments(String mode,String LOGTAG) throws StepException {
		
		//DETERMINE_NEW_OR_EXISTING_ACCOUNT
		String allocateNewAccount = getStepPropertyValue("DETERMINE_NEW_OR_EXISTING_ACCOUNT", "allocateNewAccount");
		addResultProperty("allocateNewAccount", allocateNewAccount);
		logger.info(LOGTAG+"allocateNewAccount is "+allocateNewAccount);
		
		if ("false".equals(allocateNewAccount)) {
			addResultProperty("Step SKIPPED", "This is not a new account");
			logger.info(LOGTAG+"Step SKIPPED -- This is not a new account");
			update(COMPLETED_STATUS, SUCCESS_RESULT);
			return;
		}	
		
		HashSet<String> groups = new HashSet<String>();
		groups.add(groupName);
		
		String newAccountId = getStepPropertyValue("GENERATE_NEW_ACCOUNT", "newAccountId");
		addResultProperty("newAccountId", newAccountId);
		logger.info(LOGTAG+"newAccountId is "+newAccountId);
		
		
        // Get a producer from the pool
        RequestService rs = null;
        try {
            PointToPointProducer p2p =
                (PointToPointProducer) p2pIdm.getProducer();
            p2p.setRequestTimeoutInterval(requestTimeoutIntervalInMillis);
            rs = (RequestService)p2p;
        }
        catch (JMSException jmse) {
            String errMsg = "An error occurred getting a producer " +
                "from the pool. The exception is: " + jmse.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new StepException(errMsg, jmse);
        }
        
		for(String candidate: groups) {

			logger.info(LOGTAG+"candidate is "+candidate);
			
			try {
				
				AccountAssignment aa = (AccountAssignment) getAppConfig().getObject("AccountAssignment.v1_0");
				
				if ("CREATE".equals(mode)) {
					logger.info(LOGTAG+"Creating account assignment for "+groupName+" with permisionSet "+permissionSetName+"...");
					
					AccountAssignmentRequisition aar = (AccountAssignmentRequisition) getAppConfig().getObject("AccountAssignmentRequisition.v1_0");
					aar.setPermissionSetName(permissionSetName);				
					aar.setPrincipalType("GROUP");
					aar.setTargetId(newAccountId);
					aar.setTargetType("AWS_ACCOUNT");
					aar.setPublicId(groupName);
					
					@SuppressWarnings("unchecked")
					List<AccountAssignment> result = aa.generate(aar, rs);
					AccountAssignment newaa = result.get(0);
					addResultProperty("status","SUCCEEDED");
					addResultProperty("groupId",newaa.getPrincipalId());
				
				}
				
				if ("DELETE".equals(mode)) {
					logger.info(LOGTAG+"Deleting account assignment for "+groupName+" with permisionSet "+permissionSetName+"...");
					
					AccountAssignmentQuerySpecification aaq = (AccountAssignmentQuerySpecification) getAppConfig().getObject("AccountAssignmentQuerySpecification.v1_0");
					aaq.setPermissionSetName(permissionSetName);			
					aaq.setPrincipalType("GROUP");
					aaq.setTargetId(newAccountId);
					aaq.setTargetType("AWS_ACCOUNT");
					aaq.setPublicId(groupName);

					@SuppressWarnings("unchecked")
					List<AccountAssignment> queryResult = aa.query(aaq, rs);
					logger.info(LOGTAG+"Reply from query account assignment: "+queryResult);
					if (queryResult.size()>1) logger.warn(LOGTAG+"Expected exactly one account assignment object returned from query but got back "+queryResult.size());
					
					AccountAssignment currentaa = queryResult.get(0);
					
					currentaa.delete("DELETE", rs);
					logger.info(LOGTAG+"account assignment delete succeeded");
					addResultProperty(groupName+".status", "delete succeeded");
				}
										
			} catch (OpenEaiException e) {
				String errMsg = "An error occurred trying to create or delete the account assignment. " +
						e.getMessage();
				logger.error(LOGTAG + errMsg);
				throw new StepException(errMsg,e);       	      				
			} 
					
		}
	}

	protected List<Property> run() throws StepException {
		long startTime = System.currentTimeMillis();
		String LOGTAG = getStepTag() + "[AssignGroupAndAccountToPermissionSet.run] ";
		
		processAccountAssignments("CREATE", LOGTAG);
		
		// Set result properties.
		addResultProperty("stepExecutionMethod", RUN_EXEC_TYPE);
		
		// Update the step.
		update(COMPLETED_STATUS, SUCCESS_RESULT);

		// Log completion time.
		long time = System.currentTimeMillis() - startTime;
		logger.info(LOGTAG + "Step run completed in " + time + "ms.");

		// Return the properties.
		return getResultProperties();

	}

	protected List<Property> simulate() throws StepException {
		long startTime = System.currentTimeMillis();
		String LOGTAG = getStepTag() +
				"[AssignUserAndAccountToPermissionSet.simulate] ";
		logger.info(LOGTAG + "Begin step simulation.");

		// Set return properties.
		addResultProperty("stepExecutionMethod", SIMULATED_EXEC_TYPE);
		String userId=getVirtualPrivateCloudProvisioning().getVirtualPrivateCloudRequisition().getAccountOwnerUserId();
		addResultProperty("userId", userId);

		// Update the step.
		update(COMPLETED_STATUS, SUCCESS_RESULT);

		// Log completion time.
		long time = System.currentTimeMillis() - startTime;
		logger.info(LOGTAG + "Step simulation completed in " + time + "ms.");

		// Return the properties.
		return getResultProperties();
	}

	protected List<Property> fail() throws StepException {
		long startTime = System.currentTimeMillis();
		String LOGTAG = getStepTag() +
				"[AssignGroupAndAccountToPermissionSet.fail] ";
		logger.info(LOGTAG + "Begin step failure simulation.");

		// Set return properties.
		addResultProperty("stepExecutionMethod", FAILURE_EXEC_TYPE);
		String userId=getVirtualPrivateCloudProvisioning().getVirtualPrivateCloudRequisition().getAccountOwnerUserId();
		addResultProperty("userId", userId);

		// Update the step.
		update(COMPLETED_STATUS, FAILURE_RESULT);

		// Log completion time.
		long time = System.currentTimeMillis() - startTime;
		logger.info(LOGTAG + "Step failure simulation completed in " + time + "ms.");

		// Return the properties.
		return getResultProperties();
	}
	
	public void rollback() throws StepException {
		long startTime = System.currentTimeMillis();
		String LOGTAG = getStepTag() + "[AssignGroupAndAccountToPermissionSet.rollback] ";
		logger.info(LOGTAG + "Rollback started");
		
		processAccountAssignments("DELETE", LOGTAG);	
		
		update(ROLLBACK_STATUS, SUCCESS_RESULT);

		// Log completion time.
		long time = System.currentTimeMillis() - startTime;
		logger.info(LOGTAG + "Rollback completed in " + time + "ms.");
	}

}
