/*******************************************************************************
 $Source: $
 $Revision: $
 *******************************************************************************/

/******************************************************************************
 This file is part of the Emory AWS Account Service.

 Copyright (C) 2017 Emory University. All rights reserved.
 ******************************************************************************/
package edu.emory.awsaccount.service.provider.step;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.jms.JMSException;

import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.jms.producer.MessageProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.moa.EnterpriseObjectCreateException;
import org.openeai.moa.EnterpriseObjectQueryException;
import org.openeai.moa.XmlEnterpriseObjectException;
import org.openeai.transport.RequestService;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.VirtualPrivateCloudProvisioning;
import com.amazon.aws.moa.jmsobjects.user.v1_0.UserProfile;
import com.amazon.aws.moa.objects.resources.v1_0.Datetime;
import com.amazon.aws.moa.objects.resources.v1_0.Property;
import com.amazon.aws.moa.objects.resources.v1_0.UserProfileQuerySpecification;
import com.amazon.aws.moa.objects.resources.v1_0.VirtualPrivateCloudRequisition;

import edu.emory.awsaccount.service.provider.VirtualPrivateCloudProvisioningProvider;

/**
 * If this is a new account request, create user profiles for anyone assigned 
 * a role in the account.
 * <p>
 *
 * @author Tod Jackson (jtjacks@emory.edu)
 * @version 1.0 - 22 September 2023
 **/
public class CreateUserProfiles extends AbstractStep implements Step {

    private ProducerPool m_awsAccountServiceProducerPool = null;
    private Properties userProfileProperties = null;

    public void init(String provisioningId, Properties props,
                     AppConfig aConfig, VirtualPrivateCloudProvisioningProvider vpcpp)
            throws StepException {

        super.init(provisioningId, props, aConfig, vpcpp);

        String LOGTAG = getStepTag() + "[CreateUserProfiles.init] ";

        // This step needs to send messages to the AWS account service
        // to create user profile meta data if needed
        ProducerPool p2p1 = null;
        try {
            p2p1 = (ProducerPool) getAppConfig()
                    .getObject("AwsAccountServiceProducerPool");
            setAwsAccountServiceProducerPool(p2p1);
        } catch (EnterpriseConfigurationObjectException ecoe) {
            // An error occurred retrieving an object from AppConfig. Log it and
            // throw an exception.
            String errMsg = "An error occurred retrieving an object from " +
                    "AppConfig. The exception is: " + ecoe.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new StepException(errMsg);
        }

        userProfileProperties = new Properties();
        Properties stepProperties = getProperties();
		Iterator<Object> keysIterator = stepProperties.keySet().iterator();

		// userProfileProperty:sendSrdNotificationsLow
		String upTag = "userprofileproperty:";
		while (keysIterator.hasNext()) {
			String key = (String)keysIterator.next();
			if (key.trim().toLowerCase().indexOf(upTag) == 0) {
				String upProp = key.substring(upTag.length()).trim();
				String value = stepProperties.getProperty(key).trim();
		        logger.info(LOGTAG + "UserProfile property: " + upProp + "=" + value);
				userProfileProperties.setProperty(upProp, value);
			}
		}

        logger.info(LOGTAG + "Initialization complete.");
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
	protected List<Property> run() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[CreateUserProfiles.run] ";
        logger.info(LOGTAG + "Begin running the step.");

        boolean userProfilesCreated = false;
        boolean hadProfilesToCreate = false;

        // Return properties
        addResultProperty("stepExecutionMethod", RUN_EXEC_TYPE);

        // Get the VPCP requisition object.
        VirtualPrivateCloudProvisioning vpcp = getVirtualPrivateCloudProvisioning();
        VirtualPrivateCloudRequisition req = vpcp.getVirtualPrivateCloudRequisition();
        
        // for each customer admin in the account:
        // - see if they have a user profile
        // - if they don't have one, create one with default props
        RequestService rs;
		try {
			rs = (RequestService)getAwsAccountServiceProducerPool().getProducer();
		}
		catch (JMSException e1) {
            String errMsg = "An error occurred getting a producer from the pool. The exception is: " + e1.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new StepException(errMsg, e1);
		}
        
		List<String> adminPpids = new java.util.ArrayList<String>();
		// add the owner first
		adminPpids.add(req.getAccountOwnerUserId());
		// now add all other admins
        for (String adminPpid : (List<String>) req.getCustomerAdminUserId()) {
        	if (!adminPpids.contains(adminPpid)) {
        		adminPpids.add(adminPpid);
        	}
        }
		
        int adminCntr=0;
        for (String adminPpid : adminPpids) {
        	adminCntr++;
            UserProfile actionable = new UserProfile();
            UserProfileQuerySpecification querySpec = new UserProfileQuerySpecification();
            try {
                actionable = (UserProfile)getAppConfig().getObjectByType(actionable.getClass().getName());
                querySpec = (UserProfileQuerySpecification)getAppConfig()
                        .getObjectByType(querySpec.getClass().getName());

	            // Build the querySpec.
	            logger.info(LOGTAG + "adminPpid is: " + adminPpid);
	            addResultProperty("adminPpid-" + adminCntr, adminPpid);
                querySpec.setUserId(adminPpid);
                
                long queryStartTime = System.currentTimeMillis();
                List results = actionable.query(querySpec, rs);
                long queryTime = System.currentTimeMillis() - queryStartTime;
                logger.info(LOGTAG + "Queried for UserProfile for adminPpid " +
                    adminPpid + " in " + queryTime + " ms. Returned " + results.size() +
                    " result(s).");
                
                if (results.size() == 0) {
                	// no existing profile, create one...
                	hadProfilesToCreate = true;
                	logger.info(LOGTAG + "admin " + adminPpid 
            			+ " does not have a profile, creating a default profile.");
                	
                    // Get a configured account object from AppConfig.
                    UserProfile userProfile = new UserProfile();
                    userProfile = (UserProfile) getAppConfig().getObjectByType(userProfile.getClass().getName());

                    userProfile.setUserId(adminPpid);

                    long currentTime = System.currentTimeMillis();
                    userProfile.setCreateUser(req.getAuthenticatedRequestorUserId());
                    Datetime createDatetime = new Datetime("Create", currentTime);
                    userProfile.setCreateDatetime(createDatetime);

                    Datetime lastLoginDatetime = new Datetime("LastLoginDatetime", currentTime);
                    userProfile.setLastLoginDatetime(lastLoginDatetime);

                    // add default profile properties from the config doc
            		Iterator<Object> keys = userProfileProperties.keySet().iterator();
            		while (keys.hasNext()) {
            			String key = (String)keys.next();
        				String value = userProfileProperties.getProperty(key);
        				Property upp = userProfile.newProperty();
        				upp.setKey(key);
        				upp.setValue(value);
        				upp.setCreateUser(req.getAuthenticatedRequestorUserId());
                        Datetime upp_createDatetime = new Datetime("Create", System.currentTimeMillis());
                        upp.setCreateDatetime(upp_createDatetime);
        				userProfile.addProperty(upp);
            		}

                    // Log the state of the account.
                    try {
                        logger.info(LOGTAG + "UserProfile to create is: " + userProfile.toXmlString());
                    } catch (XmlEnterpriseObjectException xeoe) {
                        String errMsg = "An error occurred serializing the query spec to XML. The exception is: " + xeoe.getMessage();
                        logger.error(LOGTAG + errMsg);
                        throw new StepException(errMsg, xeoe);
                    }

                    long createStartTime = System.currentTimeMillis();
                    userProfile.create(rs);
                    long createTime = System.currentTimeMillis() - createStartTime;
                    logger.info(LOGTAG + "Create UserProfile in " + createTime + " ms.");
                    
                    userProfilesCreated = true;
                    addResultProperty("createdUserProfile", Boolean.toString(userProfilesCreated));
                }
                else {
                	// user already has a profile, no need to create one, just log a message
	    			logger.info(LOGTAG + "admin " + adminPpid 
	    				+ " already has a profile, no need to create one");
                }
            }
            catch (EnterpriseConfigurationObjectException ecoe) {
                String errMsg = "An error occurred retrieving an object from " +
                  "AppConfig. The exception is: " + ecoe.getMessage();
                logger.error(LOGTAG + errMsg);
                throw new StepException(errMsg, ecoe);
            }
			catch (EnterpriseObjectQueryException e) {
                String errMsg = "An error occurred querying for the  " +
                  "Account object. The exception is: " + e.getMessage();
                logger.error(LOGTAG + errMsg);
                throw new StepException(errMsg, e);
			}
			catch (EnterpriseFieldException e) {
                String errMsg = "An error occurred setting the values of the " +
                "query spec. The exception is: " + e.getMessage();
                logger.error(LOGTAG + errMsg);
                throw new StepException(errMsg, e);
			}
			catch (EnterpriseObjectCreateException e) {
                String errMsg = "An error occurred creating the UserProfile " +
                    ". The exception is: " + e.getMessage();
	                logger.error(LOGTAG + errMsg);
	                throw new StepException(errMsg, e);
			}
			finally {
                getAwsAccountServiceProducerPool().releaseProducer((MessageProducer) rs);
            }
        }

        // Update the step result.
        String stepResult = FAILURE_RESULT;
        if (hadProfilesToCreate && userProfilesCreated) {
            stepResult = SUCCESS_RESULT;
        }
        else if (!hadProfilesToCreate) {
            stepResult = SUCCESS_RESULT;
			logger.info(LOGTAG + "No profiles were needed for this account.  Step is successful.");
        }

        // Update the step.
        update(COMPLETED_STATUS, stepResult);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step run completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    protected List<Property> simulate() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[CreateUserProfiles.simulate] ";
        logger.info(LOGTAG + "Begin step simulation.");

        // Set return properties.
        addResultProperty("stepExecutionMethod", SIMULATED_EXEC_TYPE);

        // Update the step.
        update(COMPLETED_STATUS, SUCCESS_RESULT);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step simulation completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    protected List<Property> fail() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[CreateUserProfiles.fail] ";
        logger.info(LOGTAG + "Begin step failure simulation.");

        // Set return properties.
        addResultProperty("stepExecutionMethod", FAILURE_EXEC_TYPE);

        // Update the step.
        update(COMPLETED_STATUS, FAILURE_RESULT);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step failure simulation completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

	public void rollback() throws StepException {
        long startTime = System.currentTimeMillis();

        super.rollback();

        String LOGTAG = getStepTag() + "[CreateUserProfiles.rollback] ";
        logger.info(LOGTAG + "Rollback called, for user profile metadata.");

//        // Get the VPCP requisition object.
//        VirtualPrivateCloudProvisioning vpcp = getVirtualPrivateCloudProvisioning();
//        VirtualPrivateCloudRequisition req = vpcp.getVirtualPrivateCloudRequisition();
//        
//        // TODO: for each customer admin in the account:
//        // - see if they have a user profile
//        // - if they don't have one, create one with default props
//        RequestService rs;
//		try {
//			rs = (RequestService)getAwsAccountServiceProducerPool().getProducer();
//		}
//		catch (JMSException e1) {
//            String errMsg = "An error occurred getting a producer from the pool. The exception is: " + e1.getMessage();
//            logger.error(LOGTAG + errMsg);
//            throw new StepException(errMsg, e1);
//		}
//        
//        for (String adminPpid : (List<String>) req.getCustomerAdminUserId()) {
//            UserProfile actionable = new UserProfile();
//            UserProfileQuerySpecification querySpec = new UserProfileQuerySpecification();
//            try {
//                actionable = (UserProfile)getAppConfig().getObjectByType(actionable.getClass().getName());
//                querySpec = (UserProfileQuerySpecification)getAppConfig()
//                        .getObjectByType(querySpec.getClass().getName());
//
//	            // Build the querySpec.
//	            logger.info(LOGTAG + "adminPpid is: " + adminPpid);
//	            addResultProperty("adminPpid", adminPpid);
//                querySpec.setUserId(adminPpid);
//                
//                long queryStartTime = System.currentTimeMillis();
//                List results = actionable.query(querySpec, rs);
//                long queryTime = System.currentTimeMillis() - queryStartTime;
//                logger.info(LOGTAG + "Queried for UserProfile for adminPpid " +
//                    adminPpid + " in " + queryTime + " ms. Returned " + results.size() +
//                    " result(s).");
//
//	            // If there is a result, delete the account metadata
//	            if (results.size() > 0) {
//	                UserProfile userProfile = (UserProfile) results.get(0);
//	
//	                // Delete the account metadata
//                    long deleteStartTime = System.currentTimeMillis();
//                    userProfile.delete("Delete", rs);
//                    long deleteTime = System.currentTimeMillis() - deleteStartTime;
//                    logger.info(LOGTAG + "Deleted UserProfile in " + deleteTime + " ms. Got " + results.size() + " result(s).");
//                    addResultProperty("deletedAccountMetadataOnRollback", "true");
//	            }
//	        } catch (EnterpriseObjectDeleteException eode) {
//	            String errMsg = "An error occurred deleting the object. The exception is: " + eode.getMessage();
//	            logger.error(LOGTAG + errMsg);
//	            throw new StepException(errMsg, eode);
//	        }
//			catch (EnterpriseConfigurationObjectException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			catch (EnterpriseFieldException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			catch (EnterpriseObjectQueryException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} finally {
//	            getAwsAccountServiceProducerPool().releaseProducer((MessageProducer) rs);
//	        }
//        }

        update(ROLLBACK_STATUS, SUCCESS_RESULT);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Rollback completed in " + time + "ms.");
    }

    private void setAwsAccountServiceProducerPool(ProducerPool pool) {
        m_awsAccountServiceProducerPool = pool;
    }

    private ProducerPool getAwsAccountServiceProducerPool() {
        return m_awsAccountServiceProducerPool;
    }
}
