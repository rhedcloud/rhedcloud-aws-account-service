package edu.emory.awsaccount.service.provider;

public class AcctNotificationPojo {

	String annotationText;
	long timeAdded;
	
	public AcctNotificationPojo() {
		// TODO Auto-generated constructor stub
	}

	public String getAnnotationText() {
		return annotationText;
	}

	public void setAnnotationText(String annotationText) {
		this.annotationText = annotationText;
	}

	public long getTimeAdded() {
		return timeAdded;
	}

	public void setTimeAdded(long timeAdded) {
		this.timeAdded = timeAdded;
	}

}
