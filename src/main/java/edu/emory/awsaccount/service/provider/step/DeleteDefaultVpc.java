/*******************************************************************************
 $Source: $
 $Revision: $
 *******************************************************************************/

/******************************************************************************
 This file is part of the Emory AWS Account Service.

 Copyright (C) 2017 Emory University. All rights reserved.
 ******************************************************************************/
package edu.emory.awsaccount.service.provider.step;

import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.openeai.config.AppConfig;

import com.amazon.aws.moa.objects.resources.v1_0.Property;
import com.amazonaws.SdkClientException;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.AmazonEC2Exception;
import com.amazonaws.services.ec2.model.DeleteDhcpOptionsRequest;
import com.amazonaws.services.ec2.model.DeleteInternetGatewayRequest;
import com.amazonaws.services.ec2.model.DeleteNetworkAclRequest;
import com.amazonaws.services.ec2.model.DeleteRouteTableRequest;
import com.amazonaws.services.ec2.model.DeleteSecurityGroupRequest;
import com.amazonaws.services.ec2.model.DeleteSubnetRequest;
import com.amazonaws.services.ec2.model.DeleteVpcRequest;
import com.amazonaws.services.ec2.model.DescribeInternetGatewaysRequest;
import com.amazonaws.services.ec2.model.DescribeInternetGatewaysResult;
import com.amazonaws.services.ec2.model.DescribeNetworkAclsRequest;
import com.amazonaws.services.ec2.model.DescribeNetworkAclsResult;
import com.amazonaws.services.ec2.model.DescribeRouteTablesRequest;
import com.amazonaws.services.ec2.model.DescribeRouteTablesResult;
import com.amazonaws.services.ec2.model.DescribeSecurityGroupsRequest;
import com.amazonaws.services.ec2.model.DescribeSecurityGroupsResult;
import com.amazonaws.services.ec2.model.DescribeSubnetsRequest;
import com.amazonaws.services.ec2.model.DescribeSubnetsResult;
import com.amazonaws.services.ec2.model.DescribeVpcsRequest;
import com.amazonaws.services.ec2.model.DescribeVpcsResult;
import com.amazonaws.services.ec2.model.DetachInternetGatewayRequest;
import com.amazonaws.services.ec2.model.DisassociateRouteTableRequest;
import com.amazonaws.services.ec2.model.Filter;
import com.amazonaws.services.ec2.model.InternetGateway;
import com.amazonaws.services.ec2.model.NetworkAcl;
import com.amazonaws.services.ec2.model.RouteTable;
import com.amazonaws.services.ec2.model.RouteTableAssociation;
import com.amazonaws.services.ec2.model.SecurityGroup;
import com.amazonaws.services.ec2.model.Subnet;
import com.amazonaws.services.ec2.model.Vpc;
import com.amazonaws.services.securitytoken.model.AWSSecurityTokenServiceException;

import  edu.emory.awsaccount.service.AwsClientBuilderHelper;
import edu.emory.awsaccount.service.provider.VirtualPrivateCloudProvisioningProvider;

/**
 * If a this is a request for a new VPC in an existing account,
 * send RoleAssignment.Query-Request to determine if the user
 * is an account administrator or central administrator of the
 * account.
 * <P>
 *
 * @author Tom Cervenka (tcerven@emory.edu)
 * @version 1.0 - 15 December 2023
 **/
public class DeleteDefaultVpc extends AbstractStep implements Step {

	/**
	 * Some regions, like us-iso-east-1, cause long connection attempts that ultimately timeout
	 * so we want to ignore them completely.  Other regions like the us-gov regions are non-public and can be ignored.
	 */
	private final Set<String> ignoredRegionNames = new HashSet<>();
	private String accessKeyId;
	private String secretKey;



	public void init (String provisioningId, Properties props, AppConfig aConfig, VirtualPrivateCloudProvisioningProvider vpcpp) throws StepException {

		super.init(provisioningId, props, aConfig, vpcpp);


		String LOGTAG = getStepTag() + "[DeleteDefaultVpc.init] ";

		logger.info(LOGTAG + "Getting custom step properties...");

		accessKeyId = getProperties().getProperty("accessKeyId");
		logger.info(LOGTAG+"accessKeyId: "+accessKeyId);
		secretKey = getProperties().getProperty("secretKey");

		if (secretKey == null || secretKey.replaceAll("\\s","").isEmpty() ) {
			String errMsg = "secretKey not found";
			logger.error(LOGTAG + errMsg );
			throw new StepException(errMsg);
		}
		logger.info(LOGTAG+"secretKey is present");

		String property = getProperties().getProperty("ignoredRegions");

		if (property != null && !property.isEmpty()) {
			String[] ignoredRegions = property.replaceAll("[\n\t\r ]", "").split(",");
			for (String ignoredRegion : ignoredRegions)
				if (!ignoredRegion.isEmpty())
					this.ignoredRegionNames.add(ignoredRegion);
		}

		logger.info(LOGTAG+"ignoredRegionNames: "+ignoredRegionNames);

		logger.info(LOGTAG + "Initialization complete. ");

	}

	protected List<Property> run() throws StepException {
		long startTime = System.currentTimeMillis();

		String LOGTAG = getStepTag() + "[DeleteDefaultVpc.run] ";
		logger.info(LOGTAG + "Begin running the step.");
		
		//DETERMINE_NEW_OR_EXISTING_ACCOUNT
		String allocateNewAccount = getStepPropertyValue("DETERMINE_NEW_OR_EXISTING_ACCOUNT", "allocateNewAccount");
		addResultProperty("allocateNewAccount", allocateNewAccount);
		logger.info(LOGTAG+"newAccount is "+allocateNewAccount);
		
		if ("false".equals(allocateNewAccount)) {
			addResultProperty("Step SKIPPED", "This is not a new account");
			logger.info(LOGTAG+"Step SKIPPED -- This is not a new account");	
			update(COMPLETED_STATUS, SUCCESS_RESULT);
			return getResultProperties();
		}	


		addResultProperty("stepExecutionMethod", RUN_EXEC_TYPE);

		String accountId = getStepPropertyValue("GENERATE_NEW_ACCOUNT", "newAccountId"); //TODO Is this the only way to get the account ID? 
		addResultProperty("newAccountId", accountId);
		logger.info(LOGTAG+"newAccountId is "+accountId);

		try {

			// VPCs are unconditionally created in every AWS region during provisioning so we want
			// to scan all of them and not just the RHEDcloud regions
			for (Region region : RegionUtils.getRegions()) {
				if (ignoredRegionNames.contains(region.getName())) {
					logger.info(LOGTAG + "Ignoring region: " + region.getName());
					continue;
				}
				String roleArnPattern = "arn:aws:iam::ACCOUNT_NUMBER:role/rhedcloud/RHEDcloudAwsAccountServiceRole";
				AmazonEC2 ec2 = null;
				try {
					ec2 = AwsClientBuilderHelper.buildAmazonEC2Client(
							accountId,
							region.toString(),
							accessKeyId, secretKey, 
							roleArnPattern, 
							900);
				} catch (AWSSecurityTokenServiceException e) {
					System.err.println("AWSSecurityTokenServiceException for region "+region);
					continue;
				} catch (SdkClientException e) {
					System.err.println("SdkClientException for region "+region);
					continue;
				}

				DescribeVpcsResult mainDescribeVpcsResult = ec2.describeVpcs();
				logger.info(LOGTAG+"Number of VPCs described is "+mainDescribeVpcsResult.getVpcs().size());

				// and compare - anything unregistered is flagged as a risk
				for (Vpc vpc : mainDescribeVpcsResult.getVpcs()) {
					String vpcId = null;
					if (vpc.isDefault()) {
						vpcId = vpc.getVpcId();
				        // Internet Gateways
				        DescribeInternetGatewaysRequest describeInternetGatewaysRequest = new DescribeInternetGatewaysRequest()
				                .withFilters(new Filter().withName("attachment.vpc-id").withValues(vpcId));
				        DescribeInternetGatewaysResult describeInternetGatewaysResult = ec2.describeInternetGateways(describeInternetGatewaysRequest);
				        for (InternetGateway nr: describeInternetGatewaysResult.getInternetGateways()) {
				            try {
				                DetachInternetGatewayRequest detachInternetGatewayRequest = new DetachInternetGatewayRequest()
				                        .withInternetGatewayId(nr.getInternetGatewayId())
				                        .withVpcId(vpcId);
				                ec2.detachInternetGateway(detachInternetGatewayRequest);
				                logger.info(LOGTAG+" -- detach internet gateway: "+nr);
				            } catch (AmazonEC2Exception e) {
				            	logger.warn(LOGTAG+" -- unable to detach internet gateway: "+". AWS Error: "+e.getMessage());
				            }
				            try {
				                DeleteInternetGatewayRequest deleteInternetGatewayRequest = new DeleteInternetGatewayRequest()
				                        .withInternetGatewayId(nr.getInternetGatewayId());
				                ec2.deleteInternetGateway(deleteInternetGatewayRequest);
				                logger.info(LOGTAG+" -- delete internet gateway: "+nr);
				            } catch (AmazonEC2Exception e) {
				            	logger.warn(LOGTAG+" -- unable to delete internet gateway: "+nr+". AWS Error: "+e.getMessage());
				            }
				        }


						// Subnets
						// a common filter for network resource searches and diagnostic info in case of errors
						Filter vpcIdFilter = new Filter().withName("vpc-id").withValues(vpcId );
						DescribeSubnetsRequest describeSubnetsRequest = new DescribeSubnetsRequest()
								.withFilters(vpcIdFilter);
						DescribeSubnetsResult describeSubnetsResult = ec2.describeSubnets(describeSubnetsRequest);
						for (Subnet nr: describeSubnetsResult.getSubnets()) {
							try {
								DeleteSubnetRequest deleteSubnetRequest = new DeleteSubnetRequest()
										.withSubnetId(nr.getSubnetId());
								ec2.deleteSubnet(deleteSubnetRequest);
								logger.info(LOGTAG+" -- delete subnet: "+nr);
							} catch (AmazonEC2Exception e) {
								logger.warn(LOGTAG+" -- unable to delete subnet: "+nr+". AWS Error: "+e.getMessage());
							}				        	
						}
						
				        // Network ACLs - delete the non-default ones
				        DescribeNetworkAclsRequest describeNetworkAclsRequest = new DescribeNetworkAclsRequest()
				                .withFilters(vpcIdFilter);
				        DescribeNetworkAclsResult describeNetworkAclsResult = ec2.describeNetworkAcls(describeNetworkAclsRequest);
				        for ( NetworkAcl nr: describeNetworkAclsResult.getNetworkAcls()) {
				        	if (!nr.isDefault()) {
					            try {
					                DeleteNetworkAclRequest deleteNetworkAclRequest = new DeleteNetworkAclRequest()
					                        .withNetworkAclId(nr.getNetworkAclId());
					                ec2.deleteNetworkAcl(deleteNetworkAclRequest);
					                logger.info(LOGTAG+" -- delete network ACL: "+nr);
					            } catch (AmazonEC2Exception e) {
					            	logger.warn(LOGTAG+" -- unable to delete network ACL: . AWS Error: "+e.getMessage());
					            }
				        	}
				        }

						// Route Tables
						DescribeRouteTablesRequest describeRouteTablesRequest = new DescribeRouteTablesRequest()
								.withFilters(vpcIdFilter);
						DescribeRouteTablesResult describeRouteTablesResult;
						do {
							describeRouteTablesResult = ec2.describeRouteTables(describeRouteTablesRequest);
							for( RouteTable nr: describeRouteTablesResult.getRouteTables()) {
								for(RouteTableAssociation ass: nr.getAssociations()) {
									if (ass.isMain()) {
										try {
											DisassociateRouteTableRequest disassociateRouteTableRequest = new DisassociateRouteTableRequest()
													.withAssociationId(ass.getRouteTableAssociationId());
											ec2.disassociateRouteTable(disassociateRouteTableRequest);
											logger.info(LOGTAG+" -- disassociate route table: "+nr);
										} catch (AmazonEC2Exception e) {
											logger.warn(LOGTAG+" -- unable to disassociate route table: "+nr+". AWS Error: "+e.getMessage());
										}
									}
								}
								try {
									DeleteRouteTableRequest deleteRouteTableRequest = new DeleteRouteTableRequest()
											.withRouteTableId(nr.getRouteTableId());
									ec2.deleteRouteTable(deleteRouteTableRequest);
									logger.info(LOGTAG+" -- delete route table: "+nr);
								} catch (AmazonEC2Exception e) {
									logger.warn(LOGTAG+" -- unable to delete route table: "+nr+". AWS Error: "+e.getMessage());
								}				            	
							}
							describeRouteTablesRequest.setNextToken(describeRouteTablesResult.getNextToken());
						} while (describeRouteTablesResult.getNextToken() != null);
						
				        // Security Groups
				        DescribeSecurityGroupsRequest describeSecurityGroupsRequest = new DescribeSecurityGroupsRequest()
				                .withFilters(vpcIdFilter);
				        DescribeSecurityGroupsResult describeSecurityGroupsResult;
				        do {
				            describeSecurityGroupsResult = ec2.describeSecurityGroups(describeSecurityGroupsRequest);
				            
							for (SecurityGroup nr: describeSecurityGroupsResult.getSecurityGroups()) {
				            	
				                DeleteSecurityGroupRequest deleteSecurityGroupRequest = new DeleteSecurityGroupRequest()
				                        .withGroupId(nr.getGroupId());
				                try {
				                    ec2.deleteSecurityGroup(deleteSecurityGroupRequest);
				                    logger.info(LOGTAG+" -- delete security group: "+nr);
				                } catch (AmazonEC2Exception e) {
				                	logger.warn(LOGTAG+" -- unable to delete security group: "+nr+". AWS Error: "+e.getMessage());
				                }
				            }

				            describeSecurityGroupsRequest.setNextToken(describeSecurityGroupsResult.getNextToken());
				        } while (describeSecurityGroupsResult.getNextToken() != null);
				        
				        // DHCP Options Sets
				        // can only be deleted after the VPC is deleted, so get the DHCP options set ID and delete below
				        DescribeVpcsRequest describeVpcsRequest = new DescribeVpcsRequest()
				                .withVpcIds(vpcId);
				        DescribeVpcsResult describeVpcsResult = ec2.describeVpcs(describeVpcsRequest);

						ec2.deleteVpc(new DeleteVpcRequest(vpcId));
						String logmsg = "Deleted VPC with ID, "+vpcId+", in region, "+region;
						logger.info(LOGTAG+logmsg);
						addResultProperty("Deleted "+vpc.getVpcId(),logmsg);
						
			            // now that the VPC is gone we can delete the DHCP options set
			            // but failure here doesn't change the overall success status though it is logged
						for(Vpc nr: describeVpcsResult.getVpcs()) {
			                try {
			                    DeleteDhcpOptionsRequest deleteDhcpOptionsRequest = new DeleteDhcpOptionsRequest()
			                            .withDhcpOptionsId(nr.getDhcpOptionsId());
			                    ec2.deleteDhcpOptions(deleteDhcpOptionsRequest);
			                    logger.info(LOGTAG+" -- delete DHCP options set: "+nr);
			                } catch (AmazonEC2Exception e) {
			                	logger.warn(LOGTAG+" -- unable to delete DHCP options set: "+". AWS Error: "+e.getMessage());
			                }
			            }
					} else {
						String logmsg = "Skipped non-default VPC with ID, "+vpc.getVpcId()+", in region, "+region;
						logger.info(LOGTAG+logmsg);
						addResultProperty("Deleted "+vpcId,logmsg);            		
					}
				}
			}
			// Update the step.
			update(COMPLETED_STATUS, SUCCESS_RESULT);

		} catch (RuntimeException ecoe) {
			ecoe.printStackTrace();
			String errMsg = "An error occurred. The exception is: " + ecoe.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new StepException(errMsg, ecoe);
		}

		// Log completion time.
		long time = System.currentTimeMillis() - startTime;
		logger.info(LOGTAG + "Step simulation completed in " + time + "ms.");

		return getResultProperties();
	}

	protected List<Property> simulate() throws StepException {
		long startTime = System.currentTimeMillis();
		String LOGTAG = getStepTag() + "[DeleteDefaultVpc.simulate] ";
		logger.info(LOGTAG + "Begin step simulation.");

		// Set return properties.
		addResultProperty("stepExecutionMethod", SIMULATED_EXEC_TYPE);
		addResultProperty("isAuthorized", "true");

		// Update the step.
		update(COMPLETED_STATUS, SUCCESS_RESULT);

		// Log completion time.
		long time = System.currentTimeMillis() - startTime;
		logger.info(LOGTAG + "Step simulation completed in " + time + "ms.");

		// Return the properties.
		return getResultProperties();
	}

	protected List<Property> fail() throws StepException {
		long startTime = System.currentTimeMillis();
		String LOGTAG = getStepTag() + "[DeleteDefaultVpc.fail] ";
		logger.info(LOGTAG + "Begin step failure simulation.");

		// Set return properties.
		addResultProperty("stepExecutionMethod", FAILURE_EXEC_TYPE);

		// Update the step.
		update(COMPLETED_STATUS, FAILURE_RESULT);

		// Log completion time.
		long time = System.currentTimeMillis() - startTime;
		logger.info(LOGTAG + "Step failure simulation completed in " + time + "ms.");

		// Return the properties.
		return getResultProperties();
	}

	public void rollback() throws StepException {

		super.rollback();

		long startTime = System.currentTimeMillis();
		String LOGTAG = getStepTag() + "[DeleteDefaultVpc.rollback] ";
		logger.info(LOGTAG + "Rollback called, but this step has nothing to roll back.");
		update(ROLLBACK_STATUS, SUCCESS_RESULT);

		// Log completion time.
		long time = System.currentTimeMillis() - startTime;
		logger.info(LOGTAG + "Rollback completed in " + time + "ms.");
	}



}
