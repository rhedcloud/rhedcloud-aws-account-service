/*******************************************************************************
 $Source: $
 $Revision: $
 *******************************************************************************/

/******************************************************************************
 This file is part of the Emory AWS Account Service.

 Copyright (C) 2017 Emory University. All rights reserved.
 ******************************************************************************/
package edu.emory.awsaccount.service.provider.step;

import java.util.List;
import java.util.Properties;

import org.openeai.config.AppConfig;

import com.amazon.aws.moa.objects.resources.v1_0.Property;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.ModifyVpcEndpointServicePermissionsRequest;
import com.amazonaws.services.securitytoken.AWSSecurityTokenService;
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClientBuilder;
import com.amazonaws.services.securitytoken.model.AssumeRoleRequest;
import com.amazonaws.services.securitytoken.model.AssumeRoleResult;
import com.amazonaws.services.securitytoken.model.Credentials;

import edu.emory.awsaccount.service.provider.VirtualPrivateCloudProvisioningProvider;

/**
 * If this is a new account, modify the vpc endpoint permissions in the security account.
 * <P>
 *
 * @author Tod Jackson (jtjacks@emory.edu)
 * @version 1.0 - 22 February 2022
 **/
public class CreateVpcEndpointServicePermissions extends AbstractStep implements Step {
    private String m_accessKeyId = null;
    private String m_secretKey = null;
    private String roleAssumptionArn = null;
    private int m_roleAssumptionDurationSeconds = 0;
    private String modifyVpcEndpointServicePermissionsArnPattern=null;

    public void init (String provisioningId, Properties props,
            AppConfig aConfig, VirtualPrivateCloudProvisioningProvider vpcpp)
            throws StepException {

        super.init(provisioningId, props, aConfig, vpcpp);

        String LOGTAG = getStepTag() + "[ModifyVpcEndpointServicePermissions.init] ";

        // Get custom step properties.
        logger.info(LOGTAG + "Getting custom step properties...");

        // Access key
        String accessKeyId = getProperties().getProperty("accessKeyId", null);
        setAccessKeyId(accessKeyId);
        logger.info(LOGTAG + "accessKeyId is: " + getAccessKeyId());

        // Secret key
        String secretKey = getProperties().getProperty("secretKey", null);
        setSecretKey(secretKey);
        logger.info(LOGTAG + "secretKey is: present");

        // Set the roleAssumptionArn property
        setRoleAssumptionArn(getProperties().getProperty("roleAssumptionArn", null));
        logger.info(LOGTAG + "roleAssumptionArn property is: " + getRoleAssumptionArn());

        // Set the roleAssumptionDurationSeconds property
        setRoleAssumptionDurationSeconds(getProperties().getProperty("roleAssumptionDurationSeconds", null));
        logger.info(LOGTAG + "roleAssumptionDurationSeconds is: " + getRoleAssumptionDurationSeconds());
        
        modifyVpcEndpointServicePermissionsArnPattern = getProperties().getProperty("modifyVpcEndpointServicePermissionsArnPattern", null);
        logger.info(LOGTAG + "modifyVpcEndpointServicePermissionsArnPattern is: " + modifyVpcEndpointServicePermissionsArnPattern);

        logger.info(LOGTAG + "Initialization complete.");
    }

    protected List<Property> run() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[ModifyVpcEndpointServicePermissions.run] ";
        logger.info(LOGTAG + "Begin running the step.");

        // Return properties
        addResultProperty("stepExecutionMethod", RUN_EXEC_TYPE);

        // Get some properties from previous steps.
        String allocateNewAccount = getStepPropertyValue("GENERATE_NEW_ACCOUNT", "allocateNewAccount");
        String newAccountId = getStepPropertyValue("GENERATE_NEW_ACCOUNT", "newAccountId");
        String accountId=null;
        String region = getVirtualPrivateCloudProvisioning().getVirtualPrivateCloudRequisition().getRegion();
        String endPointServiceId = getProperties().getProperty("glbEndpointServiceId-" + region, null);
        logger.info(LOGTAG + "endPointServiceId is: " + endPointServiceId);

        boolean allocatedNewAccount = Boolean.parseBoolean(allocateNewAccount) ;
        logger.info(LOGTAG + "allocatedNewAccount: " + allocatedNewAccount);
        logger.info(LOGTAG + "newAccountId: " + newAccountId);
        
        boolean modifiedEndPointPermissions = false;

        // If allocateNewAccount is true and newAccountId is not null,
        // it's a new account
        if (allocatedNewAccount && 
        	(newAccountId != null && !newAccountId.equals(PROPERTY_VALUE_NOT_AVAILABLE))) {
            logger.info(LOGTAG + "allocatedNewAccount is true and newAccountId " +
                    "is not null. Modifying VPC endpoint service permissions in security account.");

            accountId=newAccountId;
        }
        // If allocateNewAccount is false, we're adding a VPC to an existing account
        // so we'll need to get the account id and set the permissions
        else {
            logger.info(LOGTAG + "allocateNewAccount is false. adding a VPC to an existing account.");
            accountId = getStepPropertyValue("DETERMINE_NEW_OR_EXISTING_ACCOUNT", "accountId");
        }
        
        if (accountId != null && accountId.length() > 0) {
            // Build the EC2 client.
            AmazonEC2Client client = buildAmazonEC2Client(accountId, region);

            // Build the request.
            String roleArn = modifyVpcEndpointServicePermissionsArnPattern.replace("ACCOUNT_NUMBER", accountId);
            logger.info(LOGTAG + "roleArn is: " + roleArn);
            ModifyVpcEndpointServicePermissionsRequest request = new ModifyVpcEndpointServicePermissionsRequest();
            request
            	.withAddAllowedPrincipals(roleArn)
            	.withServiceId(endPointServiceId);

            try {
                logger.info(LOGTAG + "Sending the ModifyVpcEndpointServicePermissions request...");
                long queryStartTime = System.currentTimeMillis();
                client.modifyVpcEndpointServicePermissions(request);
                long queryTime = System.currentTimeMillis() - queryStartTime;
                logger.info(LOGTAG + "received response to ModifyVpcEndpointServicePermissions request in " + queryTime + " ms.");
                modifiedEndPointPermissions=true;
                addResultProperty("modifiedEndPointPermissions", "true");
            } catch (Exception e) {
                String errMsg = "An error occurred in the ModifyVpcEndpointServicePermissions request. The exception is: " + e.getMessage();
                logger.error(LOGTAG + errMsg);
                throw new StepException(errMsg, e);
            }
        }
        else {
        	// there's a problem
            String errMsg = "Unable to determine account id.  This is a problem.  Processing cannot continue";
            logger.error(LOGTAG + errMsg);
        }

        // Update the step.
        if (modifiedEndPointPermissions == true) {
            update(COMPLETED_STATUS, SUCCESS_RESULT);
        }
        else update(COMPLETED_STATUS, FAILURE_RESULT);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step run completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    protected List<Property> simulate() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[ModifyVpcEndpointServicePermissions.simulate] ";
        logger.info(LOGTAG + "Begin step simulation.");

        // Set return properties.
        addResultProperty("stepExecutionMethod", SIMULATED_EXEC_TYPE);

        // Update the step.
        update(COMPLETED_STATUS, SUCCESS_RESULT);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step simulation completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    protected List<Property> fail() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[ModifyVpcEndpointServicePermissions.fail] ";
        logger.info(LOGTAG + "Begin step failure simulation.");

        // Set return properties.
        addResultProperty("stepExecutionMethod", FAILURE_EXEC_TYPE);

        // Update the step.
        update(COMPLETED_STATUS, FAILURE_RESULT);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step failure simulation completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    public void rollback() throws StepException {
        long startTime = System.currentTimeMillis();

        super.rollback();

        String LOGTAG = getStepTag() + "[ModifyVpcEndpointServicePermissions.rollback] ";

        String newAccountId = getStepPropertyValue("GENERATE_NEW_ACCOUNT", "newAccountId");
        String accountId=null;
        String region = getVirtualPrivateCloudProvisioning().getVirtualPrivateCloudRequisition().getRegion();
        String endPointServiceId = getProperties().getProperty("glbEndpointServiceId-" + region, null);
        logger.info(LOGTAG + "endPointServiceId is: " + endPointServiceId);
        
        if ((newAccountId != null && !newAccountId.equals(PROPERTY_VALUE_NOT_AVAILABLE))) {
            logger.info(LOGTAG + "newAccountId " +
                        "is not null. Deleting VPC endpoint service permissions from security account.");

                accountId=newAccountId;
        }
        // If allocateNewAccount is false, we're adding a VPC to an existing account
        // so we'll need to get the account id and remove the permissions
        else {
            logger.info(LOGTAG + "allocateNewAccount is false. adding a VPC to an existing account.");
            accountId = getStepPropertyValue("DETERMINE_NEW_OR_EXISTING_ACCOUNT", "accountId");
        }

        if (accountId != null && accountId.length() > 0) {
            // Build the EC2 client.
            AmazonEC2Client client = buildAmazonEC2Client(accountId, region);

            // Build the request.
            String roleArn = modifyVpcEndpointServicePermissionsArnPattern.replace("ACCOUNT_NUMBER", accountId);
            ModifyVpcEndpointServicePermissionsRequest request = new ModifyVpcEndpointServicePermissionsRequest();
            request
            	.withRemoveAllowedPrincipals(roleArn)
            	.withServiceId(endPointServiceId);

            try {
                logger.info(LOGTAG + "Sending the ModifyVpcEndpointServicePermissions request...");
                long queryStartTime = System.currentTimeMillis();
                client.modifyVpcEndpointServicePermissions(request);
                long queryTime = System.currentTimeMillis() - queryStartTime;
                logger.info(LOGTAG + "received response to ModifyVpcEndpointServicePermissions request in " + queryTime + " ms.");
            } catch (Exception e) {
                String errMsg = "An error occurred in the ModifyVpcEndpointServicePermissions request. The exception is: " + e.getMessage();
                logger.error(LOGTAG + errMsg);
            }
        }
        else {
        	// there's a problem
            String errMsg = "Unable to determine account id.  This is a problem.  Processing cannot continue";
            logger.error(LOGTAG + errMsg);
        }

        update(ROLLBACK_STATUS, SUCCESS_RESULT);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Rollback completed in " + time + "ms.");
    }

    private void setAccessKeyId (String accessKeyId) throws StepException {
        if (accessKeyId == null) {
            String errMsg = "accessKeyId property is null. Can't continue.";
            throw new StepException(errMsg);
        }

        m_accessKeyId = accessKeyId;
    }

    private String getAccessKeyId() {
        return m_accessKeyId;
    }

    private void setSecretKey (String secretKey) throws StepException {
        if (secretKey == null) {
            String errMsg = "secretKey property is null. Can't continue.";
            throw new StepException(errMsg);
        }

        m_secretKey = secretKey;
    }

    private String getSecretKey() {
        return m_secretKey;
    }

    private void setRoleAssumptionArn(String roleArn) throws StepException {
        if (roleArn == null) {
            String errMsg = "roleAssumptionArn property is null. Can't assume role in target accounts. Can't continue.";
            throw new StepException(errMsg);
        }

        roleAssumptionArn = roleArn;
    }

    private String getRoleAssumptionArn() {
        return roleAssumptionArn;
    }

    private void setRoleAssumptionDurationSeconds(String seconds) throws StepException {
        if (seconds == null) {
            String errMsg = "roleAssumptionDurationSeconds property is null. Can't continue.";
            throw new StepException(errMsg);
        }

        m_roleAssumptionDurationSeconds = Integer.parseInt(seconds);
    }


    private int getRoleAssumptionDurationSeconds() {
        return m_roleAssumptionDurationSeconds;
    }

    private AmazonEC2Client buildAmazonEC2Client(String accountId, String region) {
        String LOGTAG = getStepTag() + "[buildAmazonEC2Client] ";

        // Build the roleArn of the role to assume from the base ARN and
        // the account number in the query spec.
        logger.info(LOGTAG + "The account targeted by this request is: " + accountId);
        logger.info(LOGTAG + "The roleAssumptionArn is: " + getRoleAssumptionArn());
        logger.info(LOGTAG + "The region targeted by this request is: " + region);

        // Instantiate a basic credential provider
        BasicAWSCredentials creds = new BasicAWSCredentials(getAccessKeyId(), getSecretKey());
        AWSStaticCredentialsProvider cp = new AWSStaticCredentialsProvider(creds);

        // Create the STS client
        AWSSecurityTokenService sts = AWSSecurityTokenServiceClientBuilder.standard()
                                        .withCredentials(cp)
                                        .withRegion(region)
                                        .build();

        // Assume the appropriate role in the appropriate account.
        AssumeRoleRequest assumeRequest = new AssumeRoleRequest()
        	.withRoleArn(getRoleAssumptionArn())
            .withDurationSeconds(getRoleAssumptionDurationSeconds())
            .withRoleSessionName("AwsAccountService");

        AssumeRoleResult assumeResult = sts.assumeRole(assumeRequest);
        Credentials credentials = assumeResult.getCredentials();

        // Instantiate a credential provider
        BasicSessionCredentials temporaryCredentials = new BasicSessionCredentials(credentials.getAccessKeyId(), credentials.getSecretAccessKey(), credentials.getSessionToken());
        AWSStaticCredentialsProvider credProvider = new AWSStaticCredentialsProvider(temporaryCredentials);

        // Create the EC2 client
        return (AmazonEC2Client)AmazonEC2ClientBuilder.standard()
                .withCredentials(credProvider)
                .withRegion(region)
                .build();
    }
}
