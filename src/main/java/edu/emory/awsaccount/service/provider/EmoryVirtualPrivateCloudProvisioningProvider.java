/*******************************************************************************
 $Source: $
 $Revision: $
 *******************************************************************************/

/******************************************************************************
 This file is part of the Emory AWS Account Service.

 Copyright (C) 2017 Emory University. All rights reserved.
 ******************************************************************************/

package edu.emory.awsaccount.service.provider;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.jms.JMSException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.config.PropertyConfig;
import org.openeai.jms.producer.MessageProducer;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.moa.EnterpriseObjectCreateException;
import org.openeai.moa.EnterpriseObjectGenerateException;
import org.openeai.moa.EnterpriseObjectQueryException;
import org.openeai.moa.EnterpriseObjectUpdateException;
import org.openeai.moa.XmlEnterpriseObjectException;
import org.openeai.threadpool.ThreadPool;
import org.openeai.threadpool.ThreadPoolException;
import org.openeai.transport.RequestService;
import org.openeai.utils.sequence.Sequence;
import org.openeai.utils.sequence.SequenceException;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.AccountDeprovisioning;
import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.VirtualPrivateCloudProvisioning;
import com.amazon.aws.moa.jmsobjects.user.v1_0.UserNotification;
import com.amazon.aws.moa.objects.resources.v1_0.AccountDeprovisioningQuerySpecification;
import com.amazon.aws.moa.objects.resources.v1_0.AccountDeprovisioningRequisition;
import com.amazon.aws.moa.objects.resources.v1_0.Datetime;
import com.amazon.aws.moa.objects.resources.v1_0.DeprovisioningStep;
import com.amazon.aws.moa.objects.resources.v1_0.Property;
import com.amazon.aws.moa.objects.resources.v1_0.ProvisioningStep;
import com.amazon.aws.moa.objects.resources.v1_0.VirtualPrivateCloudProvisioningQuerySpecification;
import com.amazon.aws.moa.objects.resources.v1_0.VirtualPrivateCloudRequisition;
import com.service_now.moa.jmsobjects.servicedesk.v2_0.Incident;
import com.service_now.moa.objects.resources.v2_0.IncidentRequisition;

import edu.emory.awsaccount.service.provider.step.AbstractStep;
import edu.emory.awsaccount.service.provider.step.Step;
import edu.emory.awsaccount.service.provider.step.StepException;
import edu.emory.moa.jmsobjects.identity.v1_0.RoleAssignment;
import edu.emory.moa.objects.resources.v1_0.RoleAssignmentQuerySpecification;

/**
 * A provider that maintains provisions AWS accounts and VPC
 * in Emory infrastructure and AWS.
 *
 * @author Steve Wheat (swheat@emory.edu)
 */
public class EmoryVirtualPrivateCloudProvisioningProvider extends OpenEaiObject
implements VirtualPrivateCloudProvisioningProvider {

	private final Logger logger = LogManager.getLogger(EmoryVirtualPrivateCloudProvisioningProvider.class);;
	private AppConfig m_appConfig;
	private boolean m_verbose = false;
	private Sequence m_provisioningIdSequence = null;
	private String m_centralAdminRoleDn = null;
	private ProducerPool m_awsAccountServiceProducerPool = null;
	private ProducerPool m_idmServiceProducerPool = null;
	private ProducerPool m_serviceNowServiceProducerPool = null;
	private ThreadPool m_threadPool = null;
	private final int m_threadPoolSleepInterval = 1000;
	private final String LOGTAG = "[EmoryVirtualPrivateCloudProvisioningProvider] ";
	protected String COMPLETED_STATUS = "completed";
	protected String PENDING_STATUS = "pending";
	protected String ROLLBACK_STATUS = "rolled back";
	protected String SUCCESS_RESULT = "success";
	protected String FAILURE_RESULT = "failure";

	// TJ:11/12/2020 - Sprint 4.15
	private Properties incidentProperties;
	private String m_centralAdminPpid;

	@SuppressWarnings({ "unchecked" })
	@Override
	public void init(AppConfig aConfig) throws ProviderException {
		logger.info(LOGTAG + "Initializing...");
		setAppConfig(aConfig);

		// TJ:11/12/2020 - Sprint 4.15
		// get the incident properties that holds static incident data
		try {
			incidentProperties = aConfig.getProperties("ProvisioningFailureIncidentProperties");
		} catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			String errMsg = "Error retrieving a 'IncidentProperties' object from "
					+ "AppConfig: The exception is: " + e.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, e);
		}

		// Get the provider properties
		PropertyConfig pConfig;
		try {
			pConfig = (PropertyConfig) aConfig
					.getObject("VirtualPrivateCloudProvisioningProviderProperties");
		} catch (EnterpriseConfigurationObjectException eoce) {
			String errMsg = "Error retrieving a PropertyConfig object from "
					+ "AppConfig: The exception is: " + eoce.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, eoce);
		}

		Properties props = pConfig.getProperties();
		setProperties(props);
		logger.info(LOGTAG + getProperties().toString());

		// Set the verbose property.
		setVerbose(Boolean.parseBoolean(getProperties().getProperty("verbose", "false")));
		logger.info(LOGTAG + "Verbose property is: " + getVerbose());

		// Set the verbose property.
		setCentralAdminRoleDn(getProperties().getProperty("centralAdminRoleDn", null));
		logger.info(LOGTAG + "centralAdminRoleDn is: " + getCentralAdminRoleDn());

		// Set the centralAdmin PPID to use for deprovisioning generation
		setCentralAdminPpid(getProperties().getProperty("centralAdminPpid"));
		if (getCentralAdminPpid()==null) {
			String errMsg = "Can't find centralAdminPpid property.";
			logger.fatal(LOGTAG + errMsg);
			throw new ProviderException(errMsg);			
		}

		// Get the sequences to use.
		// This provider needs a sequence to generate a unique ProvisioningId
		// for each transaction in multiple threads and multiple instances.
		Sequence seq;
		try {
			seq = (Sequence) getAppConfig().getObject("ProvisioningIdSequence");
			setProvisioningIdSequence(seq);
		} catch (EnterpriseConfigurationObjectException ecoe) {
			// An error occurred retrieving an object from AppConfig. Log it and
			// throw an exception.
			String errMsg = "An error occurred retrieving an object from " +
					"AppConfig. The exception is: " + ecoe.getMessage();
			logger.fatal(LOGTAG + errMsg);
			throw new ProviderException(errMsg);
		}

		// This provider needs to send messages to the AWS account service
		// to initialize provisioning transactions.
		ProducerPool p2p1;
		try {
			p2p1 = (ProducerPool) getAppConfig()
					.getObject("AwsAccountServiceProducerPool");
			setAwsAccountServiceProducerPool(p2p1);
		} catch (EnterpriseConfigurationObjectException ecoe) {
			// An error occurred retrieving an object from AppConfig. Log it and
			// throw an exception.
			String errMsg = "An error occurred retrieving an object from " +
					"AppConfig. The exception is: " + ecoe.getMessage();
			logger.fatal(LOGTAG + errMsg);
			throw new ProviderException(errMsg);
		}

		// This provider needs to send messages to the ServiceNow service
		// to initialize provisioning transactions.
		ProducerPool p2p2;
		try {
			p2p2 = (ProducerPool) getAppConfig()
					.getObject("ServiceNowServiceProducerPool");
			setServiceNowServiceProducerPool(p2p2);
		} catch (EnterpriseConfigurationObjectException ecoe) {
			// An error occurred retrieving an object from AppConfig. Log it and
			// throw an exception.
			String errMsg = "An error occurred retrieving an object from " +
					"AppConfig. The exception is: " + ecoe.getMessage();
			logger.fatal(LOGTAG + errMsg);
			throw new ProviderException(errMsg);
		}

		// This provider needs to send messages to the IdmService service
		// to query for RoleAssignments.
		ProducerPool p2p3;
		try {
			p2p3 = (ProducerPool) getAppConfig()
					.getObject("IdmServiceProducerPool");
			setIdmServiceProducerPool(p2p3);
		} catch (EnterpriseConfigurationObjectException ecoe) {
			// An error occurred retrieving an object from AppConfig. Log it and
			// throw an exception.
			String errMsg = "An error occurred retrieving an object from " +
					"AppConfig. The exception is: " + ecoe.getMessage();
			logger.fatal(LOGTAG + errMsg);
			throw new ProviderException(errMsg);
		}

		// Get the ThreadPool pool to use.
		// This provider needs a thread pool in which to process concurrent
		// provisioning transactions.
		ThreadPool tp;
		try {
			tp = (ThreadPool) getAppConfig().getObject("VpcProcessingThreadPool");
			setThreadPool(tp);
		} catch (EnterpriseConfigurationObjectException ecoe) {
			// An error occurred retrieving an object from AppConfig. Log it and
			// throw an exception.
			String errMsg = "An error occurred retrieving an object from " +
					"AppConfig. The exception is: " + ecoe.getMessage();
			logger.fatal(LOGTAG + errMsg);
			throw new ProviderException(errMsg);
		}

		// Initialize all provisioning steps this provider will use to
		// verify the runtime configuration as best we can.
		// Get a list of all step properties.
		List<PropertyConfig> stepPropConfigs;
		try {
			stepPropConfigs = getAppConfig().getObjectsLike("ProvisioningStep");
		} catch (EnterpriseConfigurationObjectException eoce) {
			String errMsg = "An error occurred getting ProvisioningStep " +
					"properties from AppConfig. The exception is: " +
					eoce.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, eoce);
		}
		logger.info(LOGTAG + "There are " + stepPropConfigs.size() + " steps.");

		// Convert property configs to properties
		List<Properties> stepProps = new ArrayList<>();
		ListIterator<PropertyConfig> stepPropConfigsIterator = stepPropConfigs.listIterator();
		while (stepPropConfigsIterator.hasNext()) {
			PropertyConfig stepConfig = stepPropConfigsIterator.next();
			Properties stepProp = stepConfig.getProperties();
			stepProps.add(stepProp);
		}

		// Sort the list by stepId integer.
		stepProps.sort(new StepPropIdComparator(1));

		// For each property instantiate the step and log out its details.
		ListIterator<Properties> stepPropsIterator = stepProps.listIterator();
		while (stepPropsIterator.hasNext()) {
			Properties sp = stepPropsIterator.next();
			String className = sp.getProperty("className");
			String stepId = sp.getProperty("stepId");
			String stepType = sp.getProperty("type");
			if (className != null) {
				logger.info(LOGTAG + "Step " + stepId + ": " + stepType);
				logger.info(LOGTAG + "Verified class for step "
						+ stepId + ": " + className);
			}
		}

		logger.info(LOGTAG + "Initialization complete.");
	}

	private String getCentralAdminPpid() {
		return m_centralAdminPpid;
	}

	private void setCentralAdminPpid(String centralAdminPpid) {
		m_centralAdminPpid = centralAdminPpid;		
	}

	/**
	 * This method proxys a query to an RDBMS command that handles it. The
	 * purpose of including this operation in this command (and not just the
	 * generate) operations is that it will give us one command that should
	 * handle all broad access to the VirtualPrivateCloudProvisioining service
	 * operations. In general, applications and clients will only need to
	 * perform the query and generate operations and the create, update, and
	 * delete operations will be handled by and RDBMS connector deployment
	 * and accessed by this command and administrative applications like the
	 * VPCP web application.
	 */
	@SuppressWarnings("unchecked")
	public List<VirtualPrivateCloudProvisioning> query(VirtualPrivateCloudProvisioningQuerySpecification querySpec)
			throws ProviderException {
		logger.debug(LOGTAG + "Querying for VPCP with ProvisioningId: " +
				querySpec.getProvisioningId());

		// Get a configured VirtualPrivateCloudProvisioning object to use.
		VirtualPrivateCloudProvisioning vpcp = new VirtualPrivateCloudProvisioning();
		try {
			vpcp = (VirtualPrivateCloudProvisioning) getAppConfig()
					.getObjectByType(vpcp.getClass().getName());
		} catch (EnterpriseConfigurationObjectException ecoe) {
			String errMsg = "An error occurred getting an object from " +
					"AppConfig. The exception is: " + ecoe.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException();
		}

		// Get a RequestService to use for this transaction.
		RequestService rs;
		try {
			rs = (RequestService) getAwsAccountServiceProducerPool().getExclusiveProducer();
			PointToPointProducer p2p = (PointToPointProducer) rs;
			p2p.setRequestTimeoutInterval(1000000);
		} catch (JMSException jmse) {
			String errMsg = "An error occurred getting a request service to use " +
					"in this transaction. The exception is: " + jmse.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, jmse);
		}
		// Create the VirtualPrivateCloudProvisioningObject.
		List<VirtualPrivateCloudProvisioning> results;
		try {
			logger.debug(LOGTAG + "Querying for the VPCP...");
			long startTime = System.currentTimeMillis();
			results = vpcp.query(querySpec, rs);
			long time = System.currentTimeMillis() - startTime;
			logger.debug(LOGTAG + "Queried for VirtualPrivateCloudProvisioning " +
					"objects in " + time + " ms.");
		} catch (EnterpriseObjectQueryException eoce) {
			String errMsg = "An error occurred querying the VirtualPrivate" +
					"CloudProvisioning object The exception is: " +
					eoce.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, eoce);
		}
		// In any case, release the producer back to the pool.
		finally {
			getAwsAccountServiceProducerPool().releaseProducer((PointToPointProducer) rs);
		}

		// Return the results
		return results;
	}

	public void create(VirtualPrivateCloudProvisioning vpcp) throws ProviderException {
		String LOGTAG = "[EmoryVirtualPrivateCloudProvisioningProvider.create] ";

		// Get a RequestService to use for this transaction.
		RequestService rs;
		try {
			rs = (RequestService) getAwsAccountServiceProducerPool().getExclusiveProducer();
			((PointToPointProducer) rs).setRequestTimeoutInterval(1_000_000);
		} catch (JMSException jmse) {
			String errMsg = "An error occurred getting a request service to use " +
					"in this transaction. The exception is: " + jmse.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, jmse);
		}
		// Create the VirtualPrivateCloudProvisioningObject.
		try {
			long startTime = System.currentTimeMillis();
			vpcp.create(rs);
			long time = System.currentTimeMillis() - startTime;
			logger.info(LOGTAG + "Created VirtualPrivateCloudProvisioning " +
					"object in " + time + " ms.");
		} catch (EnterpriseObjectCreateException eoce) {
			String errMsg = "An error occurred creating the VirtualPrivate" +
					"CloudProvisioning object The exception is: " +
					eoce.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, eoce);
		}
		// In any case, release the producer back to the pool.
		finally {
			getAwsAccountServiceProducerPool().releaseProducer((PointToPointProducer) rs);
		}
	}

	@SuppressWarnings("unchecked")
	public VirtualPrivateCloudProvisioning generate(VirtualPrivateCloudRequisition vpcr)
			throws ProviderException {
		
		try {
			logger.info(LOGTAG+"vpcr = "+vpcr.toXmlString());
		} catch (XmlEnterpriseObjectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Get a configured VirtualPrivateCloudProvisioning object from AppConfig
		VirtualPrivateCloudProvisioning vpcp =
				new VirtualPrivateCloudProvisioning();
		try {
			vpcp = (VirtualPrivateCloudProvisioning) m_appConfig
					.getObjectByType(vpcp.getClass().getName());
		} catch (EnterpriseConfigurationObjectException ecoe) {
			String errMsg = "An error occurred retrieving an object from " +
					"AppConfig. The exception is: " + ecoe.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, ecoe);
		}

		// Get the next sequence number to identify the VPCP.
		String seq;
		try {
			seq = getProvisioningIdSequence().next();
			logger.info(LOGTAG + "The ProvisioningIdSequence value is: " + seq);
		} catch (SequenceException se) {
			String errMsg = "An error occurred getting the next value " +
					"from the ProvisioningId sequence. The exception is: " +
					se.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, se);
		}

		// Set the values of the VPCP.
		try {
			vpcp.setProvisioningId("rhedcloud-vpc-" + seq);
			logger.info(LOGTAG + "The ProvisioningId is: " + vpcp.getProvisioningId());
			vpcp.setVirtualPrivateCloudRequisition(vpcr);
			vpcp.setStatus(PENDING_STATUS);
			vpcp.setCreateUser("AwsAccountService");
			vpcp.setCreateDatetime(new Datetime("Create", System.currentTimeMillis()));
		} catch (EnterpriseFieldException efe) {
			String errMsg = "An error occurred setting the values of the " +
					"VirtualPrivateCloud object. The exception is: " +
					efe.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, efe);
		}

		// Add all of the steps.
		// Initialize all provisioning steps this provider will use to
		// verify the runtime configuration as best we can.
		// Get a list of all step properties.
		List<PropertyConfig> stepPropConfigs;
		try {
			stepPropConfigs = getAppConfig().getObjectsLike("ProvisioningStep");
		} catch (EnterpriseConfigurationObjectException eoce) {
			String errMsg = "An error occurred getting ProvisioningStep " +
					"properties from AppConfig. The exception is: " +
					eoce.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, eoce);
		}
		logger.info(LOGTAG + "There are " + stepPropConfigs.size() + " steps.");

		// Convert property configs to properties
		List<Properties> stepProps = new ArrayList<>();
		ListIterator<PropertyConfig> stepPropConfigsIterator = stepPropConfigs.listIterator();
		while (stepPropConfigsIterator.hasNext()) {
			PropertyConfig stepConfig = stepPropConfigsIterator.next();
			Properties stepProp = stepConfig.getProperties();
			stepProps.add(stepProp);
		}

		// Sort the list by stepId integer.
		stepProps.sort(new StepPropIdComparator(1));

		// For each property instantiate a provisioning step
		// and add it to the provisioning object.
		ListIterator<Properties> stepPropsIterator = stepProps.listIterator();
		int totalAnticipatedTime = 0;
		while (stepPropsIterator.hasNext()) {
			Properties sp = stepPropsIterator.next();
			String stepId = sp.getProperty("stepId");
			String stepType = sp.getProperty("type");
			String stepDesc = sp.getProperty("description");
			String stepAnticipatedTime = sp.getProperty("anticipatedTime");
			int anticipatedTime = Integer.parseInt(stepAnticipatedTime);
			totalAnticipatedTime = totalAnticipatedTime + anticipatedTime;

			ProvisioningStep pStep = vpcp.newProvisioningStep();
			try {
				pStep.setProvisioningId(vpcp.getProvisioningId());
				pStep.setStepId(stepId);
				pStep.setType(stepType);
				pStep.setDescription(stepDesc);
				pStep.setStatus(PENDING_STATUS);
				pStep.setAnticipatedTime(stepAnticipatedTime);
				pStep.setCreateUser("AwsAccountService");
				pStep.setCreateDatetime(new Datetime("Create", System.currentTimeMillis()));

				vpcp.addProvisioningStep(pStep);
			} catch (EnterpriseFieldException efe) {
				String errMsg = "An error occurred setting field values of " +
						"the provisioning object. The exception is: " +
						efe.getMessage();
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg, efe);

			}
			logger.info(LOGTAG + "Added step " + stepId + " " + stepType + " to the provisioning object"
					+ " bringing total anticipated time to " + totalAnticipatedTime + " ms.");
		}

		// update the VPCP anticipated time.
		try {
			vpcp.setAnticipatedTime(Integer.toString(totalAnticipatedTime));
		} catch (EnterpriseFieldException efe) {
			String errMsg = "An error occurred setting field values of " +
					"the provisioning object. The exception is: " +
					efe.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, efe);
		}

		// Create the VPCP.
		try {
			long createStartTime = System.currentTimeMillis();
			create(vpcp);
			long createTime = System.currentTimeMillis() - createStartTime;
			logger.info(LOGTAG + "Created VPCP in " + createTime + " ms.");
		} catch (ProviderException pe) {
			String errMsg = "An error occurred performing the VPCP create. " +
					"The exception is: " + pe.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, pe);
		}

		// Add the VPCP to the ThreadPool for processing.
		// If this thread pool is set to check for available threads before
		// adding jobs to the pool, it may throw an exception indicating it
		// is busy when we try to add a job. We need to catch that exception
		// and try to add the job until we are successful.
		boolean jobAdded = false;
		while (jobAdded == false) {
			try {
				logger.info(LOGTAG + "Adding job to threadpool for " +
						"ProvisioningId: " + vpcp.getProvisioningId());
				getThreadPool().addJob(new VirtualPrivateCloudProvisioningTransaction(vpcp));
				jobAdded = true;
			} catch (ThreadPoolException tpe) {
				// The thread pool is busy. Log it and sleep briefly to try to
				// add the job again later.
				String msg = "The thread pool is busy. Sleeping for " +
						getSleepInterval() + " milliseconds.";
				logger.debug(LOGTAG + msg);
				try {
					Thread.sleep(getSleepInterval());
				} catch (InterruptedException ie) {
					// An error occurred while sleeping to allow threads in the pool
					// to clear for processing. Log it and throw and exception.
					String errMsg = "An error occurred while sleeping to allow " +
							"threads in the pool to clear for processing. The exception " +
							"is " + ie.getMessage();
					logger.fatal(LOGTAG + errMsg);
					throw new ProviderException(errMsg);
				}
			}
		}

		// Return the object.
		return vpcp;
	}

	public void update(VirtualPrivateCloudProvisioning vpcp) throws ProviderException {
		String LOGTAG = "[EmoryVirtualPrivateCloudProvisioningProvider.update] ";

		// Get a RequestService to use for this transaction.
		RequestService rs;
		try {
			rs = (RequestService) getAwsAccountServiceProducerPool().getExclusiveProducer();
		} catch (JMSException jmse) {
			String errMsg = "An error occurred getting a request service to use " +
					"in this transaction. The exception is: " + jmse.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, jmse);
		}
		// Update the VirtualPrivateCloudProvisioningObject.
		try {
			long startTime = System.currentTimeMillis();
			vpcp.update(rs);
			long time = System.currentTimeMillis() - startTime;
			logger.info(LOGTAG + "Updated VirtualPrivateCloudProvisioning object in " + time + " ms.");
		} catch (EnterpriseObjectUpdateException eoce) {
			String errMsg = "An error occurred updating the VirtualPrivateCloudProvisioning object The exception is: " + eoce.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, eoce);
		}
		// In any case, release the producer back to the pool.
		finally {
			getAwsAccountServiceProducerPool().releaseProducer((PointToPointProducer) rs);
		}
	}

	public void delete(VirtualPrivateCloudProvisioning vpcp) throws ProviderException {
		String LOGTAG = "[EmoryVirtualPrivateCloudProvisioningProvider.delete] ";

		// Get a RequestService to use for this transaction.
		RequestService rs;
		try {
			rs = (RequestService) getAwsAccountServiceProducerPool().getExclusiveProducer();
		} catch (JMSException jmse) {
			String errMsg = "An error occurred getting a request service to use " +
					"in this transaction. The exception is: " + jmse.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, jmse);
		}
		// Create the VirtualPrivateCloudProvisioningObject.
		try {
			long startTime = System.currentTimeMillis();
			vpcp.create(rs);
			long time = System.currentTimeMillis() - startTime;
			logger.info(LOGTAG + "Updated VirtualPrivateCloudProvisioning " +
					"object in " + time + " ms.");
		} catch (EnterpriseObjectCreateException eoce) {
			String errMsg = "An error occurred updating the VirtualPrivate" +
					"CloudProvisioning object The exception is: " +
					eoce.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, eoce);
		}
		// In any case, release the producer back to the pool.
		finally {
			getAwsAccountServiceProducerPool().releaseProducer((PointToPointProducer) rs);
		}
	}

	/**
	 * This method sets the centralAdminRoleDn
	 */
	private void setCentralAdminRoleDn(String dn) throws ProviderException {
		if (dn == null) {
			String errMsg = "centralAdminRoleDn property is null. " +
					"Can't continue.";
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg);
		}

		m_centralAdminRoleDn = dn;
	}

	/**
	 * This method returns the centralAdminRoleDn
	 */
	private String getCentralAdminRoleDn() {
		return m_centralAdminRoleDn;
	}

	/**
	 * This method sets the verbose logging property
	 */
	private void setVerbose(boolean verbose) {
		m_verbose = verbose;
	}

	/**
	 * This method returns the verbose logging property
	 */
	private boolean getVerbose() {
		return m_verbose;
	}

	/**
	 * This method sets the ProvisioningId sequence.
	 */
	private void setProvisioningIdSequence(Sequence seq) {
		m_provisioningIdSequence = seq;
	}

	/**
	 * This method returns a reference to the ProvisioningId sequence.
	 */
	private Sequence getProvisioningIdSequence() {
		return m_provisioningIdSequence;
	}

	/**
	 * This method sets the producer pool to use to send
	 * messages to the AWS Account Service.
	 */
	private void setAwsAccountServiceProducerPool(ProducerPool pool) {
		m_awsAccountServiceProducerPool = pool;
	}

	/**
	 * This method returns a reference to the producer pool to use to
	 * send messages to the AWS account service.
	 */
	private ProducerPool getAwsAccountServiceProducerPool() {
		return m_awsAccountServiceProducerPool;
	}

	/**
	 * This method sets the producer pool to use to send
	 * messages to the ServiceNow Service.
	 */
	private void setServiceNowServiceProducerPool(ProducerPool pool) {
		m_serviceNowServiceProducerPool = pool;
	}

	/**
	 * This method returns a reference to the producer pool to use to
	 * send messages to the ServiceNow service.
	 */
	private ProducerPool getServiceNowServiceProducerPool() {
		return m_serviceNowServiceProducerPool;
	}

	/**
	 * This method sets the producer pool to use to send
	 * messages to the IDM Service.
	 */
	private void setIdmServiceProducerPool(ProducerPool pool) {
		m_idmServiceProducerPool = pool;
	}

	/**
	 * This method returns a reference to the producer pool to use to
	 * send messages to the IDM service.
	 */
	private ProducerPool getIdmServiceProducerPool() {
		return m_idmServiceProducerPool;
	}

	/**
	 * This method gets the thread pool.
	 */
	public final ThreadPool getThreadPool() {
		return m_threadPool;
	}

	/**
	 * This method sets the thread pool.
	 */
	private void setThreadPool(ThreadPool tp) {
		m_threadPool = tp;
	}

	/**
	 * This method gets the value of the threadPoolSleepInterval.
	 */
	public final int getSleepInterval() {
		return m_threadPoolSleepInterval;
	}

	/**
	 * This method sets the AppConfig object for this provider to use.
	 */
	private void setAppConfig(AppConfig aConfig) {
		m_appConfig = aConfig;
	}

	/**
	 * This method returns a reference to the AppConfig this provider is
	 * using.
	 */
	private AppConfig getAppConfig() {
		return m_appConfig;
	}

	private VirtualPrivateCloudProvisioningProvider getVirtualPrivateCloudProvisioningProvider() {
		return this;
	}

	private class StepPropIdComparator implements Comparator<Properties> {

		int m_order;

		public StepPropIdComparator(int order) {
			m_order = order;
		}

		public int compare(Properties prop1, Properties prop2) {
			int returnVal = 0;

			// Convert stepIds to integers
			int stepId1 = Integer.parseInt(prop1.getProperty("stepId"));
			int stepId2 = Integer.parseInt(prop2.getProperty("stepId"));

			// Compare integer stepIds.
			if (stepId1 < stepId2) {
				returnVal = -1;
			} else if (stepId1 > stepId2) {
				returnVal = 1;
			} else if (stepId1 == stepId2) {
				returnVal = 0;
			}
			return (returnVal * m_order);
		}
	}

	private class StepIdComparator implements Comparator<Step> {

		int m_order;

		public StepIdComparator(int order) {
			m_order = order;
		}

		public int compare(Step step1, Step step2) {
			int returnVal = 0;

			// Convert stepIds to integers
			int stepId1 = Integer.valueOf(step1.getStepId());
			int stepId2 = Integer.valueOf(step2.getStepId());

			// Compare integer stepIds.
			if (stepId1 < stepId2) {
				returnVal = -1;
			} else if (stepId1 > stepId2) {
				returnVal = 1;
			} else if (stepId1 == stepId2) {
				returnVal = 0;
			}
			return (returnVal * m_order);
		}
	}

	public Incident generateIncident(IncidentRequisition req)
			throws ProviderException {

		String LOGTAG = "[EmoryVirtualPrivateCloudProvisioningProvider.generateIncident] ";

		if (req == null) {
			String errMsg = "IncidentRequisition is null. Can't generate an Incident.";
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg);
		}

		// Get a configured Incident object from AppConfig.
		Incident incident = new Incident();
		try {
			incident = (Incident) getAppConfig()
					.getObjectByType(incident.getClass().getName());
		} catch (EnterpriseConfigurationObjectException ecoe) {
			String errMsg = "An error occurred retrieving an object from " +
					"AppConfig. The exception is: " + ecoe.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, ecoe);
		}

		// Log the state of the requisition.
		try {
			logger.info(LOGTAG + "Incident requisition is: " + req.toXmlString());
		} catch (XmlEnterpriseObjectException xeoe) {
			String errMsg = "An error occurred serializing the requisition " +
					"to XML. The exception is: " + xeoe.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, xeoe);
		}

		// Get a producer from the pool
		RequestService rs;
		try {
			rs = (RequestService) getServiceNowServiceProducerPool()
					.getExclusiveProducer();
		} catch (JMSException jmse) {
			String errMsg = "An error occurred getting a producer " +
					"from the pool. The exception is: " + jmse.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, jmse);
		}

		List<?> results;
		try {
			long generateStartTime = System.currentTimeMillis();
			results = incident.generate(req, rs);
			long generateTime = System.currentTimeMillis() - generateStartTime;
			logger.info(LOGTAG + "Generated Incident in " +
					+generateTime + " ms. Returned " + results.size() +
					" result.");
		} catch (EnterpriseObjectGenerateException eoge) {
			String errMsg = "An error occurred generating the  " +
					"Incident object. The exception is: " + eoge.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, eoge);
		} finally {
			// Release the producer back to the pool
			getServiceNowServiceProducerPool()
			.releaseProducer((MessageProducer) rs);
		}

		return (Incident) results.get(0);
	}

	public int notifyCentralAdministrators(UserNotification notification)
			throws ProviderException {

		// Query for the list of central administrators.
		List<RoleAssignment> roleAssignments =
				roleAssignmentQuery(getCentralAdminRoleDn());

		ListIterator<RoleAssignment> li = roleAssignments.listIterator();
		int i = 0;
		while (li.hasNext()) {
			RoleAssignment ra = li.next();
			String userDn = (String) ra.getExplicitIdentityDNs().getDistinguishedName().get(0);
			String userId = parseUserId(userDn);
			try {
				notification.setUserId(userId);
			} catch (EnterpriseFieldException efe) {
				String errMsg = "An error occurred setting the " +
						"field values on an object. The exception is: " +
						efe.getMessage();
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg, efe);
			}
			createUserNotification(notification);
			i++;
		}

		return i;
	}

	public List<String> getCentralAdministrators()
			throws ProviderException {

		// Query for the list of central administrators.
		List<RoleAssignment> roleAssignments =
				roleAssignmentQuery(getCentralAdminRoleDn());

		ListIterator<RoleAssignment> li = roleAssignments.listIterator();
		List<String> userIds = new ArrayList<>();
		while (li.hasNext()) {
			RoleAssignment ra = li.next();
			String userDn = (String) ra.getExplicitIdentityDNs().getDistinguishedName().get(0);
			String userId = parseUserId(userDn);
			userIds.add(userId);
		}

		return userIds;
	}

	private String parseUserId(String dn) {
		StringTokenizer st1 = new StringTokenizer(dn, ",");
		String firstToken = st1.nextToken();
		StringTokenizer st2 = new StringTokenizer(firstToken, "=");
		st2.nextToken();
		String userId = st2.nextToken();
		return userId;
	}

	private void createUserNotification(UserNotification notification)
			throws ProviderException {

		// Create the UserNotification in the AWS Account Service.
		// Get a RequestService to use for this transaction.
		RequestService rs;
		try {
			rs = (RequestService) getAwsAccountServiceProducerPool().getExclusiveProducer();
		} catch (JMSException jmse) {
			String errMsg = "An error occurred getting a request service to use " +
					"in this transaction. The exception is: " + jmse.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, jmse);
		}
		// Create the UserNotification object.
		try {
			long startTime = System.currentTimeMillis();
			notification.create(rs);
			long time = System.currentTimeMillis() - startTime;
			logger.info(LOGTAG + "Created UserNotification " +
					"object in " + time + " ms.");
		} catch (EnterpriseObjectCreateException eoce) {
			String errMsg = "An error occurred creating the " +
					"UserNotification object The exception is: " +
					eoce.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, eoce);
		}
		// In any case, release the producer back to the pool.
		finally {
			getAwsAccountServiceProducerPool().releaseProducer((PointToPointProducer) rs);
		}
	}

	@SuppressWarnings("unchecked")
	private List<RoleAssignment> roleAssignmentQuery(String roleDn)
			throws ProviderException {

		// Query the IDM service for all users in the named role
		// Get a configured AccountUser, RoleAssignment, and
		// RoleAssignmentQuerySpecification from AppConfig
		RoleAssignment roleAssignment = new RoleAssignment();
		RoleAssignmentQuerySpecification querySpec = new RoleAssignmentQuerySpecification();
		try {
			roleAssignment = (RoleAssignment) m_appConfig
					.getObjectByType(roleAssignment.getClass().getName());
			querySpec = (RoleAssignmentQuerySpecification) m_appConfig
					.getObjectByType(querySpec.getClass().getName());
		} catch (EnterpriseConfigurationObjectException ecoe) {
			String errMsg = "An error occurred retrieving an object from " +
					"AppConfig. The exception is: " + ecoe.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, ecoe);
		}

		// Set the values of the querySpec.
		try {
			querySpec.setRoleDN(roleDn);
			querySpec.setIdentityType("USER");
			querySpec.setDirectAssignOnly("true");
		} catch (EnterpriseFieldException efe) {
			String errMsg = "An error occurred setting the values of the " +
					"query specification object. The exception is: " +
					efe.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, efe);
		}

		// Get a RequestService to use for this transaction.
		RequestService rs;
		try {
			rs = (RequestService) getIdmServiceProducerPool().getExclusiveProducer();
		} catch (JMSException jmse) {
			String errMsg = "An error occurred getting a request service to use " +
					"in this transaction. The exception is: " + jmse.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, jmse);
		}
		// Query for the RoleAssignments for the Administrator Role.
		List<RoleAssignment> roleAssignments;
		try {
			long startTime = System.currentTimeMillis();
			roleAssignments = roleAssignment.query(querySpec, rs);
			long time = System.currentTimeMillis() - startTime;
			logger.info(LOGTAG + "Queried for RoleAssignments for " +
					"roleDn " + roleDn + " in " + time + " ms. Returned " +
					roleAssignments.size() + " users in the role.");
		} catch (EnterpriseObjectQueryException eoqe) {
			String errMsg = "An error occurred querying for the " +
					"RoleAssignment objects The exception is: " +
					eoqe.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, eoqe);
		}
		// In any case, release the producer back to the pool.
		finally {
			getIdmServiceProducerPool().releaseProducer((PointToPointProducer) rs);
		}

		return roleAssignments;
	}

	/**
	 * A transaction to process virtual private cloud provisioning.
	 */
	private class VirtualPrivateCloudProvisioningTransaction implements java.lang.Runnable {

		VirtualPrivateCloudProvisioning m_vpcp;
		long m_executionStartTime = 0;

		public VirtualPrivateCloudProvisioningTransaction(VirtualPrivateCloudProvisioning vpcp) {
			logger.info(LOGTAG + "Initializing provisioning process for " +
					"ProvisioningId: " + vpcp.getProvisioningId());
			try {
				logger.info(LOGTAG+"vpcp = "+vpcp.toXmlString());
			} catch (XmlEnterpriseObjectException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			m_vpcp = vpcp;
		}

		@SuppressWarnings({ "deprecation", "unchecked" })
		public void run() {
			setExecutionStartTime(System.currentTimeMillis());
			String LOGTAG = "[VirtualPrivateCloudProvisioningTransaction{" +
					getProvisioningId() + "}] ";
			logger.info(LOGTAG + "Processing ProvisioningId number: "
					+ getProvisioningId());

			// Get the FailStep if it exists.
			int failStep = 0;
			String purpose = m_vpcp.getVirtualPrivateCloudRequisition().getPurpose();
			if (purpose != null) {
				if (purpose.startsWith("FailStep")) {
					String[] args = purpose.split("=");
					String sFailStep = args[1];
					logger.info(LOGTAG + "sFailStep is: " + sFailStep);
					try {
						failStep = Integer.parseInt(sFailStep);
					} catch (Exception e) {
						String errMsg = "Invalid format for FailStep.";
						logger.error(LOGTAG + errMsg);
					}
					logger.info(LOGTAG + "failStep is: " + failStep);
				}
			}

			// Get a list of all step properties.
			List<PropertyConfig> stepPropConfigs;
			try {
				stepPropConfigs = getAppConfig().getObjectsLike("ProvisioningStep");
			} catch (EnterpriseConfigurationObjectException eoce) {
				String errMsg = "An error occurred getting ProvisioningStep " +
						"properties from AppConfig. The exception is: " +
						eoce.getMessage();
				logger.error(LOGTAG + errMsg);
				return;
			}
			logger.info(LOGTAG + "There are " + stepPropConfigs.size() + " steps.");

			// Convert property configs to properties
			List<Properties> stepProps = new ArrayList<>();
			ListIterator<PropertyConfig> stepPropConfigsIterator = stepPropConfigs.listIterator();
			while (stepPropConfigsIterator.hasNext()) {
				PropertyConfig pConfig = stepPropConfigsIterator.next();
				Properties stepProp = pConfig.getProperties();
				stepProps.add(stepProp);
			}

			// Sort the list by stepId integer.
			stepProps.sort(new StepPropIdComparator(1));

			// For each property instantiate the step, call the execute
			// method, and if successful, place it in the map of
			// completed steps.
			List<Step> completedSteps = new ArrayList<>();
			ListIterator<Properties> stepPropsIterator = stepProps.listIterator();
			int i = 0;
			while (stepPropsIterator.hasNext()) {
				i++;
				Properties props = stepPropsIterator.next();
				String className = props.getProperty("className");
				if (className != null) {
					// Instantiate the step
					Step step = null;
					AbstractStep abstep = null;
					try {
						Class<Step> stepClass = (Class<Step>) Class.forName(className);
						step = (Step) stepClass.newInstance();
						abstep = (AbstractStep) step;
						logger.info(LOGTAG + "Initializing step " + i + ".");

						// If this is the failStep, set the failStep to be true.
						if (i == failStep) {
							logger.info(LOGTAG + "This step (" + i + ") is the FailStep. " +
									"Setting failStep property to true.");
							props.setProperty("failStep", "true");
						} else {
							logger.info(LOGTAG + "This step (" + i + ") is not the FailStep.");
						}

						step.init(getProvisioningId(), props, getAppConfig(),
								getVirtualPrivateCloudProvisioningProvider());
						abstep.setVpcr(m_vpcp.getVirtualPrivateCloudRequisition());
					} catch (ClassNotFoundException | IllegalAccessException | InstantiationException ie) {
						// An error occurred instantiating the step.
						// Log it and roll back all preceding steps.
						String errMsg = "An error occurred instantiating the step. " + "The exception is: " + ie.getMessage();
						logger.error(LOGTAG + errMsg);
						logger.error(LOGTAG + "Can't set completed status and failure result.");
						rollbackCompletedSteps(completedSteps, step, errMsg);
						return;
					} catch (StepException se) {
						// An error occurred initializing the step.
						// Log it and roll back all preceding steps.
						String errMsg = "An error occurred initializing the Step " +
								step.getStepId() + "The exception is: " + se.getMessage();
						logger.error(LOGTAG + errMsg);
						try {
							logger.info(LOGTAG + "Setting completed status and failure result...");
							step.update(COMPLETED_STATUS, FAILURE_RESULT);
							logger.info(LOGTAG + "Updated to completed status and failure result.");
						} catch (StepException se2) {
							String errMsg2 = "An error occurred updating the " +
									"status to indicate failure. The exception " +
									"is: " + se2.getMessage();
							logger.error(LOGTAG + errMsg2);
						}
						rollbackCompletedSteps(completedSteps, step, errMsg);
						return;
					}

					// Execute the step
					List<Property> resultProps;
					try {
						logger.info(LOGTAG + "Executing [Step-" +
								step.getStepId() + "] " +
								step.getDescription());
						long startTime = System.currentTimeMillis();
						resultProps = step.execute();
						long time = System.currentTimeMillis() - startTime;
						logger.info(LOGTAG + "Completed Step " +
								step.getStepId() + " with result " +
								step.getResult() + " in " + time + " ms.");
						logger.info(LOGTAG + "Step result properties are: " +
								resultPropsToXmlString(resultProps));

						// If the result of the step is failure, roll back
						// all completed steps and return.
						if (step.getResult().equals(FAILURE_RESULT)) {
							logger.info(LOGTAG + "[Step " + step.getStepId() +
									"] failed. Rolling back all completed steps.");
							rollbackCompletedSteps(completedSteps, step, "Step failed");
							return;
						}

						// Add all successfully completed steps to the list
						// of completed steps.
						completedSteps.add(step);

					} catch (StepException se) {
						// An error occurred executing the step.
						// Log it and roll back all preceding steps.
						LOGTAG = LOGTAG + "[StepExecutionException][Step-" +
								step.getStepId() + "] ";
						String errMsg = "An error occurred executing step " +
								step.getStepId() + ". The exception is: " + se.getMessage();
						logger.error(LOGTAG + errMsg);

						try {
							logger.info(LOGTAG + "Setting completed status, "
									+ "failure result, and final error details...");
							// Add an error step property limited to 255 characters.
							String stepExecutionException;
							if (se.getMessage() != null) {
								int size = se.getMessage().length();
								logger.info(LOGTAG + "stepExecutionException size is: "
										+ size);
								if (size > 254) size = 254;
								stepExecutionException =
										se.getMessage().substring(0, size);
							} else {
								stepExecutionException = "No step execution exception found.";
							}
							logger.info(LOGTAG + "Final step execution exception text is: " +
									stepExecutionException);

							step.addResultProperty("stepExecutionException",
									stepExecutionException);
							logger.info(LOGTAG + "Added property " +
									"stepExecutionException: " +
									stepExecutionException);
							step.update(COMPLETED_STATUS, FAILURE_RESULT);
							logger.info(LOGTAG + "Updated to completed status " +
									"and failure result.");
						} catch (StepException se2) {
							String errMsg2 = "An error occurred updating the " +
									"status to indicate failure. The exception " +
									"is: " + se2.getMessage();
							logger.error(LOGTAG + errMsg2);
						} finally {
							rollbackCompletedSteps(completedSteps, step, errMsg);
						}
						return;
					}
				} else {
					String errMsg = "An error occurred instantiating " +
							"a step. The className property is null.";
					logger.error(LOGTAG + errMsg);
					rollbackCompletedSteps(completedSteps, null, errMsg);
					return;
				}
			}

			// All steps completed successfully.
			// Set the end of execution.
			long executionTime = System.currentTimeMillis() - getExecutionStartTime();

			// Update the state of the VPCP object in this transaction.
			queryForVpcpBaseline();

			// Set the status to complete, the result to success, and the
			// execution time.
			try {
				getVirtualPrivateCloudProvisioning().setStatus(COMPLETED_STATUS);
				getVirtualPrivateCloudProvisioning().setProvisioningResult(SUCCESS_RESULT);
				getVirtualPrivateCloudProvisioning().setActualTime(Long.toString(executionTime));
			} catch (EnterpriseFieldException efe) {
				String errMsg = "An error occurred setting field values on the " +
						"VPCP object. The exception is: " + efe.getMessage();
				logger.error(LOGTAG + errMsg);
			}

			// Update the VPCP object.
			try {
				getVirtualPrivateCloudProvisioningProvider()
				.update(getVirtualPrivateCloudProvisioning());
			} catch (ProviderException pe) {
				String errMsg = "An error occurred querying for the  " +
						"current state of a VirtualPrivateCloudProvisioning object. " +
						"The exception is: " + pe.getMessage();
				logger.error(LOGTAG + errMsg);
			}
		}

		private Step theFailedStep;

		private void rollbackCompletedSteps(List<Step> completedSteps,
				Step failedStep, String extraErrMsg) {
			theFailedStep = failedStep;

			String header = "This provisioning step has failed. Background processes are removing "
					+ "any resources that were created in the process. Once those resources are removed, "
					+ "the prior provisioning steps will be rolled back appropriately. This process may take 3-4 minutes to complete.<br>";

			long startTime = System.currentTimeMillis();

			String errMessage = header+"<br>"+extraErrMsg+"<br>";
			String rollbackStatus = "Initiating account deprovisioning...please wait<br>";
			String deprovStatus = "<br>";

			updateDescription(errMessage+rollbackStatus+deprovStatus);
			logger.info(LOGTAG + "Initiating account deprovisioning...");

			boolean doRollback = false;
			String dId = null;
			String accountId = null;
			String allocateNewAccount = null;
			try {

				// to get the allocateNewAccount property you must loop thru completed step looking for a property named 'allocateNewAccount'
				for (Step step: completedSteps) {
					for (Property prop: step.getResultProperties()) {
						allocateNewAccount = prop.getKey().equalsIgnoreCase("allocateNewAccount") 
								? prop.getValue() : null;
						if (allocateNewAccount != null) break; 
					}
					if (allocateNewAccount != null) break; 
				}

				// to get the account ID you must loop thru completed step looking for a property named 'newAccountId'
				for (Step step: completedSteps) {
					for (Property prop: step.getResultProperties()) {
						accountId = prop.getKey().equalsIgnoreCase("newAccountId") 
								? prop.getValue() : null;
						if (accountId != null) break; 
					}
					if (accountId != null) break; 
				}


				if (accountId == null)  {
					logger.warn(LOGTAG + "Can't initiate account deprovisioning: Can't find account ID");
					rollbackStatus = "Can't initiate account deprovisioning: Can't find account ID. Rolling back steps one by one...<br>";
					updateDescription(errMessage+rollbackStatus+deprovStatus);
					doRollback = true;
				} else if ("false".equals(allocateNewAccount)) {
					logger.info(LOGTAG + "This is a VPC provisioning run. Account is not new.");
					rollbackStatus = "This is a VPC provisioning run. Account is not new. Rolling back steps one by one...<br>";
					updateDescription(errMessage+rollbackStatus+deprovStatus);
					doRollback = true;					
				} else {
					String userId = getVirtualPrivateCloudProvisioning().getVirtualPrivateCloudRequisition().getAccountOwnerUserId();
					logger.info(LOGTAG + "Initiating account deprovisioning for user "+userId+" for "+accountId
							+"using centralAdminPpid '"+getCentralAdminPpid()+"' ...");
					AccountDeprovisioning accountDeprovisioning = (AccountDeprovisioning) getAppConfig().getObject("AccountDeprovisioning.v1_0");
					AccountDeprovisioningRequisition accountDeprovisioningRequisition = (AccountDeprovisioningRequisition) getAppConfig().getObject("AccountDeprovisioningRequisition.v1_0");
					AccountDeprovisioningQuerySpecification accountDeprovisioningQuerySpec = (AccountDeprovisioningQuerySpecification) getAppConfig().getObject("AccountDeprovisioningQuerySpecification.v1_0");
					accountDeprovisioningRequisition.setAccountId(accountId);
					accountDeprovisioningRequisition.setAuthenticatedRequestorUserId(getCentralAdminPpid());
					RequestService rs = (RequestService) getAwsAccountServiceProducerPool().getProducer();
					@SuppressWarnings("unchecked")
					List<AccountDeprovisioning> results = accountDeprovisioning.generate(accountDeprovisioningRequisition, rs);
					AccountDeprovisioning result = results.get(0);
					dId = result.getDeprovisioningId();
					logger.info(LOGTAG + "AccountDeprovisioningId is "+dId);
					rollbackStatus = "AccountDeprovisioning ID is "+dId+"<br>";
					updateDescription(errMessage+rollbackStatus+deprovStatus);
					accountDeprovisioningQuerySpec.setDeprovisioningId(dId);
					//loop until deprovision is done
					long start = System.currentTimeMillis();
					long timeout = start + (5 * 60 * 1000);
					long sleepInterval = 7000; 
					String status = "unknown";
					do {
						deprovStatus = "Deprovisioning has been running for "+(System.currentTimeMillis()-start)/1000+" seconds.<br>"
								+"<br><code><b>Step #, Status, Anticipated, Actual</b> (times in seconds)<br>";
						@SuppressWarnings("unchecked")
						List<AccountDeprovisioning> dresults = accountDeprovisioning.query(accountDeprovisioningQuerySpec, rs);
						status = dresults.get(0).getStatus();
						logger.info(LOGTAG + dId+" status is "+status);
						if (!"completed".equals(status)) {
							@SuppressWarnings({ "unchecked" })
							List<DeprovisioningStep> deprovSteps = dresults.get(0).getDeprovisioningStep();
							Collections.sort(deprovSteps, new Comparator<DeprovisioningStep>(){
								public int compare(DeprovisioningStep o1, DeprovisioningStep o2){
									if (o1.getStepId() == null && o1.getStepId() == null) return 0;
									if (o1.getStepId() == null) return -1;
									if (o2.getStepId() == null) return 1;
									return Integer.parseInt(o1.getStepId()) - Integer.parseInt(o2.getStepId());
								}
							});

							for(DeprovisioningStep deprovStep: deprovSteps) {
								if (deprovStatus.length() > 2500 ) {
									deprovStatus = deprovStatus.substring(0,2499);
									break;
								}
								deprovStatus += (deprovStep.getStepId()==null?"?":deprovStep.getStepId())+", "
										+deprovStep.getStatus()+", "
										+Long.parseLong(deprovStep.getAnticipatedTime())/1000
										+(deprovStep.getActualTime()==null?"":", "+Long.parseLong(deprovStep.getActualTime())/1000)+"<br>";
							}
							updateDescription(errMessage+rollbackStatus+deprovStatus+"</code>");

							Thread.sleep(sleepInterval);
						} else {
							break;
						}

					} while (System.currentTimeMillis() < timeout );
					if (!"completed".equals(status)) {
						logger.error(LOGTAG + "Timed out waiting for deprovisioning to complete. "+dId+" status is "+status);
						deprovStatus = "Timed out after "+(System.currentTimeMillis()-start)/1000+" seconds waiting for deprovisioning to complete. Status is "+status;
						updateDescription(errMessage+rollbackStatus+deprovStatus);
						logger.warn(LOGTAG + "timeout waiting for deprovisioning to finish");
						doRollback = true;
					} else {
						deprovStatus = "Deprovisioning is complete in "+(System.currentTimeMillis()-start)/1000+" seconds.<br>"+"Status is "+status;
						updateDescription(rollbackStatus+deprovStatus);
						logger.info(LOGTAG + "Deprovisioning is complete. "+dId+" status is "+status);
					}
				}
			} catch (EnterpriseConfigurationObjectException | EnterpriseFieldException | EnterpriseObjectGenerateException | JMSException | EnterpriseObjectQueryException | InterruptedException e1) {
				logger.error(LOGTAG + "Can't initiate or query account deprovisioning: Exception: "
						+e1.getClass()+", Message: ",e1.getMessage());
				// fallback to rollback
				logger.warn(LOGTAG + "reverting to step rollback");
				doRollback = true;
			}

			logger.info(LOGTAG + "Starting rollback of completed steps...");

			// Reverse the order of the completedSteps list.
			completedSteps.sort(new StepIdComparator(-1));

			ListIterator<Step> completedStepsIterator = completedSteps.listIterator();

			while (completedStepsIterator.hasNext()) {
				Step completedStep = completedStepsIterator.next();
				try {
					if (doRollback) {
						if ("not applicable".equals(accountId)) {
							// This is a VPC provisioning, do not deprovision the account
							logger.info(LOGTAG + "Rollback of step "+completedStep.getStepId());							
							completedStep.rollback();
							logger.info(LOGTAG + "Rollback of step "+completedStep.getStepId()+" completed.");
						} else if (accountId != null) {
							// not going to actually rollback this but we could
							// completedStep.rollback();
							logger.info(LOGTAG + "Skipped rollback of step "+completedStep.getStepId());							
							completedStep.update(AbstractStep.ROLLBACK_STATUS, AbstractStep.NO_RESULT, completedStep.getDescription()
									+ "<br>Rollback skipped");
						} else {
							// no choice but to do the rollback
							completedStep.rollback();
							logger.info(LOGTAG + "No account ID. Rollback of step "+completedStep.getStepId()+" completed.");
						}
					} else {
						logger.info(LOGTAG + "rollback of step "+completedStep.getStepId()+" delegated to account deprovisioning ID "+dId);
						completedStep.update(AbstractStep.ROLLBACK_STATUS, AbstractStep.SUCCESS_RESULT, completedStep.getDescription()
								+ "<br>Rollback delegated to account deprovisioning ID "+dId);
					}
				} catch (StepException se) {
					String errMsg = "An error occurred rolling back step " +
							completedStep.getStepId() + ": " +
							completedStep.getType() + ". The exception is: " +
							se.getMessage();
					logger.error(LOGTAG + errMsg);
				}
			}
			long time = System.currentTimeMillis() - startTime;
			logger.info(LOGTAG + "Provisioning rollback complete in " + time + " ms.");

			// All steps completed successfully.
			// Set the end of execution.
			long executionTime = System.currentTimeMillis() - getExecutionStartTime();

			// Update the state of the VPCP object in this transaction.
			queryForVpcpBaseline();

			// Set the status to complete, the result to failure, and the
			// execution time.
			try {
				getVirtualPrivateCloudProvisioning().setStatus(COMPLETED_STATUS);
				getVirtualPrivateCloudProvisioning().setProvisioningResult(FAILURE_RESULT);
				getVirtualPrivateCloudProvisioning().setActualTime(Long.toString(executionTime));
			} catch (EnterpriseFieldException efe) {
				String errMsg = "An error setting field values on the " +
						"VPCP object. The exception is: " + efe.getMessage();
				logger.error(LOGTAG + errMsg);
			}

			// Update the VPCP object.
			try {
				getVirtualPrivateCloudProvisioningProvider()
				.update(getVirtualPrivateCloudProvisioning());
			} catch (ProviderException pe) {
				String errMsg = "An error occurred querying for the  " +
						"current state of a VirtualPrivateCloudProvisioning object. " +
						"The exception is: " + pe.getMessage();
				logger.error(LOGTAG + errMsg);
			}

			// TJ:11/12/2020 - Sprint 4.15
			// The the provider is configured to create an incident
			// in ServiceNow upon failure, create an incident.
			boolean createIncident = Boolean.parseBoolean(getProperties().getProperty("createIncidentOnFailure", "true"));
			if (createIncident) {
				logger.info(LOGTAG + "Creating an Incident " +
						"in ServiceNow...");

				//create an incident.
				IncidentRequisition ir = new IncidentRequisition();
				try {
					ir = (IncidentRequisition) getAppConfig()
							.getObjectByType(ir.getClass().getName());

					// reflectively populate the requisition from the incidentProperties
					Iterator<Object> keys = incidentProperties.keySet().iterator();
					while (keys.hasNext()) {
						Object key = keys.next();
						try {
							Method setter = ir.getClass().getMethod("set" + key, String.class);
							setter.invoke(ir, incidentProperties.getProperty((String) key));

						} catch (NoSuchMethodException e) {
							e.printStackTrace();
							String errMsg = "[NoSuchMethodException] An error occurred populating the "
									+ "IncidentRequisition object from " +
									"AppConfig. The exception is: " + e.getMessage();
							logger.error(LOGTAG + errMsg);
						} catch (SecurityException e) {
							e.printStackTrace();
							String errMsg = "[SecurityException] An error occurred populating the "
									+ "IncidentRequisition object from " +
									"AppConfig. The exception is: " + e.getMessage();
							logger.error(LOGTAG + errMsg);
						} catch (IllegalAccessException e) {
							e.printStackTrace();
							String errMsg = "[IllegalAccessException] An error occurred populating the "
									+ "IncidentRequisition object from " +
									"AppConfig. The exception is: " + e.getMessage();
							logger.error(LOGTAG + errMsg);
						} catch (IllegalArgumentException e) {
							e.printStackTrace();
							String errMsg = "[IllegalArgumentException] An error occurred populating the "
									+ "IncidentRequisition object from " +
									"AppConfig. The exception is: " + e.getMessage();
							logger.error(LOGTAG + errMsg);
						} catch (InvocationTargetException e) {
							e.printStackTrace();
							String errMsg = "[InvocationTargetException] An error occurred populating the "
									+ "IncidentRequisition object from " +
									"AppConfig. The exception is: " + e.getMessage();
							logger.error(LOGTAG + errMsg);
						}
					}

					// put more info in the requisition (description)?
					String provisioningId = this.getProvisioningId();
					String failedStepId = failedStep.getStepId();
					String d = ir.getDescription();
					d = d.replaceAll("PROVISIONING_ID", provisioningId);
					if (extraErrMsg == null) {
						extraErrMsg = "Unknown Error";
					}
					d = d.replaceAll("ERROR_DESCRIPTION", extraErrMsg);
					d = d.replaceAll("STEP_ID", failedStepId);
					ir.setDescription(d);
					logger.info(LOGTAG + "Provisioning Failure incident description is: " + ir.getDescription());

					Incident incident = generateIncident(ir);
					String incidentNumber = incident.getNumber();

					logger.info(LOGTAG + "Provisioning Failure incidentNumber is: " + incidentNumber);
				} catch (EnterpriseConfigurationObjectException ecoe) {
					String errMsg = "An error occurred retrieving an IncidentRequisition object from " +
							"AppConfig. The exception is: " + ecoe.getMessage();
					logger.error(LOGTAG + errMsg);
				} catch (EnterpriseFieldException | ProviderException e) {
					e.printStackTrace();
					String errMsg = "An error occurred generating an Incident.  " +
							"The exception is: " + e.getMessage();
					logger.error(LOGTAG + errMsg);
				}

			} else {
				logger.info(LOGTAG + "createIncidentOnFailure is "
						+ "false. Will not create an incident in "
						+ "ServiceNow.");
			}
		}

		private String resultPropsToXmlString(List<Property> resultProps) {
			String stringProps = "";

			ListIterator<Property> li = resultProps.listIterator();
			while (li.hasNext()) {
				Property prop = li.next();
				try {
					stringProps = stringProps + prop.toXmlString();
				} catch (XmlEnterpriseObjectException xeoe) {
					String errMsg = "An error occurred serializing a Property "
							+ "object to an XML string.";
					logger.error(LOGTAG + errMsg);
				}
			}

			return stringProps;
		}

		private String getProvisioningId() {
			return m_vpcp.getProvisioningId();
		}

		private void setVirtualPrivateCloudProvisioning(VirtualPrivateCloudProvisioning vpcp) {
			m_vpcp = vpcp;
		}

		private VirtualPrivateCloudProvisioning getVirtualPrivateCloudProvisioning() {
			return m_vpcp;
		}

		private void queryForVpcpBaseline() {
			// Query for the VPCP object in the AWS Account Service.
			// Get a configured query spec from AppConfig
			VirtualPrivateCloudProvisioningQuerySpecification vpcpqs = new
					VirtualPrivateCloudProvisioningQuerySpecification();
			try {
				vpcpqs = (VirtualPrivateCloudProvisioningQuerySpecification) getAppConfig()
						.getObjectByType(vpcpqs.getClass().getName());
			} catch (EnterpriseConfigurationObjectException ecoe) {
				String errMsg = "An error occurred retrieving an object from " +
						"AppConfig. The exception is: " + ecoe.getMessage();
				logger.error(LOGTAG + errMsg);
			}

			// Set the values of the query spec.
			try {
				vpcpqs.setProvisioningId(getProvisioningId());
			} catch (EnterpriseFieldException efe) {
				String errMsg = "An error occurred setting the values of the " +
						"VPCP query spec. The exception is: " + efe.getMessage();
				logger.error(LOGTAG + errMsg);
			}

			// Log the state of the query spec.
			try {
				logger.info(LOGTAG + "Query spec is: " + vpcpqs.toXmlString());
			} catch (XmlEnterpriseObjectException xeoe) {
				String errMsg = "An error occurred serializing the query spec " +
						"to XML. The exception is: " + xeoe.getMessage();
				logger.error(LOGTAG + errMsg);
			}

			@SuppressWarnings("rawtypes")
			List results = null;
			try {
				results = getVirtualPrivateCloudProvisioningProvider()
						.query(vpcpqs);
			} catch (ProviderException pe) {
				String errMsg = "An error occurred querying for the  " +
						"current state of a VirtualPrivateCloudProvisioning object. " +
						"The exception is: " + pe.getMessage();
				logger.error(LOGTAG + errMsg);
			}
			VirtualPrivateCloudProvisioning vpcp =
					(VirtualPrivateCloudProvisioning) results.get(0);

			setVirtualPrivateCloudProvisioning(vpcp);
		}

		private void setExecutionStartTime(long time) {
			m_executionStartTime = time;
		}

		private long getExecutionStartTime() {
			return m_executionStartTime;
		}

		public void updateDescription(String string) {
			try {
				theFailedStep.update(COMPLETED_STATUS, FAILURE_RESULT, string);
			} catch (StepException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}		
		}

	}


}

