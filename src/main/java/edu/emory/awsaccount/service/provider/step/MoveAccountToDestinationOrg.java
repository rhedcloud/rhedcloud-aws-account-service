/*******************************************************************************
 $Source: $
 $Revision: $
 *******************************************************************************/

/******************************************************************************
 This file is part of the Emory AWS Account Service.

 Copyright (C) 2017 Emory University. All rights reserved.
 ******************************************************************************/
package edu.emory.awsaccount.service.provider.step;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Properties;

import com.amazon.aws.moa.objects.resources.v1_0.VirtualPrivateCloudRequisition;
import org.openeai.config.AppConfig;
import com.amazon.aws.moa.objects.resources.v1_0.Property;
import com.amazon.aws.moa.objects.resources.v1_0.ProvisioningStep;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.organizations.AWSOrganizationsClient;
import com.amazonaws.services.organizations.AWSOrganizationsClientBuilder;
import com.amazonaws.services.organizations.model.ListAccountsForParentRequest;
import com.amazonaws.services.organizations.model.ListAccountsForParentResult;
import com.amazonaws.services.organizations.model.ListAccountsRequest;
import com.amazonaws.services.organizations.model.ListAccountsResult;
import com.amazonaws.services.organizations.model.MoveAccountRequest;
import com.amazonaws.services.organizations.model.MoveAccountResult;
import edu.emory.awsaccount.service.provider.VirtualPrivateCloudProvisioningProvider;

/**
 * If the account is a standard account, place it in the standard
 * OU, if it is a HIPAA account place it in the HIPAA OU.
 * <P>
 *
 * @author Steve Wheat (swheat@emory.edu)
 * @version 1.0 - 19 December 2018
 **/
public class MoveAccountToDestinationOrg extends AbstractStep implements Step {

    private String m_accessKey = null;
    private String m_secretKey = null;
    private String m_sourceParentId = null;
    private String m_standardDestinationParentId = null;
    private String m_hipaaDestinationParentId = null;
    private String m_destinationParentId = null;

    private AWSOrganizationsClient m_awsOrganizationsClient = null;

    public void init (String provisioningId, Properties props,
            AppConfig aConfig, VirtualPrivateCloudProvisioningProvider vpcpp)
            throws StepException {

        super.init(provisioningId, props, aConfig, vpcpp);

        String LOGTAG = getStepTag() + "[MoveAccountToDestinationOrg.init] ";

        // Get custom step properties.
        logger.info(LOGTAG + "Getting custom step properties...");

        String accessKey = getProperties().getProperty("accessKey", null);
        setAccessKey(accessKey);
        logger.info(LOGTAG + "accessKey is: " + getAccessKey());

        String secretKey = getProperties().getProperty("secretKey", null);
        setSecretKey(secretKey);
        logger.info(LOGTAG + "secretKey is: present");

        String sourceParentId = getProperties().getProperty("sourceParentId", null);
        setSourceParentId(sourceParentId);
        logger.info(LOGTAG + "sourceParentId is: " + getSourceParentId());

        String standardDestinationParentId = getProperties().getProperty("standardDestinationParentId", null);
        setStandardDestinationParentId(standardDestinationParentId);
        logger.info(LOGTAG + "standardDestinationParentId is: " + getStandardDestinationParentId());

        String hipaaDestinationParentId = getProperties().getProperty("hipaaDestinationParentId", null);
        setHipaaDestinationParentId(hipaaDestinationParentId);
        logger.info(LOGTAG + "hipaaDestinationParentId is: " + getHipaaDestinationParentId());

        // Set the AWS account credentials
        BasicAWSCredentials creds = new BasicAWSCredentials(accessKey, secretKey);

        // Instantiate an AWS client builder
        AWSOrganizationsClientBuilder builder = AWSOrganizationsClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(creds));
        builder.setRegion("us-east-1");

        // Initialize the AWS client
        logger.info(LOGTAG+"Initializing AmazonCloudFormationClient...");
        AWSOrganizationsClient client = (AWSOrganizationsClient)builder.build();
        logger.info(LOGTAG+"AWSOrganizationsClient initialized.");
        ListAccountsRequest request = new ListAccountsRequest();

        // Perform a test query
        ListAccountsResult result = client.listAccounts(request);
        logger.info(LOGTAG + "List accounts result: " + result.toString());

        // Set the client
        setAwsOrganizationsClient(client);

        logger.info(LOGTAG + "Initialization complete.");
    }

    protected List<Property> run() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[MoveAccountToDestinationOrg.run] ";
        logger.info(LOGTAG + "Begin running the step.");

        // Return properties
        addResultProperty("stepExecutionMethod", RUN_EXEC_TYPE);

        VirtualPrivateCloudRequisition vpcpr = getVirtualPrivateCloudProvisioning().getVirtualPrivateCloudRequisition();

        String accountId = getStepPropertyValue("GENERATE_NEW_ACCOUNT", "newAccountId");
        if (accountId.equals(PROPERTY_VALUE_NOT_APPLICABLE) || accountId.equals(PROPERTY_VALUE_NOT_AVAILABLE)) {
            accountId = vpcpr.getAccountId();
            if (accountId == null || accountId.equals("")) {
                String errMsg = "No account number for the stack creation can be found. Can't continue.";
                logger.error(LOGTAG + errMsg);
                throw new StepException(errMsg);
            }
        }

        addResultProperty("accountId", accountId);

        // Determine the compliance class of the account.
        String complianceClass = vpcpr.getComplianceClass();
        if (complianceClass.equalsIgnoreCase("HIPAA")) {
            setDestinationParentId(getHipaaDestinationParentId());
        }
        else {
            setDestinationParentId(getStandardDestinationParentId());
        }

        logger.info(LOGTAG + "This account has a compliance class of " + complianceClass + ". Setting destinationParentId to: " + getDestinationParentId());
        addResultProperty("complianceClass", complianceClass);

        // Build the request.
        MoveAccountRequest request = new MoveAccountRequest();
        request.setAccountId(accountId);
        request.setDestinationParentId(getDestinationParentId());
        request.setSourceParentId(getSourceParentId());

        // Send the request.
        try {
            logger.info(LOGTAG + "Sending the move account request...");
            long moveStartTime = System.currentTimeMillis();
            MoveAccountResult result = getAwsOrganizationsClient().moveAccount(request);
            long moveTime = System.currentTimeMillis() - moveStartTime;
            logger.info(LOGTAG + "received response to move account request in " + moveTime + " ms.");
        }
        catch (Exception e) {
            String errMsg = "An error occurred moving the account. The exception is: " + e.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new StepException(errMsg, e);
        }

        addResultProperty("sourceParentId", getSourceParentId());
        addResultProperty("destinationParentId", getDestinationParentId());
        addResultProperty("movedAccount", "true");
        logger.info(LOGTAG + "Successfully moved account " + accountId + "to org unit " + getDestinationParentId());

        // Update the step.
        update(COMPLETED_STATUS, SUCCESS_RESULT);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step run completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    protected List<Property> simulate() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[MoveAccountToAdminOrg.simulate] ";
        logger.info(LOGTAG + "Begin step simulation.");

        // Set return properties.
        addResultProperty("stepExecutionMethod", SIMULATED_EXEC_TYPE);

        // Update the step.
        update(COMPLETED_STATUS, SUCCESS_RESULT);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step simulation completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    protected List<Property> fail() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[MoveAccountToAdminOrg.fail] ";
        logger.info(LOGTAG + "Begin step failure simulation.");

        // Set return properties.
        addResultProperty("stepExecutionMethod", FAILURE_EXEC_TYPE);

        // Update the step.
        update(COMPLETED_STATUS, FAILURE_RESULT);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step failure simulation completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    public void rollback() throws StepException {
        long startTime = System.currentTimeMillis();

        super.rollback();

        String LOGTAG = getStepTag() + "[MoveAccountToAdminOrg.rollback] ";
        logger.info(LOGTAG + "Rollback called, if movedAccount is true, move it back.");

        // Get the createdNewAccount and account number properties
        String newAccountId = getResultProperty("newAccountId");
        boolean movedAccount = Boolean.getBoolean(getResultProperty("movedAccount"));
        boolean isAccountInAdminOu = false;
        boolean movedAccountBackToOrgRoot = false;

        // If newAccountId is not null, determine if the account is still in
        // the destination ou.
        if (newAccountId != null) {
            try {
                ListAccountsForParentRequest request = new ListAccountsForParentRequest();
                request.setParentId(getDestinationParentId());
                ListAccountsForParentResult result = getAwsOrganizationsClient().listAccountsForParent(request);
                List<com.amazonaws.services.organizations.model.Account> accounts = result.getAccounts();
                for (com.amazonaws.services.organizations.model.Account account : accounts) {
                    if (account.getId().equalsIgnoreCase(newAccountId))
                        isAccountInAdminOu = true;
                }
            }
            catch (Exception e) {
                String errMsg = "An error occurred querying for a list of accounts in the admin org. The exception is: " + e.getMessage();
                logger.error(LOGTAG + errMsg);
                throw new StepException(errMsg, e);
            }
        }

        // If the movedAccount is true and isAccountInAdminOrg is true, move the account to the org root.
        if (movedAccount && isAccountInAdminOu) {
            // Build the request.
            MoveAccountRequest request = new MoveAccountRequest();
            request.setAccountId(newAccountId);
            request.setDestinationParentId(getSourceParentId());
            request.setSourceParentId(getDestinationParentId());

            // Send the request.
            try {
                logger.info(LOGTAG + "Sending the move account request...");
                long moveStartTime = System.currentTimeMillis();
                getAwsOrganizationsClient().moveAccount(request);
                long moveTime = System.currentTimeMillis() - moveStartTime;
                logger.info(LOGTAG + "received response to move account request in " + moveTime + " ms.");
                movedAccountBackToOrgRoot = true;
            }
            catch (Exception e) {
                String errMsg = "An error occurred moving the account. The exception is: " + e.getMessage();
                logger.error(LOGTAG + errMsg);
                throw new StepException(errMsg, e);
            }
            addResultProperty("isAccountInAdminOu", Boolean.toString(isAccountInAdminOu));
            addResultProperty("movedAccountBackToOrgRoot", Boolean.toString(movedAccountBackToOrgRoot));

        }
        // If movedAccount or isAccountInAdminOrg is false, there is nothing to roll back. Log it.
        else {
            logger.info(LOGTAG + "No account was created or it is no longer in the organization root, so there is nothing to roll back.");
            addResultProperty("isAccountInAdminOu", Boolean.toString(isAccountInAdminOu));
            addResultProperty("movedAccountBackToOrgRoot", "not applicable");
        }

        update(ROLLBACK_STATUS, SUCCESS_RESULT);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Rollback completed in " + time + "ms.");
    }

    private void setAwsOrganizationsClient(AWSOrganizationsClient client) {
        m_awsOrganizationsClient = client;
    }

    private AWSOrganizationsClient getAwsOrganizationsClient() {
        return m_awsOrganizationsClient;
    }

    private void setAccessKey (String accessKey) throws StepException {
        if (accessKey == null) {
            String errMsg = "accessKey property is null. Can't continue.";
            throw new StepException(errMsg);
        }

        m_accessKey = accessKey;
    }

    private String getAccessKey() {
        return m_accessKey;
    }

    private void setSecretKey (String secretKey) throws StepException {
        if (secretKey == null) {
            String errMsg = "secretKey property is null. Can't continue.";
            throw new StepException(errMsg);
        }

        m_secretKey = secretKey;
    }

    private String getSecretKey() {
        return m_secretKey;
    }

    private void setSourceParentId (String id) throws StepException {
        if (id == null) {
            String errMsg = "sourceParentId property is null. Can't continue.";
            throw new StepException(errMsg);
        }

        m_sourceParentId = id;
    }

    private String getSourceParentId() {
        return m_sourceParentId;
    }

    private void setDestinationParentId (String id) {
        m_destinationParentId = id;
    }

    private String getDestinationParentId() {
        return m_destinationParentId;
    }

    private void setStandardDestinationParentId (String id) throws StepException {
        if (id == null) {
            String errMsg = "standardDestinationParentId property is null. Can't continue.";
            throw new StepException(errMsg);
        }

        m_standardDestinationParentId = id;
    }

    private String getStandardDestinationParentId() {
        return m_standardDestinationParentId;
    }

    private void setHipaaDestinationParentId (String id) throws StepException {
        if (id == null) {
            String errMsg = "hipaaDestinationParentId property is null. Can't continue.";
            throw new StepException(errMsg);
        }

        m_hipaaDestinationParentId = id;
    }

    private String getHipaaDestinationParentId() {
        return m_hipaaDestinationParentId;
    }
}
