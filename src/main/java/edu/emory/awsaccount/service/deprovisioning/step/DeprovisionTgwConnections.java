package edu.emory.awsaccount.service.deprovisioning.step;

import com.amazon.aws.moa.objects.resources.v1_0.Property;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.AmazonEC2Exception;
import com.amazonaws.services.ec2.model.DeleteTransitGatewayVpcAttachmentRequest;
import com.amazonaws.services.ec2.model.DescribeTransitGatewayAttachmentsRequest;
import com.amazonaws.services.ec2.model.DescribeTransitGatewayAttachmentsResult;
import com.amazonaws.services.ec2.model.Filter;
import com.amazonaws.services.ec2.model.TransitGatewayAttachment;
import com.amazonaws.services.securitytoken.AWSSecurityTokenService;
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClientBuilder;
import com.amazonaws.services.securitytoken.model.AssumeRoleRequest;
import com.amazonaws.services.securitytoken.model.AssumeRoleResult;
import com.amazonaws.services.securitytoken.model.Credentials;
import edu.emory.awsaccount.service.provider.AccountDeprovisioningProvider;
import org.openeai.config.AppConfig;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * Deprovision TGW connections for all VPCs associated with an account.
 */
public class DeprovisionTgwConnections extends AbstractStep implements Step {
    private String accessKeyId = null;
    private String secretKey = null;
    private String roleArnPattern = null;
    private int roleAssumptionDurationSeconds = 0;
    private List<String> regions;

    public void init(String provisioningId, Properties props, AppConfig aConfig, AccountDeprovisioningProvider adp) throws StepException {
        super.init(provisioningId, props, aConfig, adp);

        String LOGTAG = getStepTag() + "[DeprovisionTgwConnections.init] ";

        // Get custom step properties.
        logger.info(LOGTAG + "Getting custom step properties...");

        accessKeyId = getProperties().getProperty("accessKeyId");
        logger.info(LOGTAG + "accessKeyId is: " + accessKeyId);

        secretKey = getProperties().getProperty("secretKey");
        logger.info(LOGTAG + "secretKey is: present");

        roleArnPattern = getProperties().getProperty("roleArnPattern");
        logger.info(LOGTAG + "roleArnPattern property is: " + roleArnPattern);

        roleAssumptionDurationSeconds = Integer.parseInt(getProperties().getProperty("roleAssumptionDurationSeconds", "30"));
        logger.info(LOGTAG + "roleAssumptionDurationSeconds is: " + roleAssumptionDurationSeconds);

        String regionNames = getProperties().getProperty("regions", "");
        regions = Arrays.asList(regionNames.split("\\s*,\\s*"));
        if (regions.isEmpty()) {
            String message = "regions cannot be null or empty";
            logger.error(LOGTAG + message);
            throw new StepException(message);
        }
        logger.info(LOGTAG + "regions is: " + regions);

        logger.info(LOGTAG + "Initialization complete.");
    }

    protected List<Property> run() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[DeprovisionTgwConnections.run] ";
        logger.info(LOGTAG + "Begin running the step.");

        addResultProperty("stepExecutionMethod", RUN_EXEC_TYPE);


        // Get the list of VPCs from a previous step.
        String vpcIds = getStepPropertyValue("LIST_VPC_IDS", "tgwVpcIds");

        // If there are no VPCs there is nothing to do and the step is complete.
        if (vpcIds.equals(PROPERTY_VALUE_NOT_AVAILABLE) || vpcIds.equals("none")) {
            logger.info(LOGTAG + "There are no TGW VPCs.");
            // Update the step.
            update(COMPLETED_STATUS, SUCCESS_RESULT);

            // Log completion time.
            long time = System.currentTimeMillis() - startTime;
            logger.info(LOGTAG + "Step run completed in " + time + "ms.");

            // Return the properties.
            return getResultProperties();
        }

        // Get the VPCs as a list.
        List<String> vpcList = Arrays.asList(vpcIds.split("\\s*,\\s*"));
        logger.info(LOGTAG + "There are " + vpcList.size() + " TGW VPCs.");

        // If there are no VPCs in the list there is nothing to do and the step is complete.
        if (vpcList.size() == 0) {
            logger.info(LOGTAG + "There are no TGW VPCs.");
            // Update the step.
            update(COMPLETED_STATUS, SUCCESS_RESULT);

            // Log completion time.
            long time = System.currentTimeMillis() - startTime;
            logger.info(LOGTAG + "Step run completed in " + time + "ms.");

            // Return the properties.
            return getResultProperties();
        }

        String accountId = getAccountDeprovisioning().getAccountDeprovisioningRequisition().getAccountId();

        /*
         * the first step is to figure out what region the VPC is in by looking for the TGW attachment.
         * if a TGW attachment isn't found for the VPC, it is not considered an error because a previous
         * deprovisioning run could have deleted the TGW attachment already.
         *
         * once the region is determined, delete the TGW attachment.
         * this can fail because a previous deprovisioning run could have deleted the TGW attachment already
         * but it doesn't immediately disappear from the list returned by describeTransitGatewayAttachments().
         * generally, if it's already been deleted further operations will result in an IncorrectState exception.
         */
        try {
            for (String vpcId : vpcList) {
                AmazonEC2Client memberEc2Client = null;  // assume role in the linked/member account
                String vpcRegion = null;
                TransitGatewayAttachment tgwAttachment = null;

                /*
                 * figure out what region the VPC is in by looking for the TGW attachment
                 */
                logger.info(LOGTAG + "Finding the region for VPC " + vpcId);
                foundRegion:
                for (String region : regions) {
                    logger.info(LOGTAG + "Building EC2 client for account " + accountId + " in region " + region);
                    memberEc2Client = buildAmazonEC2Client(accountId, region);

                    DescribeTransitGatewayAttachmentsRequest describeTransitGatewayAttachmentsRequest
                            = new DescribeTransitGatewayAttachmentsRequest()
                                    .withFilters(new Filter("resource-type").withValues("vpc"),
                                                 new Filter("resource-id").withValues(vpcId));
                    DescribeTransitGatewayAttachmentsResult describeTransitGatewayAttachmentsResult;
                    do {
                        logger.info(LOGTAG + "Describe Transit Gateway Attachments for " + vpcId + " in region " + region);

                        describeTransitGatewayAttachmentsResult = memberEc2Client.describeTransitGatewayAttachments(describeTransitGatewayAttachmentsRequest);
                        // there should only be one VPC attachment that was created by the CFN template
                        if (describeTransitGatewayAttachmentsResult.getTransitGatewayAttachments().size() > 0) {
                            vpcRegion = region;
                            tgwAttachment = describeTransitGatewayAttachmentsResult.getTransitGatewayAttachments().get(0);
                            break foundRegion;
                        }
                        describeTransitGatewayAttachmentsRequest.setNextToken(describeTransitGatewayAttachmentsResult.getNextToken());
                    } while (describeTransitGatewayAttachmentsResult.getNextToken() != null);
                }

                if (vpcRegion == null) {
                    String msg = "Could not find a Transit Gateway VPC attachment for VPC " + vpcId + " in any region";
                    logger.info(LOGTAG + msg);

                    // The step is done. Update the step.
                    update(COMPLETED_STATUS, SUCCESS_RESULT);

                    // Log completion time.
                    long time = System.currentTimeMillis() - startTime;
                    logger.info(LOGTAG + "Step run completed in " + time + "ms.");

                    // Return the properties.
                    return getResultProperties();
                }

                logger.info(LOGTAG + "VPC " + vpcId + " is in region " + vpcRegion + " with Transit Gateway Attachment " + tgwAttachment);

                /*
                 * now, delete the VPC attachment from the TGW.
                 * this also takes care of disassociating the TGW route table and
                 * disabling the TGW route table propagations.
                 */
                try {
                    logger.info(LOGTAG + "Delete Transit Gateway VPC Attachment for "
                            + tgwAttachment.getTransitGatewayAttachmentId());

                    DeleteTransitGatewayVpcAttachmentRequest deleteTransitGatewayVpcAttachmentRequest = new DeleteTransitGatewayVpcAttachmentRequest()
                            .withTransitGatewayAttachmentId(tgwAttachment.getTransitGatewayAttachmentId());
                    memberEc2Client.deleteTransitGatewayVpcAttachment(deleteTransitGatewayVpcAttachmentRequest);
                }
                catch (AmazonEC2Exception e) {
                    // an IncorrectState exception is not considered an error.  see discussion above.
                    if (e.getErrorCode().equals("IncorrectState")) {
                        logger.info(LOGTAG + "Ignoring IncorrectState exception while deleting TGW VPC attachment");
                    } else {
                        throw e;
                    }
                }
            }
        }
        catch (Exception e) {
            String errMsg = "An error occurred deprovisioning the TGW. The exception is: " + e.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new StepException(errMsg, e);
        }

        // The step is done. Update the step.
        update(COMPLETED_STATUS, SUCCESS_RESULT);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step run completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    protected List<Property> simulate() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[DeprovisionTgwConnections.simulate] ";
        logger.info(LOGTAG + "Begin step simulation.");

        // Set return properties.
        addResultProperty("stepExecutionMethod", SIMULATED_EXEC_TYPE);

        // Update the step.
        update(COMPLETED_STATUS, SUCCESS_RESULT);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step simulation completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    protected List<Property> fail() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[DeprovisionTgwConnections.fail] ";
        logger.info(LOGTAG + "Begin step failure simulation.");

        // Set return properties.
        addResultProperty("stepExecutionMethod", FAILURE_EXEC_TYPE);

        // Update the step.
        update(COMPLETED_STATUS, FAILURE_RESULT);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step failure simulation completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    public void rollback() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[DeprovisiongVpnConnections.rollback] ";

        update(ROLLBACK_STATUS, SUCCESS_RESULT);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Rollback completed in " + time + "ms.");
    }

    /**
     * Amazon EC2 client connected to the correct account with the correct role
     */
    private AmazonEC2Client buildAmazonEC2Client(String accountId, String region) {
        String roleArn = roleArnPattern.replace("ACCOUNT_NUMBER", accountId);

        // use the master account credentials to get the STS client
        BasicAWSCredentials masterCredentials = new BasicAWSCredentials(accessKeyId, secretKey);
        AWSStaticCredentialsProvider cp = new AWSStaticCredentialsProvider(masterCredentials);

        AWSSecurityTokenService sts = AWSSecurityTokenServiceClientBuilder.standard()
                .withCredentials(cp)
                .withRegion(region)
                .build();

        AssumeRoleRequest assumeRequest = new AssumeRoleRequest()
                .withRoleArn(roleArn)
                .withDurationSeconds(roleAssumptionDurationSeconds)
                .withRoleSessionName("AwsAccountService");

        AssumeRoleResult assumeResult = sts.assumeRole(assumeRequest);
        Credentials credentials = assumeResult.getCredentials();

        // now use the session credentials to get the EC2 client
        BasicSessionCredentials sessionCredentials = new BasicSessionCredentials(credentials.getAccessKeyId(),
                credentials.getSecretAccessKey(), credentials.getSessionToken());
        cp = new AWSStaticCredentialsProvider(sessionCredentials);

        return (AmazonEC2Client) AmazonEC2ClientBuilder.standard()
                .withCredentials(cp)
                .withRegion(region)
                .build();
    }
}
