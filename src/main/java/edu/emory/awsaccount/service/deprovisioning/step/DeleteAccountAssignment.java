package edu.emory.awsaccount.service.deprovisioning.step;

import java.util.List;
import java.util.Properties;

import javax.jms.JMSException;

import org.openeai.OpenEaiException;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.transport.RequestService;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.Account;
import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.AccountAssignment;
import com.amazon.aws.moa.objects.resources.v1_0.AccountAssignmentQuerySpecification;
import com.amazon.aws.moa.objects.resources.v1_0.AccountAssignmentRequisition;
import com.amazon.aws.moa.objects.resources.v1_0.AccountQuerySpecification;
import com.amazon.aws.moa.objects.resources.v1_0.Property;

import edu.emory.awsaccount.service.provider.AccountDeprovisioningProvider;

public class DeleteAccountAssignment extends AbstractStep implements Step {
	private static final String LOGTAG_NAME = "DeleteAccountFromPermissionSet";
	private String createLogTag(String suffix) { return "["+LOGTAG_NAME + "." + suffix + "] "; }
	private String permissionSetName;
	private ProducerPool p2p1;
	private ProducerPool p2pAas;
	private int requestTimeoutIntervalInMillis;



	@SuppressWarnings("unused")
	@Override
	public void init(String deprovisioningId, Properties props, AppConfig aConfig, AccountDeprovisioningProvider adp) throws StepException {
		super.init(deprovisioningId, props, aConfig, adp);

		String LOGTAG = createLogTag("init");
		logger.info(LOGTAG + "Begin step init.");

		// This step needs to send messages to the IDM service
		// to create, query and delete account roles and to the
		// AAS to query for the account creator and owner
		try {
			p2p1 = (ProducerPool)getAppConfig()
					.getObject("IdmServiceProducerPool");
			p2pAas = (ProducerPool)getAppConfig()
					.getObject("AwsAccountServiceProducerPool");
		}
		catch (EnterpriseConfigurationObjectException ecoe) {
			// An error occurred retrieving an object from AppConfig. Log it and
			// throw an exception.
			String errMsg = "An error occurred retrieving an object from " +
					"AppConfig. The exception is: " + ecoe.getMessage();
			logger.fatal(LOGTAG + errMsg);
			throw new StepException(errMsg);
		}

		// Get custom step properties.
		logger.info(LOGTAG + "Getting custom step properties...");

		permissionSetName = getProperties().getProperty("permissionSet");
		logger.info(LOGTAG + "permissionSet is: " + permissionSetName);

		try {
			requestTimeoutIntervalInMillis = Integer.parseInt(getProperties().getProperty("requestTimeoutIntervalInMillis","10000"));
		} catch (NumberFormatException e) {
			String errMsg="The requestTimeoutIntervalInMillis property must be an integer.";
			logger.fatal(LOGTAG+errMsg);
			throw new StepException(errMsg,e);			
		}
		logger.info(LOGTAG + "requestTimeoutIntervalInMillis is: " + requestTimeoutIntervalInMillis);

		try {
			AccountAssignment aa = (AccountAssignment) getAppConfig().getObject("AccountAssignment.v1_0");
			AccountAssignmentRequisition aar = (AccountAssignmentRequisition) getAppConfig().getObject("AccountAssignmentRequisition.v1_0");
			AccountAssignmentQuerySpecification aaq = (AccountAssignmentQuerySpecification) getAppConfig().getObject("AccountAssignmentQuerySpecification.v1_0");
			Account acc = (Account) getAppConfig().getObject("Account.v1_0");
			AccountQuerySpecification accq = (AccountQuerySpecification) getAppConfig().getObject("AccountQuerySpecification.v1_0");
		} catch (EnterpriseConfigurationObjectException e) {
			String errMsg="The appconfig is missing AccountAssignment.v1_0 and/or AccountAssignmentRequisition.v1_0 and/or AccountAssignmentQuerySpecification.v1_0";
			logger.fatal(LOGTAG+errMsg);
			throw new StepException(errMsg,e);
		}

		logger.info(LOGTAG + "Initialization complete.");
	}

	@Override
	protected List<Property> simulate() throws StepException {
		long startTime = System.currentTimeMillis();

		String LOGTAG = createLogTag("simulate");
		logger.info(LOGTAG + "Begin step simulation.");

		addResultProperty("stepExecutionMethod", SIMULATED_EXEC_TYPE);
		addResultProperty("isAuthorized", "true");

		update(COMPLETED_STATUS, SUCCESS_RESULT);

		long time = System.currentTimeMillis() - startTime;
		logger.info(LOGTAG + "Step simulation completed in " + time + "ms.");

		return getResultProperties();
	}

	private RequestService getIdmProducer(String LOGTAG) throws StepException {
		try {
			PointToPointProducer p2p =
					(PointToPointProducer) p2p1.getProducer();
			p2p.setRequestTimeoutInterval(requestTimeoutIntervalInMillis);
			return (RequestService)p2p;
		}
		catch (JMSException jmse) {
			String errMsg = "An error occurred getting a producer " +
					"from the pool. The exception is: " + jmse.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new StepException(errMsg, jmse);
		}
	}

	private RequestService getAasProducer(String LOGTAG) throws StepException {
		try {
			PointToPointProducer p2p =
					(PointToPointProducer) p2pAas.getProducer();
			p2p.setRequestTimeoutInterval(requestTimeoutIntervalInMillis);
			return (RequestService)p2p;
		}
		catch (JMSException jmse) {
			String errMsg = "An error occurred getting a producer " +
					"from the pool. The exception is: " + jmse.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new StepException(errMsg, jmse);
		}
	}

	@SuppressWarnings("unchecked")
	private void processAccountAssignments(String mode,String LOGTAG) throws StepException {


		String accountId = getAccountDeprovisioning().getAccountDeprovisioningRequisition().getAccountId();
		addResultProperty("accountId", accountId);
		logger.info(LOGTAG+"accountId is "+accountId);

		if ("CREATE".equals(mode)) {
			try {
				Account acc = (Account) getAppConfig().getObject("Account.v1_0");
				AccountQuerySpecification accq = (AccountQuerySpecification) getAppConfig().getObject("AccountQuerySpecification.v1_0");
				accq.setAccountId(accountId);
				List<Account> accounts = acc.query(accq,getAasProducer(LOGTAG));
				if (accounts.size()==0) {
					String msg = "No account object found for ID "+accountId;
					logger.warn(LOGTAG+msg);
					addResultProperty("status",msg);
					return;
				}
				logger.info(LOGTAG+"Creating account assignments for the owner of account "+accountId);
				Account candidate = accounts.get(0);
				String accountOwnerId = candidate.getAccountOwnerId();
				AccountAssignment aa = (AccountAssignment) getAppConfig().getObject("AccountAssignment.v1_0");
				AccountAssignmentRequisition aar = (AccountAssignmentRequisition) getAppConfig().getObject("AccountAssignmentRequisition.v1_0");
				aar.setPermissionSetName(permissionSetName);
				aar.setPrincipalType("USER");
				aar.setPublicId(accountOwnerId);
				aar.setTargetId(accountId);
				aar.setTargetType("AWS_ACCOUNT");
				RequestService rs = getIdmProducer(LOGTAG);
							
				addResultProperty(accountOwnerId+".status","account assignment created for account owner");
				if (candidate.getCreateUser()!=null && !candidate.getCreateUser().equals(accountOwnerId)) {
					aar.setPublicId(candidate.getCreateUser());
					@SuppressWarnings("unused")
					List<AccountAssignment> result = aa.generate(aar, rs);
					addResultProperty(candidate.getCreateUser()+".status","account assignment created for create user");
				}
			} catch (OpenEaiException e) {
				String msg = "OpenEaiException while creating account assignments";
				logger.error(LOGTAG+msg);
				throw new StepException(msg,e);
			} 
		}	

		if ("DELETE".equals(mode)) {
			AccountAssignment aa;
			try {
				aa = (AccountAssignment) getAppConfig().getObject("AccountAssignment.v1_0");
				AccountAssignmentQuerySpecification aaq = (AccountAssignmentQuerySpecification) getAppConfig().getObject("AccountAssignmentQuerySpecification.v1_0");
				aaq.setPermissionSetName(permissionSetName);			
				aaq.setPrincipalType("USER");
				aaq.setTargetId(accountId);
				aaq.setTargetType("AWS_ACCOUNT");

				RequestService rs = getIdmProducer(LOGTAG);
				List<AccountAssignment> accountAssignments = aa.query(aaq, rs);
				if (accountAssignments.size()==0) {
					String msg = "No account assignments for account "+accountId+" with permission set "+permissionSetName;
					logger.warn(LOGTAG+msg);
					addResultProperty("status",msg);
					return;
				}
				logger.info(LOGTAG+"Deleting "+accountAssignments.size()+" account assignment(s)...");
				for(AccountAssignment candidate: accountAssignments) {
					logger.info(LOGTAG+"Deleting account assignment for principal ID"+candidate.getPublicId());
					candidate.delete(accountId, rs);
					String msg = "Account assignment deleted.";
					logger.info(LOGTAG+msg);
					addResultProperty(candidate.getPublicId()+".status",msg);
				}
				return;
			} catch (OpenEaiException e) {
				String msg = "OpenEaiException while deleting account assignments";
				logger.error(LOGTAG+msg);
				throw new StepException(msg,e);
			} 

		}


	}


	@Override
	protected List<Property> run() throws StepException {
		long startTime = System.currentTimeMillis();

		String LOGTAG = createLogTag("run");
		logger.info(LOGTAG + "Begin running the step.");

		processAccountAssignments("DELETE", LOGTAG);

		// Set result properties.
		addResultProperty("stepExecutionMethod", RUN_EXEC_TYPE);

		update(COMPLETED_STATUS, SUCCESS_RESULT);

		long time = System.currentTimeMillis() - startTime;
		logger.info(LOGTAG + "Step completed in " + time + "ms.");

		return getResultProperties();
	}

	@Override
	protected List<Property> fail() throws StepException {
		long startTime = System.currentTimeMillis();
		String LOGTAG = getStepTag() + createLogTag("fail");

		logger.info(LOGTAG + "Begin step failure simulation.");

		// Set return properties.
		addResultProperty("stepExecutionMethod", FAILURE_EXEC_TYPE);

		// Update the step.
		update(COMPLETED_STATUS, FAILURE_RESULT);

		// Log completion time.
		long time = System.currentTimeMillis() - startTime;
		logger.info(LOGTAG + "Step failure simulation completed in " + time + "ms.");

		// Return the properties.
		return getResultProperties();
	}

	public void rollback() throws StepException {

		super.rollback();

		long startTime = System.currentTimeMillis();
		String LOGTAG = getStepTag() + createLogTag("rollback");
		logger.info(LOGTAG + "Begin rolling back the step.");
		processAccountAssignments("CREATE", LOGTAG);

		// Set result properties.
		addResultProperty("stepExecutionMethod", RUN_EXEC_TYPE);


		long time = System.currentTimeMillis() - startTime;
		logger.info(LOGTAG + "Step completed in " + time + "ms.");

		update(ROLLBACK_STATUS, SUCCESS_RESULT);

	}
}
