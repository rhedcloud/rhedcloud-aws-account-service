package edu.emory.awsaccount.service.deprovisioning.step;

import java.util.List;
import java.util.ListIterator;
import java.util.Properties;

import javax.jms.JMSException;

import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.jms.producer.MessageProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.moa.EnterpriseObjectQueryException;
import org.openeai.transport.RequestService;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.VirtualPrivateCloud;
import com.amazon.aws.moa.objects.resources.v1_0.Property;
import com.amazon.aws.moa.objects.resources.v1_0.VirtualPrivateCloudQuerySpecification;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.ModifyVpcEndpointServicePermissionsRequest;
import com.amazonaws.services.securitytoken.AWSSecurityTokenService;
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClientBuilder;
import com.amazonaws.services.securitytoken.model.AssumeRoleRequest;
import com.amazonaws.services.securitytoken.model.AssumeRoleResult;
import com.amazonaws.services.securitytoken.model.Credentials;

import edu.emory.awsaccount.service.provider.AccountDeprovisioningProvider;

/**
 * Delete the VPC endpoint service permissions for the account from the security account.
 * <P>
 *
 * @author Tod Jackson (jtjacks@emory.edu)
 * @version 1.0 - 22 February 2022
 */
public class DeleteVpcEndpointServicePermissions extends AbstractStep implements Step {
    private String accessKeyId = null;
    private String secretKey = null;
    private String roleAssumptionArn = null;
    private int roleAssumptionDurationSeconds = 0;
    private String modifyVpcEndpointServicePermissionsArnPattern=null;
    private ProducerPool m_awsAccountServiceProducerPool = null;

    public void init(String provisioningId, Properties props, AppConfig aConfig, AccountDeprovisioningProvider adp) throws StepException {
        super.init(provisioningId, props, aConfig, adp);

        String LOGTAG = getStepTag() + "[DeleteVpcEndpointServicePermissions.init] ";

        // Get custom step properties.
        logger.info(LOGTAG + "Getting custom step properties...");

        accessKeyId = getProperties().getProperty("accessKeyId");
        logger.info(LOGTAG + "accessKeyId is: " + accessKeyId);

        secretKey = getProperties().getProperty("secretKey");
        logger.info(LOGTAG + "secretKey is: present");

        roleAssumptionArn = getProperties().getProperty("roleAssumptionArn");
        logger.info(LOGTAG + "roleAssumptionArn property is: " + roleAssumptionArn);

        roleAssumptionDurationSeconds = Integer.parseInt(getProperties().getProperty("roleAssumptionDurationSeconds", "30"));
        logger.info(LOGTAG + "roleAssumptionDurationSeconds is: " + roleAssumptionDurationSeconds);

        modifyVpcEndpointServicePermissionsArnPattern = getProperties().getProperty("modifyVpcEndpointServicePermissionsArnPattern", null);
        logger.info(LOGTAG + "modifyVpcEndpointServicePermissionsArnPattern is: " + modifyVpcEndpointServicePermissionsArnPattern);

        ProducerPool p2p1 = null;
        try {
            p2p1 = (ProducerPool)getAppConfig()
                .getObject("AwsAccountServiceProducerPool");
            setAwsAccountServiceProducerPool(p2p1);
        }
        catch (EnterpriseConfigurationObjectException ecoe) {
            // An error occurred retrieving an object from AppConfig. Log it and
            // throw an exception.
            String errMsg = "An error occurred retrieving an object from " +
                    "AppConfig. The exception is: " + ecoe.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new StepException(errMsg);
        }
        
        logger.info(LOGTAG + "Initialization complete.");
    }

    private List<VirtualPrivateCloud> getVpcsForAccount(String accountId) throws StepException {
        String LOGTAG = getStepTag() + "[DeleteVpcEndpointServicePermissions.getVpcsForAccount] ";
    	List<VirtualPrivateCloud> vpcs = new java.util.ArrayList<VirtualPrivateCloud>();

        // Query for the VPCs for this accountId
        // Get a configured VPC object and account query spec
        // from AppConfig.
        VirtualPrivateCloud vpc = new VirtualPrivateCloud();
        VirtualPrivateCloudQuerySpecification querySpec =
            new VirtualPrivateCloudQuerySpecification();
        try {
            vpc = (VirtualPrivateCloud)getAppConfig()
                .getObjectByType(vpc.getClass().getName());
            querySpec = (VirtualPrivateCloudQuerySpecification)getAppConfig()
                    .getObjectByType(querySpec.getClass().getName());
        }
        catch (EnterpriseConfigurationObjectException ecoe) {
            String errMsg = "An error occurred retrieving an object from " +
              "AppConfig. The exception is: " + ecoe.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new StepException(errMsg, ecoe);
        }

        // Set the values of the query spec
        try {
            querySpec.setAccountId(accountId);
        }
        catch (EnterpriseFieldException efe) {
            String errMsg = "An error occurred setting a field value. " +
                "The exception is: " + efe.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new StepException();
        }

        // Get a producer from the pool
        RequestService rs = null;
        try {
            rs = (RequestService)getAwsAccountServiceProducerPool()
                .getExclusiveProducer();
        }
        catch (JMSException jmse) {
            String errMsg = "An error occurred getting a producer " +
                "from the pool. The exception is: " + jmse.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new StepException(errMsg, jmse);
        }

        // Query for the VPC metadata
        List results = null;
        try {
            long queryStartTime = System.currentTimeMillis();
            results = vpc.query(querySpec, rs);
            if (results.size() > 0) {
                String deletedVpcMetadata = "";
                ListIterator li = results.listIterator();
                while (li.hasNext()) {
                    VirtualPrivateCloud avpc = (VirtualPrivateCloud)li.next();
                    vpcs.add(avpc);
                }
            }

            long createTime = System.currentTimeMillis() - queryStartTime;
            logger.info(LOGTAG + "Queried for VPC in " + createTime +
                " ms. Got " + results.size() + " result(s).");
        }
        catch (EnterpriseObjectQueryException eoqe) {
            String errMsg = "An error occurred querying for the object. " +
              "The exception is: " + eoqe.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new StepException(errMsg, eoqe);
        }
        finally {
            // Release the producer back to the pool
            getAwsAccountServiceProducerPool()
                .releaseProducer((MessageProducer)rs);
        }
    	
    	return vpcs;
    }
    protected List<Property> run() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[DeleteVpcEndpointServicePermissions.run] ";
        logger.info(LOGTAG + "Begin running the step.");

        addResultProperty("stepExecutionMethod", RUN_EXEC_TYPE);

        String accountId = getAccountDeprovisioning().getAccountDeprovisioningRequisition().getAccountId();
        

        // query for the VPC(s) associated to this account to get each VPC's region
        List<VirtualPrivateCloud> vpcs = getVpcsForAccount(accountId);
        for (VirtualPrivateCloud vpc : vpcs) {
            String region = vpc.getRegion();
            AmazonEC2Client client = buildAmazonEC2Client(accountId, region);
        	String roleArn = modifyVpcEndpointServicePermissionsArnPattern.replace("ACCOUNT_NUMBER", accountId);
            ModifyVpcEndpointServicePermissionsRequest request = new ModifyVpcEndpointServicePermissionsRequest();
            		
            String endPointServiceId = getProperties().getProperty("glbEndpointServiceId-" + region, null);
            logger.info(LOGTAG + "endPointServiceId is: " + endPointServiceId);

            try {
                // Build the request.
                request
                	.withRemoveAllowedPrincipals(roleArn)
                	.withServiceId(endPointServiceId);

                try {
                    logger.info(LOGTAG + "Sending the encryption by default request...");
                    long queryStartTime = System.currentTimeMillis();
                    client.modifyVpcEndpointServicePermissions(request);
                    long queryTime = System.currentTimeMillis() - queryStartTime;
                    logger.info(LOGTAG + "received response to encryption by default request in " + queryTime + " ms.");
                } catch (Exception e) {
                    String errMsg = "An error occurred setting EBS encryption by default. The exception is: " + e.getMessage();
                    logger.error(LOGTAG + errMsg);
                    throw new StepException(errMsg, e);
                }
            }
            catch (Exception e) {
                String errMsg = "An error occurred deprovisioning the TGW. The exception is: " + e.getMessage();
                logger.error(LOGTAG + errMsg);
                throw new StepException(errMsg, e);
            }
        }

        // The step is done. Update the step.
        update(COMPLETED_STATUS, SUCCESS_RESULT);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step run completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    protected List<Property> simulate() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[DeleteVpcEndpointServicePermissions.simulate] ";
        logger.info(LOGTAG + "Begin step simulation.");

        // Set return properties.
        addResultProperty("stepExecutionMethod", SIMULATED_EXEC_TYPE);

        // Update the step.
        update(COMPLETED_STATUS, SUCCESS_RESULT);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step simulation completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    protected List<Property> fail() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[DeleteVpcEndpointServicePermissions.fail] ";
        logger.info(LOGTAG + "Begin step failure simulation.");

        // Set return properties.
        addResultProperty("stepExecutionMethod", FAILURE_EXEC_TYPE);

        // Update the step.
        update(COMPLETED_STATUS, FAILURE_RESULT);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step failure simulation completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    public void rollback() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[DeleteVpcEndpointServicePermissions.rollback] ";

        update(ROLLBACK_STATUS, SUCCESS_RESULT);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Rollback completed in " + time + "ms.");
    }

    /**
     * Amazon EC2 client connected to the correct account with the correct role
     */
    private AmazonEC2Client buildAmazonEC2Client(String accountId, String region) {
        String LOGTAG = getStepTag() + "[buildAmazonEC2Client] ";

        logger.info(LOGTAG + "The account targeted by this request is: " + accountId);
        logger.info(LOGTAG + "The roleAssumptionArn is: " + roleAssumptionArn);
        logger.info(LOGTAG + "The region targeted by this request is: " + region);

        // use the master account credentials to get the STS client
        BasicAWSCredentials masterCredentials = new BasicAWSCredentials(accessKeyId, secretKey);
        AWSStaticCredentialsProvider cp = new AWSStaticCredentialsProvider(masterCredentials);

        AWSSecurityTokenService sts = AWSSecurityTokenServiceClientBuilder.standard()
                .withCredentials(cp)
                .withRegion(region)
                .build();

        AssumeRoleRequest assumeRequest = new AssumeRoleRequest()
                .withRoleArn(roleAssumptionArn)
                .withDurationSeconds(roleAssumptionDurationSeconds)
                .withRoleSessionName("AwsAccountService");

        AssumeRoleResult assumeResult = sts.assumeRole(assumeRequest);
        Credentials credentials = assumeResult.getCredentials();

        // now use the session credentials to get the EC2 client
        BasicSessionCredentials sessionCredentials = new BasicSessionCredentials(credentials.getAccessKeyId(),
                credentials.getSecretAccessKey(), credentials.getSessionToken());
        cp = new AWSStaticCredentialsProvider(sessionCredentials);

        return (AmazonEC2Client) AmazonEC2ClientBuilder.standard()
                .withCredentials(cp)
                .withRegion(region)
                .build();
    }

    private void setAwsAccountServiceProducerPool(ProducerPool pool) {
        m_awsAccountServiceProducerPool = pool;
    }

    private ProducerPool getAwsAccountServiceProducerPool() {
        return m_awsAccountServiceProducerPool;
    }
}
