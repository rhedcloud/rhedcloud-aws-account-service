package edu.emory.awsaccount.service.deprovisioning.step;

import java.util.List;
import java.util.Properties;

import org.openeai.config.AppConfig;

import com.amazon.aws.moa.objects.resources.v1_0.Property;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.organizations.AWSOrganizationsClient;
import com.amazonaws.services.organizations.AWSOrganizationsClientBuilder;
import com.amazonaws.services.organizations.model.Account;
import com.amazonaws.services.organizations.model.AccountNotFoundException;
import com.amazonaws.services.organizations.model.AccountStatus;
import com.amazonaws.services.organizations.model.CloseAccountRequest;
import com.amazonaws.services.organizations.model.CloseAccountResult;
import com.amazonaws.services.organizations.model.DescribeAccountRequest;
import com.amazonaws.services.organizations.model.DescribeAccountResult;
import com.amazonaws.services.organizations.model.TooManyRequestsException;

import edu.emory.awsaccount.service.provider.AccountDeprovisioningProvider;

/**
 * Delete the VPC endpoint service permissions for the account from the security account.
 * <P>
 *
 * @author Tod Jackson (jtjacks@emory.edu)
 * @version 1.0 - 22 February 2022
 */
public class CloseAccount extends AbstractStep implements Step {
    private String accessKeyId = null;
    private String secretKey = null;
    private int maxAttempts=6;
    
    public void init(String provisioningId, Properties props, AppConfig aConfig, AccountDeprovisioningProvider adp) throws StepException {
        super.init(provisioningId, props, aConfig, adp);

        String LOGTAG = getStepTag() + "[CloseAccount.init] ";

        // Get custom step properties.
        logger.info(LOGTAG + "Getting custom step properties...");

        accessKeyId = getProperties().getProperty("accessKeyId");
        logger.info(LOGTAG + "accessKeyId is: " + accessKeyId);

        secretKey = getProperties().getProperty("secretKey");
        logger.info(LOGTAG + "secretKey is: present");
        
        maxAttempts = Integer.parseInt(getProperties().getProperty("maxAttempts", "6"));
        logger.info(LOGTAG + "maxAttempts: " + maxAttempts);

        logger.info(LOGTAG + "Initialization complete.");
    }

    protected List<Property> run() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[CloseAccount.run] ";
        logger.info(LOGTAG + "Begin running the step.");

        addResultProperty("stepExecutionMethod", RUN_EXEC_TYPE);

        String accountId = getAccountDeprovisioning().getAccountDeprovisioningRequisition().getAccountId();
        AWSOrganizationsClient orgClient = buildOrganizationsClient();
        CloseAccountRequest car = new CloseAccountRequest();
        car.setAccountId(accountId);

    	boolean isClosed = false;
    	boolean tmreException = false;
    	int loopCntr=0;
        try {
            // Build the request.
            logger.info(LOGTAG + "Sending the close account request and waiting for account to be closed...");
            long closeStartTime = System.currentTimeMillis();
            long closeTime = 0;
            CloseAccountResult carResult = orgClient.closeAccount(car);
            logger.info(LOGTAG + "CloseAccountResult as a string: " + carResult.toString());

        	// get into a loop and describe the account until it's done not found or null...
        	DescribeAccountRequest daRequest = new DescribeAccountRequest();
        	daRequest.setAccountId(accountId);
        	describeLoop: while (!isClosed) {
        		try {
        			DescribeAccountResult daResponse = orgClient.describeAccount(daRequest);
                	Account awsAccount = daResponse.getAccount();
                	if (awsAccount == null) {
                		logger.info(LOGTAG + "Account " + accountId + " does "
               				+ "not exist anymore and is closed...");
                		isClosed = true;
                        closeTime = System.currentTimeMillis() - closeStartTime;
                		break describeLoop;
                	}
                	String accountStatus = awsAccount.getStatus();
            		logger.info(LOGTAG + "Account " + accountId + 
            			" status is: " + accountStatus);
            		if (accountStatus != null && 
                		accountStatus.equalsIgnoreCase(AccountStatus.SUSPENDED.toString())) {
                		
                		// closed
            			isClosed=true;
            			closeTime = System.currentTimeMillis() - closeStartTime;
                		logger.info(LOGTAG + "Account " + accountId + 
                			" is suspended...account was closed successfully");
            			break describeLoop;
                	}
            		loopCntr++;
            		if (loopCntr >= maxAttempts) {
            			isClosed=true;
                        closeTime = System.currentTimeMillis() - closeStartTime;
                        logger.info(LOGTAG + "checked account status " + maxAttempts + " times, "
                       		+ "considering this account closed.");
                		break describeLoop;
            		}
            		sleep(5000);
        		}
        		catch (AccountNotFoundException anf) {
        			logger.info(LOGTAG + "account " + accountId + " was not found.  Account is closed.");
                    closeTime = System.currentTimeMillis() - closeStartTime;
            		isClosed = true;
        		}
        		catch (Exception e) {
                    closeTime = System.currentTimeMillis() - closeStartTime;
            		isClosed = true;
                    String errMsg = "An unknown exception occurred describing the account " + 
                       	accountId + ". Since the exception did NOT occur "
               			+ "during the CloseAccount API call, the account will be "
               			+ "considered closed.  The exception is: " + e.getMessage();
                    logger.warn(LOGTAG + errMsg, e);
        		}
        	}
        	if (isClosed) {
                logger.info(LOGTAG + "account " + accountId 
                	+ " was closed in " + closeTime + " ms.");
        	}
        	else {
                logger.info(LOGTAG + "account " + accountId + " was NOT "
               		+ "closed and will have to be closed manually.  "
               		+ "closeTime: " + closeTime + " ms.");
        	}
        }
        catch (Exception e) {
            String errMsg = "An exception occurred closing the account (CloseAccount API call) " + 
            	accountId + ".  Processing will continue and a support "
       			+ "ticket will be created to manually close the account.  "
       			+ "The exception is: " + e.getMessage();
            logger.warn(LOGTAG + errMsg);
            e.printStackTrace();
        }

        if (isClosed) {
            addResultProperty("isClosed", "true");
            addResultProperty("tooManyRequestsException", "false");
        }
        else {
            addResultProperty("isClosed", "false");
            addResultProperty("tooManyRequestsException", Boolean.toString(tmreException));
        }
        // The step is done. Update the step.
        update(COMPLETED_STATUS, SUCCESS_RESULT);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step run completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    private void sleep(long millisToSleep) {
    	try {
			Thread.sleep(millisToSleep);
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
    }
    
    protected List<Property> simulate() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[DeleteVpcEndpointServicePermissions.simulate] ";
        logger.info(LOGTAG + "Begin step simulation.");

        // Set return properties.
        addResultProperty("stepExecutionMethod", SIMULATED_EXEC_TYPE);

        // Update the step.
        update(COMPLETED_STATUS, SUCCESS_RESULT);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step simulation completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    protected List<Property> fail() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[DeleteVpcEndpointServicePermissions.fail] ";
        logger.info(LOGTAG + "Begin step failure simulation.");

        // Set return properties.
        addResultProperty("stepExecutionMethod", FAILURE_EXEC_TYPE);

        // Update the step.
        update(COMPLETED_STATUS, FAILURE_RESULT);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step failure simulation completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    public void rollback() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[DeleteVpcEndpointServicePermissions.rollback] ";

        update(ROLLBACK_STATUS, SUCCESS_RESULT);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Rollback completed in " + time + "ms.");
    }

    private AWSOrganizationsClient buildOrganizationsClient() {
        BasicAWSCredentials basicCredentials = new BasicAWSCredentials(accessKeyId, secretKey);
        AWSStaticCredentialsProvider credentialsProvider = new AWSStaticCredentialsProvider(basicCredentials);
        return (AWSOrganizationsClient) AWSOrganizationsClientBuilder.standard()
                .withCredentials(credentialsProvider)
//                .withRegion(Regions.DEFAULT_REGION)
                .build();
    }
}
