/* *****************************************************************************
 This file is part of the Emory AWS Account Service.

 Copyright (C) 2017 Emory University. All rights reserved.
 ******************************************************************************/
package edu.emory.awsaccount.service.deprovisioning.step;

import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import javax.jms.JMSException;

import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.jms.producer.MessageProducer;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.moa.EnterpriseObjectDeleteException;
import org.openeai.moa.EnterpriseObjectQueryException;
import org.openeai.moa.XmlEnterpriseObjectException;
import org.openeai.transport.RequestService;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.AccountAssignment;
import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.CustomRole;
import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.PermissionSet;
import com.amazon.aws.moa.objects.resources.v1_0.AccountAssignmentQuerySpecification;
import com.amazon.aws.moa.objects.resources.v1_0.CustomRoleQuerySpecification;
import com.amazon.aws.moa.objects.resources.v1_0.PermissionSetQuerySpecification;
import com.amazon.aws.moa.objects.resources.v1_0.Property;
import com.zzo.javaserver.Log;

import edu.emory.awsaccount.service.provider.AccountDeprovisioningProvider;


/**
 * Delete all Custom Roles in the Account.
 */
public class DeleteAllCustomAwsIdmObjectsInAccount extends AbstractStep implements Step {
    private ProducerPool awsAccountServiceProducerPool;
    private ProducerPool idmServiceProducerPool = null;
    private String organizationalUnitDnTemplate;

    String LOGTAG = "[DeleteAllCustomAwsIdmObjectsInAccount] ";
	private List<PermissionSet> permissionSets;

    @SuppressWarnings("unused")
	public void init (String deprovisioningId, Properties props, AppConfig aConfig, AccountDeprovisioningProvider vpcpp) throws StepException {
        super.init(deprovisioningId, props, aConfig, vpcpp);
        LOGTAG =  getStepTag() + "[DeleteAllCustomAwsIdmObjectsInAccount.init] ";
        // This step needs to send messages to the AWS account service to deprovision based on CustomRoles.
        try {
            ProducerPool p = (ProducerPool)getAppConfig().getObject("AwsAccountServiceProducerPool");
            setAwsAccountServiceProducerPool(p);
        }
        catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "An error occurred retrieving an object from AppConfig. The exception is: " + e.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new StepException(errMsg, e);
        }
        // This step needs to send messages to the Idm Service
        try {
            ProducerPool p = (ProducerPool) getAppConfig().getObject("IdmServiceProducerPool");
            setIdmServiceProducerPool(p);
        }
        catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "An error occurred retrieving an object from AppConfig. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new StepException(errMsg, e);
        }
        
    	try {
    		AccountAssignment aa = (AccountAssignment) getAppConfig().getObject("AccountAssignment.v1_0");
    		AccountAssignmentQuerySpecification aaquery = (AccountAssignmentQuerySpecification) getAppConfig().getObject("AccountAssignmentQuerySpecification.v1_0");
			PermissionSet ps = (PermissionSet) getAppConfig().getObject("PermissionSet.v1_0");
			PermissionSetQuerySpecification psquery = (PermissionSetQuerySpecification) getAppConfig().getObject("PermissionSetQuerySpecification.v1_0");			
		} catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "An error occurred retrieving an object from AppConfig. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new StepException(errMsg, e);
		}

        logger.info(LOGTAG + "Getting custom step properties...");

        String organizationalUnitDnTemplate = getProperties().getProperty("organizationalUnitDnTemplate", null);
        if (organizationalUnitDnTemplate == null || organizationalUnitDnTemplate.equals("")) {
            throw new StepException("No organizationalUnitDnTemplate property specified. Can't continue.");
        }
        setOrganizationalUnitDnTemplate(organizationalUnitDnTemplate);
        logger.info(LOGTAG + "organizationalUnitDnTemplate is: " + organizationalUnitDnTemplate);


        logger.info(LOGTAG + "Initialization complete.");
    }

    private List<String> getCustomRoleNamesInAccount(String accountId, String LOGTAG) throws StepException {
        CustomRole customRole;
        CustomRoleQuerySpecification querySpec;
        try {
            customRole = (CustomRole) getAppConfig().getObjectByType(CustomRole.class.getName());
            querySpec = (CustomRoleQuerySpecification) getAppConfig().getObjectByType(CustomRoleQuerySpecification.class.getName());
        }
        catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "An error occurred getting CustomRole properties from AppConfig. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new StepException(errMsg, e);
        }
        try {
            querySpec.setAccountId(accountId);
            logger.info(LOGTAG + "CustomRoleQuerySpecification is: " + querySpec.toXmlString());
        }
        catch (EnterpriseFieldException e) {
            String errMsg = "An error occurred setting field values of the CustomRoleQuerySpecification object. The exception is: " + e.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new StepException(errMsg, e);
        }
        catch (XmlEnterpriseObjectException e) {
            String errMsg = "An error occurred serializing the CustomRoleQuerySpecification to XML. The exception is: " + e.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new StepException(errMsg, e);
        }

        // Get a producer from the pool
        RequestService rs;
        try {
            rs = (RequestService) getAwsAccountServiceProducerPool().getExclusiveProducer();
        }
        catch (JMSException e) {
            String errMsg = "An error occurred getting a producer from the pool. The exception is: " + e.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new StepException(errMsg, e);
        }

        try {
            long elapsedStartTime = System.currentTimeMillis();
            @SuppressWarnings("unchecked")
            List<CustomRole> results = customRole.query(querySpec, rs);
            long elapsedTime = System.currentTimeMillis() - elapsedStartTime;
            logger.info(LOGTAG + "CustomRole query took " + elapsedTime + " ms.");

            return results.stream().map(CustomRole::getRoleName).collect(Collectors.toList());
        }
        catch (EnterpriseObjectQueryException e) {
            String errMsg = "An error occurred querying the CustomRole. The exception is: " + e.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new StepException(errMsg, e);
        }
        finally {
            getAwsAccountServiceProducerPool().releaseProducer((MessageProducer)rs);
        }
    }
    
    private PermissionSet getPermissionSet(String name) {
    	for (PermissionSet ps:permissionSets) {
    		if (name.contentEquals(ps.getName())) return ps;
    	}
    	return null;
    }

    protected List<Property> run() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[DeleteAllCustomAwsIdmObjectsInAccount.run] ";
        logger.info(LOGTAG + "Begin running the step.");
        PermissionSet ps = null;
        PermissionSetQuerySpecification psquery = null;
        RequestService rs = null;
		try {
			ps = (PermissionSet) getAppConfig().getObject("PermissionSet.v1_0");
			psquery = (PermissionSetQuerySpecification) getAppConfig().getObject("PermissionSetQuerySpecification.v1_0");
			rs = (RequestService) getIdmServiceProducerPool().getProducer();
			((PointToPointProducer)rs).setRequestTimeoutInterval(60000);
			psquery.setWithPolicies("false");
			psquery.setWithTags("false");
	    	logger.info(LOGTAG + "Retrieving all permission sets...");
	    	long start = System.currentTimeMillis();
			permissionSets = ps.query(psquery, rs);
			logger.info(LOGTAG + "Got "+permissionSets.size()+" in "+(System.currentTimeMillis()-start)+" millis.");
		} catch (EnterpriseConfigurationObjectException | EnterpriseFieldException | JMSException | EnterpriseObjectQueryException e) {
            String errMsg = "An error trying to query all permssion sets. The exception is: " + e.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new StepException(errMsg, e);
		}

        String accountId = getAccountDeprovisioning().getAccountDeprovisioningRequisition().getAccountId();
        
        List<String> customPermissionSetNamesInAccount = getCustomRoleNamesInAccount(accountId, LOGTAG);
        if (customPermissionSetNamesInAccount.size() == 0) {
            logger.info(LOGTAG + "There are no custom permission sets in account");
            addResultProperty("customPermissionSetsInAccount", "none");
        }
        else {
            logger.info(LOGTAG + "Custom permission sets in account are " + String.join(" and ", customPermissionSetNamesInAccount));
            for (int i = 0; i < customPermissionSetNamesInAccount.size(); i++) {
                addResultProperty("customPermissionSetsInAccount" + i, customPermissionSetNamesInAccount.get(i));
            }
            
        }

        // delete the account assignments and permission set associated with the role
        for (String permissionSetName : customPermissionSetNamesInAccount) {
        	try {						
				ps = getPermissionSet(permissionSetName);
				if (ps != null) {
				ps.delete("purge",rs);
		        addResultProperty(permissionSetName, "deleted");
				} else {
					addResultProperty(permissionSetName, "does not exist");
				}
			} catch (EnterpriseObjectDeleteException e) {
	            String errMsg = "An error occurred deleting an account assignment or permission set for the permission set named "+permissionSetName+". The exception is: " + e.getMessage();
	            logger.error(LOGTAG + errMsg);
	            throw new StepException(errMsg, e);
			}
        }

        // Set return properties.
        addResultProperty("stepExecutionMethod", RUN_EXEC_TYPE);

        // Update the step.
        update(COMPLETED_STATUS, SUCCESS_RESULT);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step run completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    protected List<Property> simulate() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[DeleteAllCustomRolesInAccount.simulate] ";
        logger.info(LOGTAG + "Begin step simulation.");

        // Set return properties.
        addResultProperty("stepExecutionMethod", SIMULATED_EXEC_TYPE);

        // Update the step.
        update(COMPLETED_STATUS, SUCCESS_RESULT);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step simulation completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    protected List<Property> fail() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[DeleteAllCustomRolesInAccount.fail] ";
        logger.info(LOGTAG + "Begin step failure simulation.");

        // Set return properties.
        addResultProperty("stepExecutionMethod", FAILURE_EXEC_TYPE);

        // Update the step.
        update(COMPLETED_STATUS, FAILURE_RESULT);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step failure simulation completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    public void rollback() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[DeleteAllCustomRolesInAccount.rollback] ";
        logger.info(LOGTAG + "Rollback called, but this step has nothing to roll back.");
        update(ROLLBACK_STATUS, SUCCESS_RESULT);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Rollback completed in " + time + "ms.");
    }

    private ProducerPool getAwsAccountServiceProducerPool() { return awsAccountServiceProducerPool; }
    private void setAwsAccountServiceProducerPool(ProducerPool v) { this.awsAccountServiceProducerPool = v; }
    private ProducerPool getIdmServiceProducerPool() { return idmServiceProducerPool; }
    private void setIdmServiceProducerPool(ProducerPool v) { idmServiceProducerPool = v; }
    public String getOrganizationalUnitDnTemplate() { return organizationalUnitDnTemplate; }
    public void setOrganizationalUnitDnTemplate(String v) { organizationalUnitDnTemplate = v; }

}
