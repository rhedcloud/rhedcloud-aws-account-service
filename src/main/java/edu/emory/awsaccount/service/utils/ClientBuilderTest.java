package edu.emory.awsaccount.service.utils;

import java.util.HashSet;
import java.util.Set;

import org.openeai.OpenEaiObject;

import com.amazonaws.SdkClientException;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.DeleteVpcRequest;
import com.amazonaws.services.ec2.model.DescribeVpcsResult;
import com.amazonaws.services.ec2.model.Vpc;
import com.amazonaws.services.securitytoken.model.AWSSecurityTokenServiceException;

import edu.emory.awsaccount.service.AwsClientBuilderHelper;

public class ClientBuilderTest extends OpenEaiObject {

	private static final String LOGTAG = "";
	private static Set<String> ignoredRegionNames = new HashSet<>();

	public static void main(String[] args) {
//		ignoredRegionNames.add("us-iso-east-1");
//		ignoredRegionNames.add("us-iso-west-1");
		String accessKeyId = System.getenv().get("accessKeyId");
		String secretAccessKey = System.getenv().get("secretAccessKey");


		String roleArnPattern = "arn:aws:iam::ACCOUNT_NUMBER:role/rhedcloud/RHEDcloudAwsAccountServiceRole";
		for (Region region : RegionUtils.getRegions()) {
			System.out.println("region = "+region);
			if (ignoredRegionNames.contains(region.getName())	) {
				System.out.println(LOGTAG + "Ignoring region: " + region.getName());
				continue;

			}

			String accountId = "650733936045"; // aws-dev-884
			AmazonEC2 ec2 = null;
			try {
				ec2 = AwsClientBuilderHelper.buildAmazonEC2Client(
					accountId,
					region.toString(),
					accessKeyId, secretAccessKey, 
					roleArnPattern, 
					900);
			} catch (AWSSecurityTokenServiceException e) {
				System.err.println("AWSSecurityTokenServiceException for region "+region);
				continue;
			}	catch (SdkClientException e) {
				System.err.println("SdkClientException for region "+region);
				continue;
			}
			

			DescribeVpcsResult describeVpcsResult;

			describeVpcsResult = ec2.describeVpcs();
			System.out.println(LOGTAG+"Number of VPCs described is "+describeVpcsResult.getVpcs().size());

			// and compare - anything unregistered is flagged as a risk
			for (Vpc vpc : describeVpcsResult.getVpcs()) {
				if (vpc.isDefault()) {
					// ec2.deleteVpc(new DeleteVpcRequest(vpc.getVpcId()));
					String logmsg = "Deleted VPC with ID, "+vpc.getVpcId()+", in region, "+region;
					System.out.println(LOGTAG+logmsg);
				} else {
					String logmsg = "Skipped non-default VPC with ID, "+vpc.getVpcId()+", in region, "+region;
					System.out.println(LOGTAG+logmsg);           		
				}
			}
		} 
	} 
}
