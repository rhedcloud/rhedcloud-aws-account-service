/* *****************************************************************************
 This file is part of the RHEDcloud AWS Account Service.

 Copyright 2020 RHEDcloud Foundation. All rights reserved.
 ******************************************************************************/

package edu.emory.awsaccount.service.roleProvisioning.step;

import java.util.List;
import java.util.Properties;

import javax.jms.JMSException;

import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.moa.EnterpriseObjectDeleteException;
import org.openeai.moa.EnterpriseObjectGenerateException;
import org.openeai.moa.EnterpriseObjectQueryException;
import org.openeai.transport.RequestService;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.Policy;
import com.amazon.aws.moa.objects.resources.v1_0.PolicyQuerySpecification;
import com.amazon.aws.moa.objects.resources.v1_0.PolicyRequisition;
import com.amazon.aws.moa.objects.resources.v1_0.Property;
import com.amazon.aws.moa.objects.resources.v1_0.RoleProvisioningRequisition;

import edu.emory.awsaccount.service.provider.RoleProvisioningProvider;


/**
 * Create the AWS IAM role for the custom role.
 */
public class CreateAwsPolicyForCustomRole extends AbstractStep implements Step {
    private static final String POLICY_DOCUMENT_TEMPLATE =
            "{\n"
    		+ "    \"Version\": \"2012-10-17\",\n"
    		+ "    \"Statement\": [\n"
    		+ "        {\n"
    		+ "            \"Sid\": \"DefaultPolicyToBeReplacedWithCustomPolicy\",\n"
    		+ "            \"Effect\": \"Deny\",\n"
    		+ "            \"Action\": \"blank:blank\",\n"
    		+ "            \"Resource\": \"*\"\n"
    		+ "        }\n"
    		+ "    ]\n"
    		+ "}";
    
    String LOGTAG = getStepTag() + "[CreateAwsPolicyForCustomRole] ";

	private String policyPath;

	private RequestService rs;

    @SuppressWarnings("unused")
	public void init(String provisioningId, Properties props, AppConfig aConfig, RoleProvisioningProvider rpp) throws StepException {
        super.init(provisioningId, props, aConfig, rpp);

        logger.info(LOGTAG + "Getting custom step properties...");
        
        // This step needs to send messages to the IDM service
        // to create and delete policies.
        try {
        	ProducerPool p2pIdm = (ProducerPool)getAppConfig()
                .getObject("IdmServiceProducerPool");
            PointToPointProducer p2p =
                    (PointToPointProducer) p2pIdm.getProducer();
            rs = (RequestService)p2p;
        } catch (EnterpriseConfigurationObjectException ecoe) {
            String errMsg = "An error occurred retrieving an object from " +
                    "AppConfig. The exception is: " + ecoe.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new StepException(errMsg);
        } catch (JMSException e) {
            String errMsg = "An error occurred retrieving a P2P from " +
                    "ProducerPool. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new StepException(errMsg);
		}
        
        // This step needs to get PermissionSet and PermissionSetRequistion objects from the config
		try {
			Policy __ = (Policy) getAppConfig().getObject("Policy.v1_0");
			PolicyRequisition ___ = (PolicyRequisition) getAppConfig().getObject("PolicyRequisition.v1_0");
			PolicyQuerySpecification _____ = (PolicyQuerySpecification) getAppConfig().getObject("PolicyQuerySpecification.v1_0");
		} catch (EnterpriseConfigurationObjectException e) {
			String msg="Can't load Policy.v1_0 or Policy.v1_0 or PolicyQuerySpecification.v1_0";
			logger.fatal(LOGTAG+msg);
			throw new StepException(msg);
		}
       
        policyPath = getProperties().getProperty("policyPath");
		logger.info(LOGTAG + "policyPath is: " + policyPath);
		if (policyPath == null) {
			policyPath = "/";
		}


        logger.info(LOGTAG + "Initialization complete.");
    }

    @SuppressWarnings("unchecked")
	protected List<Property> run() throws StepException {
        long startTime = System.currentTimeMillis();

        logger.info(LOGTAG + "Begin running the step.");

        addResultProperty(STEP_EXECUTION_METHOD_PROPERTY_KEY, STEP_EXECUTION_METHOD_EXECUTED);

        // the account and custom role name was specified in the requisition
        RoleProvisioningRequisition requisition = getRoleProvisioning().getRoleProvisioningRequisition();
        String accountId = requisition.getAccountId();
        String policyName = requisition.getRoleName();

        List<Policy> newpols;
        try {
        	
			Policy policy = (Policy) getAppConfig().getObject("Policy.v1_0");
			PolicyRequisition req = (PolicyRequisition) getAppConfig().getObject("PolicyRequisition.v1_0");
			req.setAccountId(accountId);
			req.setName(policyName);
			req.setPath(policyPath);
			req.setPolicyDocument(POLICY_DOCUMENT_TEMPLATE);
			req.setDescription("This policy is part of a custom role.");        	
			newpols = policy.generate(req, rs);
        	
        } catch (EnterpriseConfigurationObjectException 
        		| EnterpriseFieldException
        		| EnterpriseObjectGenerateException e) {
            String errMsg = "An error occurred creating the policy. The exception is: " + e.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new StepException(errMsg, e);
		} 

        // set result properties
        addResultProperty("customPolicyArn", newpols.get(0).getArn());

        // Update the step.
        update(STEP_STATUS_COMPLETED, STEP_RESULT_SUCCESS);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step run completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    protected List<Property> simulate() throws StepException {
        long startTime = System.currentTimeMillis();
        logger.info(LOGTAG + "Begin step simulation.");

        addResultProperty(STEP_EXECUTION_METHOD_PROPERTY_KEY, STEP_EXECUTION_METHOD_SIMULATED);

        // simulated result properties
        addResultProperty("customPolicyArn", "arn:aws:iam::123456789012:policy/custompolicies/CustomRole");

        // Update the step.
        update(STEP_STATUS_COMPLETED, STEP_RESULT_SUCCESS);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step simulation completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    protected List<Property> fail() throws StepException {
        long startTime = System.currentTimeMillis();
        logger.info(LOGTAG + "Begin step failure simulation.");

        addResultProperty(STEP_EXECUTION_METHOD_PROPERTY_KEY, STEP_EXECUTION_METHOD_FAILURE);

        // Update the step.
        update(STEP_STATUS_COMPLETED, STEP_RESULT_FAILURE);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step failure simulation completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    public void rollback() throws StepException {
        long startTime = System.currentTimeMillis();
        super.rollback();
        
        String policyArn = getResultProperty(getProvisioningStepById("CREATE_AWS_POLICY_FOR_CUSTOM_ROLE"), "customPolicyArn");
        if (policyArn == null) {
        	logger.warn(LOGTAG+"No step property named customPolicyArn exists for the step of type: CREATE_AWS_POLICY_FOR_CUSTOM_ROLE");
        	update(STEP_STATUS_ROLLBACK, STEP_RESULT_SUCCESS);
        	return;
        }

        // the account and custom role name was specified in the requisition
        RoleProvisioningRequisition requisition = getRoleProvisioning().getRoleProvisioningRequisition();
        String accountId = requisition.getAccountId();
        String policyName = requisition.getRoleName();

        try {
			Policy policy = (Policy) getAppConfig().getObject("Policy.v1_0");
			PolicyQuerySpecification querySpec = (PolicyQuerySpecification) getAppConfig().getObject("PolicyQuerySpecification.v1_0");
			querySpec.setAccountId(accountId);
			querySpec.setName(policyName);
			String errMsg = "Too many or too few policies named "+policyName;
			@SuppressWarnings("unchecked")
			List<Policy> results = policy.query(querySpec, rs);
			if (results.size() != 1) {
				logger.error(LOGTAG + "Too many or too few policies named "+policyName);
				throw new StepException(errMsg);
			}
			results.get(0).delete("Delete", rs);
	
           logger.info(LOGTAG+"Deleted policy named "+policyName);
        }
        catch (EnterpriseConfigurationObjectException | EnterpriseFieldException | EnterpriseObjectQueryException | EnterpriseObjectDeleteException e) {
            e.printStackTrace();
        	String errMsg = "An error occurred deleting the policy. The exception is: " + e.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new StepException(errMsg, e);
        }

        // Update the step.
        update(STEP_STATUS_ROLLBACK, STEP_RESULT_SUCCESS);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Rollback completed in " + time + "ms.");
    }

}
