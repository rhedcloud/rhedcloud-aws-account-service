/* *****************************************************************************
 This file is part of the RHEDcloud AWS Account Service.

 Copyright 2020 RHEDcloud Foundation. All rights reserved.
 ******************************************************************************/

package edu.emory.awsaccount.service.roleProvisioning.step;

import java.util.List;
import java.util.Properties;

import javax.jms.JMSException;

import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.config.PropertyConfig;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.moa.EnterpriseObjectCreateException;
import org.openeai.transport.RequestService;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.IamRole;
import com.amazon.aws.moa.objects.resources.v1_0.Property;
import com.amazon.aws.moa.objects.resources.v1_0.RoleProvisioningRequisition;

import edu.emory.awsaccount.service.provider.RoleProvisioningProvider;


/**
 * Create the AWS IAM role for the custom role.
 */
public class CreateAwsRoleForCustomRole extends AbstractStep implements Step {
    // taken from https://bitbucket.org/rhedcloud/rhedcloud-aws-rs-account-cfn/src/master/rhedcloud-aws-rs-account-cfn.json
    private static final String ASSUME_ROLE_POLICY_DOCUMENT_TEMPLATE =
            "{" +
            "    \"Version\": \"2012-10-17\"," +
            "    \"Statement\": [" +
            "        {" +
            "            \"Effect\": \"Allow\"," +
            "            \"Principal\": {" +
            "                \"Federated\": \"arn:aws:iam::ACCOUNT_NUMBER:saml-provider/SAML_IDP_NAME\"" +
            "            }," +
            "            \"Action\": \"sts:AssumeRoleWithSAML\"," +
            "            \"Condition\": {" +
            "                \"StringEquals\": {" +
            "                    \"saml:aud\": \"https://signin.aws.amazon.com/saml\"," +
            "                    \"saml:iss\": \"SAML_ISSUER\"" +
            "                }" +
            "            }" +
            "        }" +
            "    ]" +
            "}";

    private String rolePath = "/customroles/";
	private RequestService rs;
	
    private String roleArnPattern;

    //private String rolePath;

    private int roleAssumptionDurationSeconds = 0;

    private String samlIdpName = null;

    private String samlIssuer = null;

    private int maxSessionDurationSeconds = 0;



    public void init(String provisioningId, Properties props, AppConfig aConfig, RoleProvisioningProvider rpp) throws StepException {
        super.init(provisioningId, props, aConfig, rpp);

        String LOGTAG = getStepTag() + "[CreateAwsRoleForCustomRole.init] ";

        // This step needs to send messages to the IDM service
        // to create and delete AWS IamRoles.
        try {
        	ProducerPool p2pIdm = (ProducerPool)getAppConfig()
                .getObject("IdmServiceProducerPool");
            PointToPointProducer p2p =
                    (PointToPointProducer) p2pIdm.getProducer();
            rs = (RequestService)p2p;

        } catch (EnterpriseConfigurationObjectException ecoe) {
            String errMsg = "An error occurred retrieving an object from " +
                    "AppConfig. The exception is: " + ecoe.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new StepException(errMsg);
        } catch (JMSException e) {
            String errMsg = "An error occurred retrieving a P2P from " +
                    "ProducerPool. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new StepException(errMsg);
		}
        
        // This step needs to get IamRole objects from the config
		try {
			getAppConfig().getObject("IamRole.v1_0");
		} catch (EnterpriseConfigurationObjectException e) {
			String msg="Can't load IamRole.v1_0";
			logger.fatal(LOGTAG+msg);
			throw new StepException(msg);
		}
		
		try {
			 setProperties((Properties)((PropertyConfig) getAppConfig().getObject("CreateAwsRoleForCustomRoleProperties")).getProperties());
			 rolePath = getProperties().getProperty("rolePath","/customroles/");
		} catch (EnterpriseConfigurationObjectException e) {
			logger.warn(LOGTAG+"No PropertyConfig found.");
		}

        setRoleArnPattern(getMandatoryStringProperty(LOGTAG, "roleArnPattern", false));

        setRoleAssumptionDurationSeconds(getMandatoryIntegerProperty(LOGTAG, "roleAssumptionDurationSeconds", false));

        setSamlIdpName(getMandatoryStringProperty(LOGTAG, "samlIdpName", false));

        setSamlIssuer(getMandatoryStringProperty(LOGTAG, "samlIssuer", false));

        setMaxSessionDurationSeconds(getWithDefaultIntegerProperty(LOGTAG, "maxSessionDurationSeconds", "43200", false));


        logger.info(LOGTAG + "Initialization complete.");
    }

    protected List<Property> run() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[CreateAwsRoleForCustomRole.run] ";
        logger.info(LOGTAG + "Begin running the step.");

        addResultProperty(STEP_EXECUTION_METHOD_PROPERTY_KEY, STEP_EXECUTION_METHOD_EXECUTED);

        // the account and custom role name was specified in the requisition
        RoleProvisioningRequisition requisition = getRoleProvisioning().getRoleProvisioningRequisition();
        String accountId = requisition.getAccountId();
        String roleName = requisition.getRoleName();
        
        String policy = ASSUME_ROLE_POLICY_DOCUMENT_TEMPLATE

                .replace("ACCOUNT_NUMBER", accountId)

                .replace("SAML_IDP_NAME", getSamlIdpName())

                .replace("SAML_ISSUER", getSamlIssuer());

        try {
        	
			IamRole iamRole = (IamRole) getAppConfig().getObject("IamRole.v1_0");
			iamRole.setAccountId(accountId);
			iamRole.setName(roleName);
			iamRole.setPath(rolePath);
			iamRole.setAssumeRolePolicyDocument(policy);
			iamRole.setDescription("Custom role created by role provisioning");
			iamRole.setMaxSessionDuration(getMaxSessionDurationSeconds()+"");
			
			iamRole.create(rs);

       }
        catch (EnterpriseConfigurationObjectException | EnterpriseFieldException | EnterpriseObjectCreateException e) {
            String errMsg = "An error occurred creating the role. The exception is: " + e.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new StepException(errMsg, e);
        }

        // Update the step.
        update(STEP_STATUS_COMPLETED, STEP_RESULT_SUCCESS);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step run completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    public void rollback() throws StepException {
        long startTime = System.currentTimeMillis();
        super.rollback();
        String LOGTAG = getStepTag() + "[CreateAwsRoleForCustomRole.rollback] ";

        // the account and custom role name was specified in the requisition
        RoleProvisioningRequisition requisition = getRoleProvisioning().getRoleProvisioningRequisition();
        String accountId = requisition.getAccountId();
        String roleName = requisition.getRoleName();

        try {
        	
			IamRole iamRole = (IamRole) getAppConfig().getObject("IamRole.v1_0");
			iamRole.setAccountId(accountId);
			iamRole.setName(roleName);
			iamRole.setAssumeRolePolicyDocument("blank");
			iamRole.delete("Delete", rs);

        }
        catch (Exception e) {
            String errMsg = "An error occurred creating the role. The exception is: " + e.getMessage();
            logger.error(LOGTAG + errMsg);
            update(STEP_STATUS_ROLLBACK, STEP_RESULT_FAILURE);
            throw new StepException(errMsg, e);
        }

        // Update the step.
        update(STEP_STATUS_ROLLBACK, STEP_RESULT_SUCCESS);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Rollback completed in " + time + "ms.");
    }
    
    protected List<Property> simulate() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[CreateAwsRoleForCustomRole.simulate] ";
        logger.info(LOGTAG + "Begin step simulation.");

        addResultProperty(STEP_EXECUTION_METHOD_PROPERTY_KEY, STEP_EXECUTION_METHOD_SIMULATED);

        // simulated result properties
        addResultProperty("customRoleArn", "arn:aws:iam::123456789012:role/MyCustomRole");

        // Update the step.
        update(STEP_STATUS_COMPLETED, STEP_RESULT_SUCCESS);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step simulation completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    protected List<Property> fail() throws StepException {
        long startTime = System.currentTimeMillis();
        String LOGTAG = getStepTag() + "[CreateAwsRoleForCustomRole.fail] ";
        logger.info(LOGTAG + "Begin step failure simulation.");

        addResultProperty(STEP_EXECUTION_METHOD_PROPERTY_KEY, STEP_EXECUTION_METHOD_FAILURE);

        // Update the step.
        update(STEP_STATUS_COMPLETED, STEP_RESULT_FAILURE);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step failure simulation completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }
    
    private String getRoleArnPattern() { return roleArnPattern; }

    private void setRoleArnPattern(String v) { this.roleArnPattern = v; }

    public String getRolePath() { return rolePath; }

    public void setRolePath(String v) { this.rolePath = v; }

    private int getRoleAssumptionDurationSeconds() { return roleAssumptionDurationSeconds; }

    private void setRoleAssumptionDurationSeconds(int v) { this.roleAssumptionDurationSeconds = v; }

    public String getSamlIdpName() { return samlIdpName; }

    public void setSamlIdpName(String v) { this.samlIdpName = v; }

    public String getSamlIssuer() { return samlIssuer; }

    public void setSamlIssuer(String v) { this.samlIssuer = v; }

	public int getMaxSessionDurationSeconds() {

		return maxSessionDurationSeconds;

	}

	public void setMaxSessionDurationSeconds(int maxSessionDurationSeconds) {

		this.maxSessionDurationSeconds = maxSessionDurationSeconds;

	}

}
