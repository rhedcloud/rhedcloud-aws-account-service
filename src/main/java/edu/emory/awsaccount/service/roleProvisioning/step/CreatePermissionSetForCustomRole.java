/* *****************************************************************************
 This file is part of the RHEDcloud AWS Account Service.

 Copyright 2020 RHEDcloud Foundation. All rights reserved.
 ******************************************************************************/

package edu.emory.awsaccount.service.roleProvisioning.step;

import java.util.List;
import java.util.Properties;

import javax.jms.JMSException;

import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.moa.EnterpriseObjectDeleteException;
import org.openeai.moa.EnterpriseObjectGenerateException;
import org.openeai.moa.EnterpriseObjectQueryException;
import org.openeai.transport.RequestService;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.Account;
import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.PermissionSet;
import com.amazon.aws.moa.objects.resources.v1_0.AccountQuerySpecification;
import com.amazon.aws.moa.objects.resources.v1_0.PermissionSetQuerySpecification;
import com.amazon.aws.moa.objects.resources.v1_0.PermissionSetRequisition;
import com.amazon.aws.moa.objects.resources.v1_0.Property;
import com.amazon.aws.moa.objects.resources.v1_0.RoleProvisioningRequisition;

import edu.emory.awsaccount.service.provider.RoleProvisioningProvider;


/**
 * Create the AWS IAM Identity Center Permission Set for the custom role.
 */
public class CreatePermissionSetForCustomRole extends AbstractStep implements Step {
 

	String LOGTAG = getStepTag() + "[CreateAwsPermissionSetForCustomRole] ";
	private RequestService idmrs;
	private RequestService aasrs;
	
    @SuppressWarnings("unused")
	public void init(String provisioningId, Properties props, AppConfig aConfig, RoleProvisioningProvider rpp) throws StepException {
        super.init(provisioningId, props, aConfig, rpp);

        // This step needs to send messages to the IDM service
        // to create and delete permission sets. It also needs to
        // send messages to the AAS service to look up account names
        try {
        	ProducerPool p2pIdm = (ProducerPool)getAppConfig()
                .getObject("IdmServiceProducerPool");
            PointToPointProducer p2p =
                    (PointToPointProducer) p2pIdm.getProducer();
            idmrs = (RequestService)p2p;
        	ProducerPool p2pAas = (ProducerPool)getAppConfig()
                    .getObject("AwsAccountServiceProducerPool");
            PointToPointProducer p2p2 =
                    (PointToPointProducer) p2pAas.getProducer();
            aasrs = (RequestService)p2p2;

        } catch (EnterpriseConfigurationObjectException ecoe) {
            String errMsg = "An error occurred retrieving an object from " +
                    "AppConfig. The exception is: " + ecoe.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new StepException(errMsg);
        } catch (JMSException e) {
            String errMsg = "An error occurred retrieving a P2P from " +
                    "ProducerPool. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new StepException(errMsg);
		}
        
        // This step needs to get PermissionSet and PermissionSetRequistion objects from the config
		try {
			getAppConfig().getObject("Account.v1_0");
			getAppConfig().getObject("AccountQuerySpecification.v1_0");
		} catch (EnterpriseConfigurationObjectException e) {
			String msg="Can't load Account.v1_0 or AccountQuerySpecification.v1_0";
			logger.fatal(LOGTAG+msg);
			throw new StepException(msg);
		}
		
        // This step needs to get PermissionSet and PermissionSetRequistion objects from the config
		try {
			getAppConfig().getObject("PermissionSet.v1_0");
			getAppConfig().getObject("PermissionSetRequisition.v1_0");
			getAppConfig().getObject("PermissionSetQuerySpecification.v1_0");
		} catch (EnterpriseConfigurationObjectException e) {
			String msg="Can't load PermissionSet.v1_0 or PermissionSetRequisition.v1_0 or PermissionSetQuerySpecification.v1_0";
			logger.fatal(LOGTAG+msg);
			throw new StepException(msg);
		}


        logger.info(LOGTAG + "Initialization complete.");
    }

    @SuppressWarnings("unchecked")
	protected List<Property> run() throws StepException {
        long startTime = System.currentTimeMillis();
        logger.info(LOGTAG + "Begin running the step.");

        addResultProperty(STEP_EXECUTION_METHOD_PROPERTY_KEY, STEP_EXECUTION_METHOD_EXECUTED);

        RoleProvisioningRequisition requisition = getRoleProvisioning().getRoleProvisioningRequisition();
        String permissionSetName = requisition.getRoleName();
        addResultProperty("customPermissionSetName", permissionSetName);
		try {
			PermissionSet ps = (PermissionSet) getAppConfig().getObject("PermissionSet.v1_0");
			PermissionSetRequisition psseed = (PermissionSetRequisition) getAppConfig().getObject("PermissionSetRequisition.v1_0");
			psseed.setName(permissionSetName);
			psseed.setSessionDuration("PT12H");
			String accountId = getRoleProvisioning().getAccountId(); 
			Account account = (Account) getAppConfig().getObject("Account.v1_0");
			AccountQuerySpecification query = (AccountQuerySpecification) getAppConfig().getObject("AccountQuerySpecification.v1_0");
			try {
				query.setAccountId(accountId);
				List<Account> accounts = account.query(query, aasrs);
				if (accounts.size() == 0 ) {
					logger.warn(LOGTAG+"No accounts found for account ID "+accountId+". ");
					psseed.setDescription("??? ??? ??? ("+accountId+")");
				} else {
					psseed.setDescription(accounts.get(0).getAccountName()+" ("+accountId+")");
				}
			} catch (EnterpriseFieldException | EnterpriseObjectQueryException e) {
				logger.warn(LOGTAG+"Exception thrown whilst querying for account name for account ID "+accountId+". The exception is "+e);
			}

			List<PermissionSet> results = null;
			try {
				results = ps.generate(psseed, idmrs);
		        addResultProperty("customPermissionSetArn", results.get(0).getArn());
			} catch (EnterpriseObjectGenerateException e) {
				String catchphrase = "PermissionSet with name "+permissionSetName+" already exists.";
				if (!e.getMessage().contains(catchphrase)) {
	 		           String errMsg = "An error occurred generating the permission set for " +
			                    permissionSetName+". The exception is: " + e.getMessage();
			            logger.error(LOGTAG + errMsg);
			            throw new StepException(errMsg,e);					
				}
				addResultProperty("skipped", catchphrase);
			}

	        // Update the step.
	        update(STEP_STATUS_COMPLETED, STEP_RESULT_SUCCESS);

	        // Log completion time.
	        long time = System.currentTimeMillis() - startTime;
	        logger.info(LOGTAG + "Step run completed in " + time + "ms.");

	        // Return the properties.
	        return getResultProperties();
	        
		} catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "An error occurred retrieving an object from " +
                    "AppConfig. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new StepException(errMsg,e);
		} catch (EnterpriseFieldException e) {
            String errMsg = "An error occurred setting a field in the requisition" +
                    ". The exception is: " + e.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new StepException(errMsg,e);
		} 
    }

    protected List<Property> simulate() throws StepException {
        long startTime = System.currentTimeMillis();
        logger.info(LOGTAG + "Begin step simulation.");

        addResultProperty(STEP_EXECUTION_METHOD_PROPERTY_KEY, STEP_EXECUTION_METHOD_SIMULATED);

        // simulated result properties
        addResultProperty("customPermissionSetArn", "arn:aws:sso:::permissionSet/ssoins-72238aa0982f098e/ps-27a030a837227e97");

        // Update the step.
        update(STEP_STATUS_COMPLETED, STEP_RESULT_SUCCESS);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step simulation completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    protected List<Property> fail() throws StepException {
        long startTime = System.currentTimeMillis();
        logger.info(LOGTAG + "Begin step failure simulation.");

        addResultProperty(STEP_EXECUTION_METHOD_PROPERTY_KEY, STEP_EXECUTION_METHOD_FAILURE);

        // Update the step.
        update(STEP_STATUS_COMPLETED, STEP_RESULT_FAILURE);

        // Log completion time.
        long time = System.currentTimeMillis() - startTime;
        logger.info(LOGTAG + "Step failure simulation completed in " + time + "ms.");

        // Return the properties.
        return getResultProperties();
    }

    public void rollback() throws StepException {
    	long startTime = System.currentTimeMillis();
        addResultProperty(STEP_EXECUTION_METHOD_PROPERTY_KEY, STEP_EXECUTION_METHOD_EXECUTED);

        RoleProvisioningRequisition requisition = getRoleProvisioning().getRoleProvisioningRequisition();
        String permissionSetName = requisition.getRoleName();
        addResultProperty("customPermissionSetName", permissionSetName);
		try {
			PermissionSet ps = (PermissionSet) getAppConfig().getObject("PermissionSet.v1_0");
			PermissionSetQuerySpecification psquery = (PermissionSetQuerySpecification) getAppConfig()
					.getObject("PermissionSetQuerySpecification.v1_0");
			psquery.setName(permissionSetName);

			@SuppressWarnings({ "unchecked" })
			List<PermissionSet> results = ps.query(psquery, idmrs);
			if (results.size() != 1) {
		           String errMsg = results.size()+" results returned from permissionSetName but expected 1 and only 1.";
		            logger.warn(LOGTAG + errMsg);
		            if (results.size() == 0) return;
			} 
			ps = results.get(0);
			ps.delete("delete", idmrs);
		    // set result properties
	        addResultProperty("deletedCustomPermissionSetArn", results.get(0).getArn());				

	        // Update the step.
	        update(STEP_STATUS_COMPLETED, STEP_RESULT_SUCCESS);

	        // Log completion time.
	        long time = System.currentTimeMillis() - startTime;
	        logger.info(LOGTAG + "Step run completed in " + time + "ms.");

	        
		} catch (EnterpriseConfigurationObjectException | EnterpriseFieldException | EnterpriseObjectDeleteException | EnterpriseObjectQueryException e) {
			e.printStackTrace();
			String errMsg = "An error occurred while trying to delete the PermissionSet.";
			logger.fatal(LOGTAG + errMsg);
            update(STEP_STATUS_ROLLBACK, STEP_RESULT_FAILURE);
            throw new StepException(errMsg,e);
		} 
    }

}
