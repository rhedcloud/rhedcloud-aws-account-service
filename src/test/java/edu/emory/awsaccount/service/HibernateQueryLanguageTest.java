package edu.emory.awsaccount.service;

import com.openii.openeai.toolkit.rdbms.persistence.hibernate.SessionFactoryUtil;
import org.apache.logging.log4j.Logger;
import org.hibernate.PropertyAccessException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Test all queries from the querylanguages.xml file - both named and hql.
 */
public class HibernateQueryLanguageTest {
    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(HibernateQueryLanguageTest.class);

    public static void main(String[] args) {
        SessionFactory sessionFactory = SessionFactoryUtil.getInstance();
        logger.info(sessionFactory);

        // here are all queries from the querylanguages.xml file - both named and hql.
        // because MOA objects don't work well standalone, queries must be tested one at a time.
        // also, hibernate might not be able to create an object even though the query was successful.

        QueryLang[] queries = {
                new QueryLang("from ServiceSecurityAssessment where :ServiceId in elements(ServiceId)",
                        "ServiceId", "313"),
//                new QueryLang("from ServiceSecurityAssessment where cast(ServiceSecurityAssessmentId AS string) = :ServiceSecurityAssessmentId and :ServiceId in elements(ServiceId)",
//                        "ServiceSecurityAssessmentId", "6", "ServiceId", "313"),
//                new QueryLang("from Service where :ConsoleCategory in elements(ConsoleCategory)",
//                        "ConsoleCategory", "API Gateway"),
//                new QueryLang("from TermsOfUse where EffectiveDate <= to_date(:currentDate, 'YYYY-MM-DD') and (ExpirationDate=null or ExpirationDate >= to_date(:currentDate, 'YYYY-MM-DD'))",
//                        "currentDate", "2020-08-20"),
//                new QueryLang("from VirtualPrivateCloudProvisioning order by CreateDatetime desc"),
//                new QueryLang("from AccountDeprovisioning order by CreateDatetime desc"),
//                new QueryLang("from RoleProvisioning order by CreateDatetime desc"),
//                new QueryLang("from RoleDeprovisioning order by CreateDatetime desc"),
//                new QueryLang("from AccountNotification where AccountId=:AccountId order by CreateDatetime desc",
//                        "AccountId", "250049456440"),
//                new QueryLang("from AccountNotification where ( CreateDatetime between to_date(:StartCreateDatetime,'YYYY-MM-DD HH24:MI:SS') and to_date(:EndCreateDatetime,'YYYY-MM-DD HH24:MI:SS') ) order by CreateDatetime desc",
//                        "StartCreateDatetime", "2021-03-24 07:09:00", "EndCreateDatetime", "2021-03-24 07:10:00"),
//                new QueryLang("from AccountNotification where CreateDatetime <= to_date(:endDate,'YYYY-MM-DD HH24:MI:SS') order by CreateDatetime desc",
//                        "endDate", "2021-03-05 16:01:01"),
//                new QueryLang("from AccountNotification where CreateDatetime >= to_date(:startDate,'YYYY-MM-DD HH24:MI:SS') order by CreateDatetime desc",
//                        "startDate", "2021-03-05 15:59:52"),
//                new QueryLang("from UserNotification where UserId = :UserId order by CreateDatetime desc",
//                        "UserId", "P4587309"),
//                new QueryLang("from UserNotification where UserId = :UserId and Read='false' order by CreateDatetime desc",
//                        "UserId", "P4587309"),
//                new QueryLang("from UserNotification where CreateDatetime between to_date(:startDate,'YYYY-MM-DD HH24:MI:SS') and to_date(:endDate,'YYYY-MM-DD HH24:MI:SS') order by CreateDatetime desc",
//                        "startDate", "2021-03-05 18:21:08", "endDate", "2021-03-05 18:21:10"),
//                new QueryLang("from UserNotification where CreateDatetime <= to_date(:endDate,'YYYY-MM-DD HH24:MI:SS') order by CreateDatetime desc",
//                        "endDate", "2021-03-05 18:21:10"),
//                new QueryLang("from UserNotification where CreateDatetime >= to_date(:startDate,'YYYY-MM-DD HH24:MI:SS') order by CreateDatetime desc",
//                        "startDate", "2021-03-05 18:21:08"),
                // in mapping files
//                new QueryLang(true, "findBillsByDates",
//                        "StartDate", "2021-03-05", "EndDate", "2021-03-05"),
//                new QueryLang(true, "pendingWithin30mins"),
//                new QueryLang(true, "findAccountNotificationsByAnnotationText",
//                        "AnnotationText", "%error%", "StartCreateDatetime", "2021-03-05 15:59:52", "EndCreateDatetime", "2021-03-05 15:59:52"),
        };

        Session session = sessionFactory.getCurrentSession();
        Transaction tx = session.beginTransaction();
        session.setDefaultReadOnly(true);

        for (QueryLang q : queries) {
            Query query = q.named ? session.getNamedQuery(q.hql) : session.createQuery(q.hql);
            for (Map.Entry<String, String> ent : q.parameters.entrySet()) {
                query.setString(ent.getKey(), ent.getValue());
            }

            try {
                List<?> qr = query.list();
                System.out.println("DONE - " + qr.size() + " - " + q.hql);
            }
            catch (PropertyAccessException e) {
                /*
                 * This is something like the following because MOA objects don't work well standalone
                 *
                 * org.hibernate.PropertyAccessException: Exception occurred inside setter of VirtualPrivateCloudProvisioning.CreateDatetime
                 * 	at org.hibernate.property.BasicPropertyAccessor$BasicSetter.set(BasicPropertyAccessor.java:89)
                 * 	... plenty more
                 * Caused by: java.lang.NullPointerException
                 * 	at org.openeai.moa.XmlEnterpriseObjectImpl.getInputLayoutManager(XmlEnterpriseObjectImpl.java:951)
                 * 	at org.openeai.moa.XmlEnterpriseObjectImpl.initializeChild(XmlEnterpriseObjectImpl.java:386)
                 * 	at com.amazon.aws.moa.jmsobjects.provisioning.v1_0.VirtualPrivateCloudProvisioning.newCreateDatetime(VirtualPrivateCloudProvisioning.java:531)
                 * 	at com.amazon.aws.moa.jmsobjects.provisioning.v1_0.VirtualPrivateCloudProvisioning.setCreateDatetime(VirtualPrivateCloudProvisioning.java:516)
                 * 	... 21 more
                 */
                System.out.println("ERROR - " + e.getMessage() + " - " + q.hql);
            }
        }

        tx.rollback();
    }

    private static class QueryLang {
        boolean named;
        String hql;
        Map<String, String> parameters;

        public QueryLang(String hql, String... param) {
            this.named = false;
            this.hql = hql;
            this.parameters = new HashMap<>();
            for (int i = 0; i < param.length; i+=2) {
                this.parameters.put(param[i], param[i+1]);
            }
        }
        public QueryLang(boolean named, String queryName, String... param) {
            this.named = named;
            this.hql = queryName;
            this.parameters = new HashMap<>();
            for (int i = 0; i < param.length; i+=2) {
                this.parameters.put(param[i], param[i+1]);
            }
        }
    }
}
