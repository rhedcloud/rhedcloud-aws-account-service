package edu.emory.awsaccount.service;

import com.openii.openeai.toolkit.rdbms.persistence.hibernate.SessionFactoryUtil;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;

/**
 * Initialize a database using the RDBMS / Hibernate Session support.
 *
 * Note: Rename hibernate-init.cfg.xml to hibernate.cfg.xml and put it on the classpath.
 *       Adjust connection and hbm2ddl settings.
 */
public class HibernateInit {
    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(HibernateInit.class);

    public static void main(String[] args) {
        SessionFactory sessionFactory = SessionFactoryUtil.getInstance();
        logger.info(sessionFactory);
    }
}
